package it.infoblu.mobilemap.manager;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import it.infoblu.mobilemap.model.data.FilterStateInfo;
import it.infoblu.mobilemap.model.prefs.KeyPreferences;
import it.infoblu.mobilemap.utils.InternalLogger;
import it.infoblu.mobilemap.utils.JsonMapperManager;

/**
 * Created on 16/04/18.
 */
public final class PoiFilterPersistenceManager implements IPoiFilterPersistenceManager {

    private PoiFilterPersistenceManager() {
    }

    @Override
    public Boolean saveFilters(List<FilterStateInfo> filterPersistenceInfoList) {
        return saveFilters(null, filterPersistenceInfoList);
    }

    @Override
    public List<FilterStateInfo> getFilters() {
        return getFilters(null);
    }

    @Override
    public Boolean saveFilters(final String tag, List<FilterStateInfo> filterPersistenceInfoList) {
        try {
            KeyPreferences.getPrefs().getString(getFilterPersistenceTag(tag)).set(JsonMapperManager.MAPPER.writeValueAsString(filterPersistenceInfoList));
        } catch (JsonProcessingException e) {
            InternalLogger.e("Error occurred during save filters action: %s", e);
            return false;
        }
        return true;
    }

    @Override
    public List<FilterStateInfo> getFilters(String tag) {
        List<FilterStateInfo> filterList = null;
        try {
            filterList = JsonMapperManager.MAPPER.readValue(tag, new TypeReference<List<FilterStateInfo>>() {
            });
        } catch (IOException e) {
            InternalLogger.e("Error occurred during retrieve filters action: %s", e);
            return new ArrayList<>();
        }
        return filterList;
    }


    /*
     * Utility methods
     */

    private static final String getFilterPersistenceTag(String tag) {
        return String.format("prefs_filters_%s", StringUtils.defaultString(tag, "default"));
    }

}
