package it.infoblu.mobilemap.manager;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import it.infoblu.mobilemap.model.data.Poi;
import it.infoblu.mobilemap.model.prefs.KeyPreferences;
import it.infoblu.mobilemap.utils.InternalLogger;
import it.infoblu.mobilemap.utils.JsonMapperManager;

/**
 * Created on 16/04/18.
 */
public final class PoiPersistenceManager implements IPoiPersistenceManager {

    private static PoiPersistenceManager _instance;

    private PoiPersistenceManager() {
    }

    public static synchronized PoiPersistenceManager getInstance() {
        return _instance != null ? _instance : (_instance = new PoiPersistenceManager());
    }


    @Override
    public Boolean savePois(String poiType, List<Poi> poiList) {
        try {
            KeyPreferences.getPrefs().getString(getPoiPersistenceTag(poiType)).set(JsonMapperManager.MAPPER.writeValueAsString(poiList));
            KeyPreferences.getPrefs().getLong(getPoiTimestampPersistenceTag(poiType)).set(now());
        } catch (JsonProcessingException e) {
            InternalLogger.e("Error occurred during save pois action: %s", e);
            return false;
        }
        return null;
    }

    @Override
    public List<Poi> getPois(String poiType) {
        List<Poi> poiList = null;
        try {
            String poiListString = KeyPreferences.getPrefs().getString(getPoiPersistenceTag(poiType)).get();
            poiList = JsonMapperManager.MAPPER.readValue(poiListString, new TypeReference<List<Poi>>() {
            });
        } catch (IOException e) {
            InternalLogger.d("Error occurred during retrieve pois action: %s", e);
            KeyPreferences.getPrefs().getLong(getPoiTimestampPersistenceTag(poiType)).delete();
            return new ArrayList<>();
        }
        return poiList;
    }

    @Override
    public long getPoiPersistenceTimestamp(String poiType) {
        return KeyPreferences.getPrefs().getLong(getPoiTimestampPersistenceTag(poiType), -1L).get();
    }


    /*
     * Utility methods
     */

    private static final String getPoiTimestampPersistenceTag(String tag) {
        return String.format("prefs_poi_timestamp_%s", StringUtils.defaultString(tag, "default"));
    }

    private static final String getPoiPersistenceTag(String tag) {
        return String.format("prefs_poi_%s", StringUtils.defaultString(tag, "default"));
    }

    private static long now() {
        return Calendar.getInstance().getTimeInMillis();
    }

}
