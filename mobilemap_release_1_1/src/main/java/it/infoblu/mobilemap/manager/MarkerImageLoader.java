package it.infoblu.mobilemap.manager;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;

import com.annimon.stream.Stream;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import com.squareup.picasso.Target;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.SingleOnSubscribe;
import io.reactivex.SingleSource;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import it.infoblu.mobilemap.MobileMap;
import it.infoblu.mobilemap.R;
import it.infoblu.mobilemap.constants.MobileMapConfigServices;
import it.infoblu.mobilemap.model.data.Poi;
import it.infoblu.mobilemap.model.data.PoiMarkerDownloadResult;
import it.infoblu.mobilemap.model.remote.NetworkProvider;
import it.infoblu.mobilemap.model.repository.SessionRepository;
import it.infoblu.mobilemap.utils.AndroidUtils;
import it.infoblu.mobilemap.utils.InternalLogger;
import it.infoblu.mobilemap.utils.StringTemplateUtils;

/**
 * Created on 14/02/18.
 */

public class MarkerImageLoader implements IMarkerImageLoader {

    private final Context mContext;

    private Map<String, Integer> mMarkerIconPlaceholderMap = new HashMap<>();

    public MarkerImageLoader(Context context) {
        mContext = context;
    }


    @Override
    public void setMarkerIconPlaceholders(Map<String, Integer> markerIconPlaceholderMap) {
        mMarkerIconPlaceholderMap.clear();
        mMarkerIconPlaceholderMap.putAll(markerIconPlaceholderMap);
    }

    @Override
    public Observable<Bitmap> getMarkerBitmapBy(final Poi poi) {
        return Observable.create(emitter -> {
            int iconResId = getPlaceholderResId(poi.getFilterTag());

            getRequestCreator(poi, iconResId)
                    .placeholder(iconResId)
                    .error(iconResId)
                    .networkPolicy(NetworkPolicy.OFFLINE)
                    .into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                            emitter.onNext(bitmap);
                            emitter.onComplete();
                        }

                        @Override
                        public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                            emitter.onNext(((BitmapDrawable) errorDrawable).getBitmap());
                            emitter.onComplete();
                        }

                        @Override
                        public void onPrepareLoad(Drawable placeHolderDrawable) {
                            emitter.onNext(((BitmapDrawable) placeHolderDrawable).getBitmap());
                            emitter.onComplete();
                        }
                    });
        });
    }

    public Observable<Bitmap> getMarkerBitmapFromCacheBy(final Poi poi) {
        return Observable.create(emitter -> {
            emitter.onNext(getMarkerBitmapSyncFromCacheBy(poi));
            emitter.onComplete();
        });
    }

    public Bitmap getMarkerBitmapSyncFromCacheBy(final Poi poi) {
        int iconResId = getPlaceholderResId(poi.getFilterTag());
        try {
            return getRequestCreator(poi, iconResId)
                    .placeholder(iconResId)
                    .error(iconResId)
                    .networkPolicy(NetworkPolicy.OFFLINE)
                    .get();
        } catch (IOException e) {
            InternalLogger.d("Error in marker image retrieve: [poiId: %s, poiType: %s] %s", poi.getId(),poi.getType(), e.getMessage());
        }
        return BitmapFactory.decodeResource(mContext.getResources(), iconResId); // fallback
    }

    @Override
    public Observable<List<PoiMarkerDownloadResult>> downloadMarkerBitmapsBy(List<Poi> poiList) {
        return composeMarkerBitmapObservables(poiList)
                .flatMap((Function<ArrayList<Single<PoiMarkerDownloadResult>>, SingleSource<List<PoiMarkerDownloadResult>>>) singles ->
                        zipMarkerBitmapObservables(singles)).toObservable();
    }


    /*
     * Utility methods
     */

    private RequestCreator getRequestCreator(Poi poi, int iconResId) {
        RequestCreator requestCreator = null;
        String iconUrl = getMarkerIconUrl(poi);
        if (TextUtils.isEmpty(iconUrl)) {
            requestCreator = Picasso.get().load(iconResId);
        } else {
            requestCreator = Picasso.get().load(getMarkerIconUrl(poi));
        }
        return requestCreator;
    }

    private String getMarkerIconUrl(Poi poi) {
        if (TextUtils.isEmpty(poi.getInfo().getIcoRelativeUrl())) {
            return null;
        }
        // NB: here config is always available
        String baseUrl = NetworkProvider.decorateApiKey(MobileMap.getConfigurationSync()
                , SessionRepository.getInstance().getRemoteConfigSync().getServices().get(MobileMapConfigServices.ICONS));
        if (TextUtils.isEmpty(baseUrl)) {
            return null;
        }
        Map<String, String> map = new HashMap<>();
        map.put("ico_size", "android_".concat(AndroidUtils.getDensityString(mContext)));
        map.put("relative_url", poi.getInfo().getIcoRelativeUrl());
        return StringTemplateUtils.decorateCurlyBracket(baseUrl, map);
    }

    private int getPlaceholderResId(String placeholderName) {
        return Stream.of(mMarkerIconPlaceholderMap.get(placeholderName)
                , MobileMap.getConfigurationSync().getPoiIconPlaceholderResIdMap().get(placeholderName)
                , R.drawable.mobile_map_marker_default)
                .filter(value -> value != null).findFirst().get();
    }

    private Single<ArrayList<Single<PoiMarkerDownloadResult>>> composeMarkerBitmapObservables(final List<Poi> poiList) {
        return Single.create((SingleOnSubscribe<ArrayList<Single<PoiMarkerDownloadResult>>>) emitter -> {
            ArrayList<Single<PoiMarkerDownloadResult>> poiMarkerSingleList = new ArrayList<>();
            for (Poi poi : poiList) {
                poiMarkerSingleList.add(cacheMarkerBitmapBy(poi));
            }
            emitter.onSuccess(poiMarkerSingleList);
        }).subscribeOn(Schedulers.io());
    }

    private Single<PoiMarkerDownloadResult> cacheMarkerBitmapBy(final Poi poi) {
        return Single.create(emitter -> {
            int iconResId = getPlaceholderResId(poi.getFilterTag());
            getRequestCreator(poi, iconResId)
                    .fetch(new Callback() {
                        @Override
                        public void onSuccess() {
                            emitter.onSuccess(new PoiMarkerDownloadResult(poi));
                        }

                        @Override
                        public void onError(Exception e) {
                            emitter.onSuccess(new PoiMarkerDownloadResult(poi, e.getCause()));
                        }
                    });
        });
    }

    private Single<List<PoiMarkerDownloadResult>> zipMarkerBitmapObservables(ArrayList<Single<PoiMarkerDownloadResult>> markerBitmapObservables) {
        if (markerBitmapObservables == null || markerBitmapObservables.isEmpty()) {
            return Single.just(new ArrayList<>()); // EXIT
        }
        return Single.zip(markerBitmapObservables, objects -> {
            List<PoiMarkerDownloadResult> poiMarkerDownloadResultList = new ArrayList<>();
            for (Object result : objects) { // hardcast caused by the java zip implementation
                poiMarkerDownloadResultList.add((PoiMarkerDownloadResult) result);
            }
            return poiMarkerDownloadResultList;
        }).subscribeOn(Schedulers.io());
    }

}
