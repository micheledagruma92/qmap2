package it.infoblu.mobilemap.manager;

import android.graphics.Bitmap;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import it.infoblu.mobilemap.model.data.Poi;
import it.infoblu.mobilemap.model.data.PoiMarkerDownloadResult;

/**
 * Created on 14/02/18.
 */

public interface IMarkerImageLoader {

    void setMarkerIconPlaceholders(Map<String, Integer> markerIconPlaceholderMap);

    Observable<Bitmap> getMarkerBitmapBy(Poi poi);

    Observable<List<PoiMarkerDownloadResult>> downloadMarkerBitmapsBy(List<Poi> poiList);

    Bitmap getMarkerBitmapSyncFromCacheBy(final Poi poi);

}
