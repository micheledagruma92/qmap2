package it.infoblu.mobilemap.manager;

import java.util.List;

import it.infoblu.mobilemap.model.data.FilterStateInfo;

/**
 * Created on 16/04/18.
 */
public interface IPoiFilterPersistenceManager {

    Boolean saveFilters(List<FilterStateInfo> filterPersistenceInfoList);

    List<FilterStateInfo> getFilters();

    Boolean saveFilters(String tag, List<FilterStateInfo> filterPersistenceInfoList);

    List<FilterStateInfo> getFilters(String tag);

}
