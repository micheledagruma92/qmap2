package it.infoblu.mobilemap.manager;

import io.reactivex.subjects.PublishSubject;

/**
 * Created on 25/05/18.
 */
public final class ErrorManager {

    private ErrorManager() {
    }

    private static PublishSubject<Throwable> errorSubject = PublishSubject.create();

    public static void propagateError(Throwable error) {
        errorSubject.onNext(error);
    }

    public static PublishSubject<Throwable> getErrorObservable() {
        return errorSubject;
    }

}
