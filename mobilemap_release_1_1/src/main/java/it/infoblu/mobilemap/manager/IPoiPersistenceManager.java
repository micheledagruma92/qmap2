package it.infoblu.mobilemap.manager;

import java.util.List;

import it.infoblu.mobilemap.model.data.Poi;

/**
 * Created on 16/04/18.
 */
public interface IPoiPersistenceManager {

    Boolean savePois(String poiType, List<Poi> poiList);

    List<Poi> getPois(String poiType);

    long getPoiPersistenceTimestamp(String poiType);

}
