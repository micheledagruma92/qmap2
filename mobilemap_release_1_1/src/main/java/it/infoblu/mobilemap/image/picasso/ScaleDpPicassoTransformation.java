package it.infoblu.mobilemap.image.picasso;

import android.graphics.Bitmap;

import com.squareup.picasso.Transformation;

import it.infoblu.mobilemap.utils.ViewUtils;

/**
 * Created on 09/04/18.
 */
public class ScaleDpPicassoTransformation implements Transformation {

    @Override
    public Bitmap transform(Bitmap source) {
        int width = (int) ViewUtils.pixelToDp(source.getWidth());
        int height = (int) ViewUtils.pixelToDp(source.getHeight());
        Bitmap mod = Bitmap.createScaledBitmap(source, width, height, false);
        source.recycle();
        return mod;
    }

    @Override
    public String key() {
        return "scaleDp";
    }

}
