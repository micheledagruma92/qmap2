package it.infoblu.mobilemap.comparator;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.SphericalUtil;

import java.util.Comparator;

import it.infoblu.mobilemap.model.data.Poi;

/**
 * Created on 15/03/18.
 */

public class PoiDistanceComparator implements Comparator<Poi> {

    private final LatLng center;

    public PoiDistanceComparator(LatLng center) {
        this.center = center;
    }

    @Override
    public int compare(Poi p1, Poi p2) {
        double distance1 = SphericalUtil.computeDistanceBetween(this.center, p1.getPosition().getLatLng());
        double distance2 = SphericalUtil.computeDistanceBetween(this.center, p2.getPosition().getLatLng());
        return (int) (distance1 - distance2);
    }
}
