package it.infoblu.mobilemap.service;

import android.app.Activity;
import android.app.Application;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

import io.reactivex.ObservableSource;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.subjects.BehaviorSubject;
import it.infoblu.mobilemap.constants.MobileMapConstants;
import it.infoblu.mobilemap.model.data.RemoteConfig;
import it.infoblu.mobilemap.model.repository.SessionRepository;
import it.infoblu.mobilemap.rx.RxTickUtils;
import it.infoblu.mobilemap.utils.InternalLogger;

/**
 * Created on 05/02/18.
 */

public class SessionRoutineService extends Service {
    public static final String TAG = SessionRoutineService.class.getName();

    private CompositeDisposable mDisposables;
    private CompositeDisposable mPoiRoutineDisposables;

    private BehaviorSubject<Boolean> mForegroundSubject;

    private Application.ActivityLifecycleCallbacks mUILifecycle = createLifecycleCallbacks();

    private static long POI_REFRESH_RETRY_COUNT = 3;
    private static long POI_UNTIL_RETRY_COUNT = 3;
    private static long POI_UNTIL_RETRY_PERIOD = 60 * 1000;


    public static void start(Context context) {
        Intent starter = new Intent(context, SessionRoutineService.class);
        context.startService(starter);
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mForegroundSubject = BehaviorSubject.createDefault(true); // assume the app is in foreground when the service is created
        getApplication().registerActivityLifecycleCallbacks(mUILifecycle);

        mDisposables = new CompositeDisposable();
        mPoiRoutineDisposables = new CompositeDisposable();
        mDisposables.add(mPoiRoutineDisposables);

        mDisposables.add(SessionRepository.getInstance().startRemoteConfigRoutine()
                .onErrorReturn(throwable -> {
                    InternalLogger.e(throwable);
                    return new RemoteConfig();
                })
                .subscribe(this::managePoiRoutine));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getApplication().unregisterActivityLifecycleCallbacks(mUILifecycle);

        mDisposables.dispose();
        mDisposables = null;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        // nothing to do here (??)
        return null;
    }


    /*
     * Internal methods
     */

    private void managePoiRoutine(RemoteConfig poiConfig) {
        InternalLogger.d("Start Poi routines");

        mPoiRoutineDisposables.clear(); // flush previous poi requests

        for (final RemoteConfig.PlaceService poiInfo : poiConfig.getPlaces()) {
            if (poiInfo.getUntil() > 0) {
                managePoiRoutineUntil(poiInfo);
            } else if (poiInfo.getRefresh() > 0) {
                managePoiRoutineRefresh(poiInfo);
            } else {
                InternalLogger.e("No poi flow routine found for: %s", poiInfo);
            }
        }
    }

    private void managePoiRoutineUntil(final RemoteConfig.PlaceService poiInfo) {
        mPoiRoutineDisposables.add(SessionRepository.getInstance().getPoiWithPersistence(poiInfo.getPoiType(), poiInfo.getEndpoint(), poiInfo.getUntil())
                .retryWhen(throwableObservable -> RxTickUtils.createTickObservable(POI_UNTIL_RETRY_COUNT, POI_UNTIL_RETRY_PERIOD))
                .onErrorReturn(throwable -> {
                    InternalLogger.e(throwable);
                    return new ArrayList<>();
                })
                .subscribe());
    }

    private void managePoiRoutineRefresh(final RemoteConfig.PlaceService poiInfo) {
        mPoiRoutineDisposables.add(SessionRepository.getInstance().getPoi(poiInfo.getPoiType(), poiInfo.getEndpoint())
                .retryWhen(throwableObservable -> RxTickUtils.createTickObservable(POI_REFRESH_RETRY_COUNT
                        , poiInfo.getRefresh() / Math.max(1, POI_REFRESH_RETRY_COUNT - 1)))
                .repeatWhen(objectObservable -> createTickPredicate(poiInfo.getRefresh()))
                .onErrorReturn(throwable -> {
                    InternalLogger.e(throwable);
                    return new ArrayList<>();
                })
                .subscribe());
    }


    /*
     * Internal methods
     */

    @NonNull
    private Application.ActivityLifecycleCallbacks createLifecycleCallbacks() {
        return new Application.ActivityLifecycleCallbacks() {
            private Class<? extends Activity> lastActivityClazz;

            @Override
            public void onActivityCreated(Activity activity, Bundle bundle) {
            }

            @Override
            public void onActivityStarted(Activity activity) {
                if (lastActivityClazz == null || activity.getClass().isInstance(lastActivityClazz)) {
                    mForegroundSubject.onNext(true);
                }
            }

            @Override
            public void onActivityResumed(Activity activity) {
                lastActivityClazz = activity.getClass();
            }

            @Override
            public void onActivityPaused(Activity activity) {
            }

            @Override
            public void onActivityStopped(Activity activity) {
                if (lastActivityClazz == null || activity.getClass().isInstance(lastActivityClazz)) {
                    mForegroundSubject.onNext(false);
                }
            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
            }

            @Override
            public void onActivityDestroyed(Activity activity) {
            }
        };
    }

    private ObservableSource<?> createTickPredicate(long periodSec) {
        if (periodSec > 0 && periodSec < MobileMapConstants.SERVICE_MIN_REFRESH_TIME) {
            InternalLogger.d("Tick period of %s is too short, it will be forced at 30 seconds", periodSec);
            periodSec = MobileMapConstants.SERVICE_MIN_REFRESH_TIME;
        } // 0 value not managed, it is skipped in RxTickUtils
        return RxTickUtils.createTickObservable(periodSec, mForegroundSubject);
    }

}
