package it.infoblu.mobilemap.rx;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.subjects.PublishSubject;

/**
 * Created on 18/06/18.
 */
public class RxTickUtils {

    public static Flowable<Long> createTickFlowable(long times, long delay) {
        return Flowable.intervalRange(0, times, delay, delay, TimeUnit.MILLISECONDS);
    }

    public static Observable<Long> createTickObservable(long times, long delay) {
        return Observable.intervalRange(0, times, delay, delay, TimeUnit.MILLISECONDS);
    }

    public static ObservableSource<?> createTickObservable(long periodSec, final Observable<Boolean> foregroundSubject) {
        if (periodSec <= 0) {
            return Observable.empty(); // EXIT: safe check to avoid dangerous polling
        }

        final PublishSubject<Long> intervalGeneratorSubject = PublishSubject.create();
        final AtomicBoolean emitSkipped = new AtomicBoolean(false);

        /* Merge two part of the condition:
         * the first one trigger at every interval tick if the app is in foreground
         * the second part trigger only when the app returns from background and if a tick was previously skipped
         */
        final long finalPeriodSec = periodSec;
        return Observable.merge(intervalGeneratorSubject.startWith(-1L)
                        // create a new interval to reset the period
                        .flatMap(new Function<Long, ObservableSource<Boolean>>() {
                            @Override
                            public ObservableSource<Boolean> apply(Long aLong) throws Exception {
                                return Observable.combineLatest(
                                        foregroundSubject
                                        , Observable.interval(finalPeriodSec, TimeUnit.MILLISECONDS)
                                        , new BiFunction<Boolean, Long, Boolean>() {
                                            @Override
                                            public Boolean apply(Boolean isForeground, Long aLong) throws Exception {
                                                return isForeground;
                                            }
                                        });
                            }
                        })
                        .filter(new Predicate<Boolean>() {
                            @Override
                            public boolean test(Boolean isForeground) throws Exception {
                                if (!isForeground) {
                                    // propagate to the second part of the merge
                                    emitSkipped.set(true);
                                }
                                return isForeground; // prevent emit if the app is in background (resumed == false)
                            }
                        })
                , foregroundSubject
                        // it has to be triggered only if the app returns from background and if a previous emit was skipped
                        .filter(new Predicate<Boolean>() {
                            @Override
                            public boolean test(Boolean resumed) throws Exception {
                                // n00b note: remember, the second condition is evaluated only if the first is true
                                return resumed && emitSkipped.compareAndSet(true, false);
                            }
                        })
                        .map(new Function<Boolean, Long>() {
                            @Override
                            public Long apply(Boolean aBoolean) throws Exception {
                                // trigger the reset of tick period
                                intervalGeneratorSubject.onNext(-1L);
                                return -1L;
                            }
                        }));
    }

}