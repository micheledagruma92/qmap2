package it.infoblu.mobilemap.utils;

import java.util.List;

import it.infoblu.mobilemap.model.data.RemoteConfig;

/**
 * Created on 16/04/18.
 */
public final class FilterUtils {

    private static final boolean DEFAULT_FILTER_VISIBILITY = true;

    private FilterUtils() {
    }

    public static boolean isPoiTypeVisible(List<RemoteConfig.MapFilter> filterList, String poiType) {
        if (filterList == null) {
            return DEFAULT_FILTER_VISIBILITY; // EXIT: default is visible
        }
        for (RemoteConfig.MapFilter mapFilter : filterList) {
            if (mapFilter.getPoiTypeList() == null) {
                continue; // SKIP
            }
            if (mapFilter.getPoiTypeList().contains(poiType)) {
                return mapFilter.isEnabled();
            }
        }
        return DEFAULT_FILTER_VISIBILITY;
    }

}
