package it.infoblu.mobilemap.utils;

import com.orhanobut.logger.Logger;

import it.infoblu.mobilemap.manager.ErrorManager;

/**
 * Created on 19/07/18.
 */
public final class InternalLogger {

    private InternalLogger() {
    }


    public static void d(String log, Object... args) {
        Logger.d(log, args);
    }

    public static void e(String log, Object... args) {
        String logFormatted = String.format(log, args);
        Logger.e(logFormatted);
        propagateError(new RuntimeException(logFormatted));
    }

    public static void e(Throwable throwable) {
        Logger.e(throwable, "MobileMap intercepted exception");
        propagateError(throwable);
    }


    /*
     * Internal methods
     */

    private static void propagateError(Throwable throwable) {
        ErrorManager.propagateError(throwable);
    }

}
