package it.infoblu.mobilemap.utils;


import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created on 13/07/17.
 */

public class StringTemplateUtils {

    public static String decorate(String text, Map<String, String> map, String leftDelimiter, String rightDelimiter) {
        String[] placeholders = new String[map.keySet().size()];

        int i = 0;
        for (String tag : map.keySet()) {
            placeholders[i] = leftDelimiter + tag + rightDelimiter;
            i++;
        }

        return StringUtils.replaceEach(text
                , placeholders
                , map.values().toArray(new String[0]));
    }


    public static String decorateCurlyBracket(String text, String placeholder, String value) {
        Map<String, String> map = new HashMap<>();
        map.put(placeholder, value);
        return decorateCurlyBracket(text, map);
    }

    public static String decorateSquareBracket(String text, String placeholder, String value) {
        Map<String, String> map = new HashMap<>();
        map.put(placeholder, value);
        return decorateSquareBracket(text, map);
    }

    public static String decorateCurlyBracket(String text, Map<String, String> map) {
        return decorate(text, map, "{", "}");
    }

    public static String decorateSquareBracket(String text, Map<String, String> map) {
        return decorate(text, map, "[", "]");
    }

}
