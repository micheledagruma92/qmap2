package it.infoblu.mobilemap.utils;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.util.List;


public final class BoundsUtils {

    private static final double ASSUMED_INIT_LATLNG_DIFF = 1.0;
    private static final float ACCURACY = 0.01f;


    private BoundsUtils() {
    }

    /**
     * @param center
     * @param rangeInMeters
     * @return
     */
    public static LatLngBounds createBoundsWithCenterAndLatLngDistance(LatLng center, float rangeInMeters) {
        return createBoundsWithCenterAndLatLngDistance(center, rangeInMeters, rangeInMeters);
    }


    /**
     * @param center
     * @param latDistanceInMeters
     * @param lngDistanceInMeters
     * @return
     */
    public static LatLngBounds createBoundsWithCenterAndLatLngDistance(LatLng center, float latDistanceInMeters, float lngDistanceInMeters) {

        if (center == null) return null; // EXIT

        latDistanceInMeters /= 2;
        lngDistanceInMeters /= 2;
        LatLngBounds.Builder builder = LatLngBounds.builder();
        float[] distance = new float[1];
        {
            boolean foundMax = false;
            double foundMinLngDiff = 0;
            double assumedLngDiff = ASSUMED_INIT_LATLNG_DIFF;
            do {
                Location.distanceBetween(center.latitude, center.longitude, center.latitude, center.longitude + assumedLngDiff, distance);
                float distanceDiff = distance[0] - lngDistanceInMeters;
                if (distanceDiff < 0) {
                    if (!foundMax) {
                        foundMinLngDiff = assumedLngDiff;
                        assumedLngDiff *= 2;
                    } else {
                        double tmp = assumedLngDiff;
                        assumedLngDiff += (assumedLngDiff - foundMinLngDiff) / 2;
                        foundMinLngDiff = tmp;
                    }
                } else {
                    assumedLngDiff -= (assumedLngDiff - foundMinLngDiff) / 2;
                    foundMax = true;
                }
            } while (Math.abs(distance[0] - lngDistanceInMeters) > lngDistanceInMeters * ACCURACY);
            LatLng east = new LatLng(center.latitude, center.longitude + assumedLngDiff);
            builder.include(east);
            LatLng west = new LatLng(center.latitude, center.longitude - assumedLngDiff);
            builder.include(west);
        }
        {
            boolean foundMax = false;
            double foundMinLatDiff = 0;
            double assumedLatDiffNorth = ASSUMED_INIT_LATLNG_DIFF;
            do {
                Location.distanceBetween(center.latitude, center.longitude, center.latitude + assumedLatDiffNorth, center.longitude, distance);
                float distanceDiff = distance[0] - latDistanceInMeters;
                if (distanceDiff < 0) {
                    if (!foundMax) {
                        foundMinLatDiff = assumedLatDiffNorth;
                        assumedLatDiffNorth *= 2;
                    } else {
                        double tmp = assumedLatDiffNorth;
                        assumedLatDiffNorth += (assumedLatDiffNorth - foundMinLatDiff) / 2;
                        foundMinLatDiff = tmp;
                    }
                } else {
                    assumedLatDiffNorth -= (assumedLatDiffNorth - foundMinLatDiff) / 2;
                    foundMax = true;
                }
            } while (Math.abs(distance[0] - latDistanceInMeters) > latDistanceInMeters * ACCURACY);
            LatLng north = new LatLng(center.latitude + assumedLatDiffNorth, center.longitude);
            builder.include(north);
        }
        {
            boolean foundMax = false;
            double foundMinLatDiff = 0;
            double assumedLatDiffSouth = ASSUMED_INIT_LATLNG_DIFF;
            do {
                Location.distanceBetween(center.latitude, center.longitude, center.latitude - assumedLatDiffSouth, center.longitude, distance);
                float distanceDiff = distance[0] - latDistanceInMeters;
                if (distanceDiff < 0) {
                    if (!foundMax) {
                        foundMinLatDiff = assumedLatDiffSouth;
                        assumedLatDiffSouth *= 2;
                    } else {
                        double tmp = assumedLatDiffSouth;
                        assumedLatDiffSouth += (assumedLatDiffSouth - foundMinLatDiff) / 2;
                        foundMinLatDiff = tmp;
                    }
                } else {
                    assumedLatDiffSouth -= (assumedLatDiffSouth - foundMinLatDiff) / 2;
                    foundMax = true;
                }
            } while (Math.abs(distance[0] - latDistanceInMeters) > latDistanceInMeters * ACCURACY);
            LatLng south = new LatLng(center.latitude - assumedLatDiffSouth, center.longitude);
            builder.include(south);
        }
        return builder.build();
    }


    /**
     * @param positions
     * @return
     */
    public static LatLngBounds createBounds(List<LatLng> positions) {
        LatLngBounds.Builder builder = LatLngBounds.builder();
        for (LatLng position : positions) {
            builder.include(position);
        }
        return builder.build();
    }


    /**
     * @param points
     * @return
     */
    public static LatLngBounds createBounds(LatLng... points) {
        LatLngBounds.Builder builder = LatLngBounds.builder();
        for (LatLng point : points) {
            builder.include(point);
        }
        return builder.build();
    }


    /**
     * @param bounds
     * @param boundsToIntesect
     * @return
     */
    public static boolean intersects(LatLngBounds bounds, LatLngBounds boundsToIntesect) {
        return boundsToIntesect.southwest.longitude < bounds.northeast.longitude
                && bounds.southwest.longitude < boundsToIntesect.northeast.longitude
                && boundsToIntesect.southwest.latitude < bounds.northeast.latitude
                && bounds.southwest.latitude < boundsToIntesect.northeast.latitude
                ;
    }


    /**
     * @param bounds
     * @param boundsContained
     * @return
     */
    public static boolean contains(LatLngBounds bounds, LatLngBounds boundsContained) {
        return bounds.contains(boundsContained.northeast)
                && bounds.contains(boundsContained.southwest)
                && bounds.contains(getNorthWestLatLng(boundsContained))
                && bounds.contains(getSouthEastLatLng(boundsContained))
                ;
    }


    /**
     * @param bounds
     * @return
     */
    public static LatLng getNorthWestLatLng(LatLngBounds bounds) {
        return new LatLng(bounds.southwest.latitude, bounds.northeast.longitude);
    }


    /**
     * @param bounds
     * @return
     */
    public static LatLng getSouthEastLatLng(LatLngBounds bounds) {
        return new LatLng(bounds.northeast.latitude, bounds.southwest.longitude);
    }


    /**
     *
     * @param bounds
     * @return
     */
    public static LatLngBounds convert(com.mapbox.mapboxsdk.geometry.LatLngBounds bounds) {
        return new LatLngBounds(new LatLng(bounds.getLatSouth(), bounds.getLonWest())
                , new LatLng(bounds.getLatNorth(), bounds.getLonWest()));
    }

}
