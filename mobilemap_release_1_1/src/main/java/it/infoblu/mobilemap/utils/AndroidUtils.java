package it.infoblu.mobilemap.utils;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.provider.Settings;
import android.view.View;

import com.orhanobut.logger.Logger;

/**
 * Created on 19/06/17.
 */

public class AndroidUtils {

    private static String deviceId;

    private AndroidUtils() {
    }

    public static void prefetchDeviceId(Context context) {
        getDeviceId(context);
    }

    public static String getDeviceId(Context context) {
        return deviceId != null ? deviceId : (deviceId = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID));
    }

    public static String getDeviceId() {
        if (deviceId == null) {
            Logger.e("CRITICAL ERROR: deviceId not initialized!");
        }
        return deviceId;
    }

    public static String getDensityString(Context context) {
        float density = context.getResources().getDisplayMetrics().density;
        if (density < 1) return "ldpi";
        if (density < 1.5) return "mdpi";
        if (density < 2.0) return "hdpi";
        if (density < 3.0) return "xhdpi";
        if (density < 4.0) return "xxhdpi";
        if (density >= 4.0) return "xxxhdpi";
        return null; // something went wrong
    }

    public static Activity extractActivity(View view) {
        Context context = view.getContext();
        while (context instanceof ContextWrapper) {
            if (context instanceof Activity) {
                return (Activity) context;
            }
            context = ((ContextWrapper) context).getBaseContext();
        }
        return null;
    }

}
