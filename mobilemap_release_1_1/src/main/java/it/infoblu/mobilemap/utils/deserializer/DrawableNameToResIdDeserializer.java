package it.infoblu.mobilemap.utils.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;

import it.infoblu.mobilemap.model.local.ResourceLoader;

/**
 *
 */
public class DrawableNameToResIdDeserializer extends JsonDeserializer<Integer> {

    @Override
    public Integer deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        JsonNode node = jp.getCodec().readTree(jp);
        String drawableName = node.asText();
        return ResourceLoader.getDrawableResId(drawableName);
    }
}