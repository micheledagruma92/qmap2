package it.infoblu.mobilemap.constants;

/**
 * Created on 23/04/18.
 */
public final class FilterConstants {

    private FilterConstants() {
    }

    public static final String FILTER_TRAFFIC_LAYER_ID = "layer_traffic";

}
