package it.infoblu.mobilemap.constants;

/**
 * Created on 23/02/18.
 */

public final class MobileMapSettingsConfig {

    private MobileMapSettingsConfig() {
    }

    public static final String SETTING_2D = "2D";
    public static final String SETTING_3D = "3D";
    public static final String SETTING_NAV = "Nav";

}
