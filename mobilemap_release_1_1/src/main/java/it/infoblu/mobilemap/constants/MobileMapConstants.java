package it.infoblu.mobilemap.constants;

/**
 * Created on 21/02/18.
 */

public final class MobileMapConstants {

    private MobileMapConstants() {
    }

    public static final long SERVICE_MIN_REFRESH_TIME = 30 * 1000;

    public static final double CENTER_INIT_ZOOM = 17f;

    public static final double NAV_INIT_ZOOM = 16f;

}
