package it.infoblu.mobilemap;

import android.content.Context;
import android.text.TextUtils;

import androidx.annotation.UiThread;

import com.mapbox.mapboxsdk.Mapbox;
import com.orhanobut.logger.AndroidLogAdapter;
import com.orhanobut.logger.Logger;

import org.apache.commons.lang3.StringUtils;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;
import it.infoblu.mobilemap.manager.ErrorManager;
import it.infoblu.mobilemap.model.data.MobileMapConfiguration;
import it.infoblu.mobilemap.model.local.ResourceLoader;
import it.infoblu.mobilemap.model.prefs.KeyPreferences;
import it.infoblu.mobilemap.model.repository.SessionRepository;
import it.infoblu.mobilemap.service.SessionRoutineService;
import it.infoblu.mobilemap.utils.InternalLogger;


/**
 * Created on 05/02/18.
 */

public final class MobileMap {

    private static final String MAPBOX_DEFAULT_TOKEN = "pk.fake_token";

    private static Disposable sStartServiceDisposable;

    private static BehaviorSubject<MobileMapConfiguration> sConfigurationSubject = BehaviorSubject.create();


    private MobileMap() {
    }


    public static void prefetch(Context context) {
        prefetch(context, null);
    }

    /**
     * It should be added to avoid crashes at MapView recreation after a process kill event
     * if the MobileMap#init method is called in async way
     *
     * @param context
     */
    public static void prefetch(Context context, String mapBoxAccessToken) {
        KeyPreferences.init(context.getApplicationContext());
        ResourceLoader.init(context.getApplicationContext());
        Mapbox.getInstance(context.getApplicationContext(), StringUtils.defaultString(mapBoxAccessToken, MAPBOX_DEFAULT_TOKEN));
    }

    @UiThread
    public static void init(Context context, MobileMapConfiguration configuration) {
        init(context, configuration, null);
    }

    @UiThread
    public static void init(Context context, MobileMapConfiguration configuration, String mapBoxAccessToken) {
        validateConfiguration(configuration);
        sConfigurationSubject.onNext(configuration);
        prefetch(context, mapBoxAccessToken);
        initLogger(configuration.isLogEnabled());
    }

    public static void startService(Context context) {
        if (sStartServiceDisposable != null) {
            sStartServiceDisposable.dispose();
        }
        sStartServiceDisposable = getConfigurationSingle()
                .subscribe(mobileMapConfiguration -> {
                    try {
                        SessionRoutineService.start(context);
                    } catch (Throwable throwable) {
                        InternalLogger.e(throwable);
                    }
                });
    }

    public static Observable<MobileMapConfiguration> getConfigurationObservable() {
        return sConfigurationSubject;
    }

    public static Single<MobileMapConfiguration> getConfigurationSingle() {
        return getConfigurationObservable()
                .take(1).singleOrError()
                .map(mobileMapConfiguration -> {
                    validateConfiguration(mobileMapConfiguration);
                    return mobileMapConfiguration;
                });
    }

    public static MobileMapConfiguration getConfigurationSync() {
        validateConfiguration(sConfigurationSubject.getValue());
        return sConfigurationSubject.getValue();
    }

    public static IMobileMapSession getSession() {
        return SessionRepository.getInstance();
    }

    // TODO: gestire nel progetto
    public static PublishSubject<Throwable> getErrorObservable() {
        return ErrorManager.getErrorObservable();
    }


    /*
     * Internal methods
     */

    private static void initLogger(final boolean logEnabled) {
        Logger.addLogAdapter(new AndroidLogAdapter() {
            @Override
            public boolean isLoggable(int priority, String tag) {
                return logEnabled;
            }
        });
    }


    /*
     * Utility methods
     */

    private static void validateConfiguration(MobileMapConfiguration configuration) {
        if (configuration == null) {
            throw new RuntimeException("Configuration must be not null!");
        }
        if (TextUtils.isEmpty(configuration.getApikey())) {
            throw new RuntimeException("An apikey must be provided!");
        }
        if (configuration.getPoiConfigDefaultRawId() == 0
                && TextUtils.isEmpty(configuration.getPoiConfigEndpoint())) {
            throw new RuntimeException("Default configuration raw id or configuration endpoint must be populated!");
        }
    }

}
