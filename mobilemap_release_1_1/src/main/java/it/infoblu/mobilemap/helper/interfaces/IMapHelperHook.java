package it.infoblu.mobilemap.helper.interfaces;


import it.infoblu.mobilemap.IMobileMapSession;

/**
 * Created on 21/02/18.
 */

public interface IMapHelperHook {

    void bind(IMapsHelper mapsHelper, IMobileMapSession session);

    void unbind();

}
