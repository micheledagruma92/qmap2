package it.infoblu.mobilemap.helper.listeners;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created on 20/03/18.
 */

public interface OnMapClickListener {

    void onMapClick(LatLng point);

}
