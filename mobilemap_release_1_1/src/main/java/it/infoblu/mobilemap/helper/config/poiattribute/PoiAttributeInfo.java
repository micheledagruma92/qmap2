package it.infoblu.mobilemap.helper.config.poiattribute;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created on 04/07/18.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PoiAttributeInfo implements Parcelable {

    private int zoomMin;

    private int zoomMax;

    private int poiGeometryZoomMin;

    @JsonProperty("ico_anchor")
    private String iconAnchor;

    public int getZoomMin() {
        return zoomMin;
    }

    public void setZoomMin(int zoomMin) {
        this.zoomMin = zoomMin;
    }

    public int getZoomMax() {
        return zoomMax;
    }

    public void setZoomMax(int zoomMax) {
        this.zoomMax = zoomMax;
    }

    public int getPoiGeometryZoomMin() {
        return poiGeometryZoomMin;
    }

    public void setPoiGeometryZoomMin(int poiGeometryZoomMin) {
        this.poiGeometryZoomMin = poiGeometryZoomMin;
    }

    public String getIconAnchor() {
        return iconAnchor;
    }

    public void setIconAnchor(String iconAnchor) {
        this.iconAnchor = iconAnchor;
    }

    @Override
    public String toString() {
        return "PoiTypeAttributeBundle{" +
                "zoomMin=" + zoomMin +
                ", zoomMax=" + zoomMax +
                ", poiGeometryZoomMin=" + poiGeometryZoomMin +
                ", iconAnchor='" + iconAnchor + '\'' +
                '}';
    }


    public static class Builder {

        private PoiAttributeInfo poiAttributeInfo;

        public Builder() {
            poiAttributeInfo = new PoiAttributeInfo();
        }

        public PoiAttributeInfo.Builder zoomMin(int zoomMin) {
            poiAttributeInfo.setZoomMin(zoomMin);
            return this;
        }

        public PoiAttributeInfo.Builder zoomMax(int zoomMax) {
            poiAttributeInfo.setZoomMax(zoomMax);
            return this;
        }

        public PoiAttributeInfo.Builder iconAnchor(String iconAnchor) {
            poiAttributeInfo.setIconAnchor(iconAnchor);
            return this;
        }

        public PoiAttributeInfo.Builder poiGeometryZoomMin(int poiGeometryZoomMin) {
            poiAttributeInfo.setPoiGeometryZoomMin(poiGeometryZoomMin);
            return this;
        }

        public PoiAttributeInfo build() {
            return poiAttributeInfo;
        }

    }


    /*
     * Parcelable
     */

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.zoomMin);
        dest.writeInt(this.zoomMax);
        dest.writeInt(this.poiGeometryZoomMin);
        dest.writeString(this.iconAnchor);
    }

    public PoiAttributeInfo() {
    }

    protected PoiAttributeInfo(Parcel in) {
        this.zoomMin = in.readInt();
        this.zoomMax = in.readInt();
        this.poiGeometryZoomMin = in.readInt();
        this.iconAnchor = in.readString();
    }

    public static final Creator<PoiAttributeInfo> CREATOR = new Creator<PoiAttributeInfo>() {
        @Override
        public PoiAttributeInfo createFromParcel(Parcel source) {
            return new PoiAttributeInfo(source);
        }

        @Override
        public PoiAttributeInfo[] newArray(int size) {
            return new PoiAttributeInfo[size];
        }
    };

}
