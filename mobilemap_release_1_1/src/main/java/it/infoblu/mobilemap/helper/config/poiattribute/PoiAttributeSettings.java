package it.infoblu.mobilemap.helper.config.poiattribute;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import it.infoblu.mobilemap.utils.InternalLogger;
import it.infoblu.mobilemap.utils.JsonMapperManager;

/**
 * Created on 04/07/18.
 */
@JsonDeserialize(using = PoiAttributeSettingsDeserializer.class)
public class PoiAttributeSettings implements IPoiAttributeSettings, Parcelable {

    private final static String KEY_DEFAULT = "def";

    protected Map<String, PoiAttributeInfo> poiType2PoiAttributeMap = new HashMap<>();


    protected PoiAttributeSettings() {
    }

    @Override
    public PoiAttributeInfo getPoiAttributeInfo(String poiType) {
        PoiAttributeInfo poiAttributeInfo = poiType2PoiAttributeMap.get(poiType);
        return poiAttributeInfo == null ? poiType2PoiAttributeMap.get(KEY_DEFAULT) : poiAttributeInfo;
    }

    public static PoiAttributeInfo parseJson(InputStream jsonInputStream) {
        try {
            return JsonMapperManager.MAPPER.readValue(jsonInputStream, PoiAttributeInfo.class);
        } catch (IOException e) {
            InternalLogger.e("json is not well formatted: %s", e.getMessage());
        }
        return null; // ERR
    }


    @Override
    public String toString() {
        return "PoiAttributeSettings{" +
                "poiType2PoiAttributeMap=" + poiType2PoiAttributeMap +
                '}';
    }

    public static class Builder {

        private final PoiAttributeSettings settings;

        public Builder() {
            settings = new PoiAttributeSettings();
        }

        public PoiAttributeSettings.Builder addPoiAttributeInfo(String poiType, PoiAttributeInfo poiAttributeInfo) {
            settings.poiType2PoiAttributeMap.put(poiType, poiAttributeInfo);
            return this;
        }

        public PoiAttributeSettings build() {
            return settings;
        }

    }


    /*
     * Parcelable
     */

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.poiType2PoiAttributeMap.size());
        for (Map.Entry<String, PoiAttributeInfo> entry : this.poiType2PoiAttributeMap.entrySet()) {
            dest.writeString(entry.getKey());
            dest.writeParcelable(entry.getValue(), flags);
        }
    }

    protected PoiAttributeSettings(Parcel in) {
        int poiType2PoiAttributeMapSize = in.readInt();
        this.poiType2PoiAttributeMap = new HashMap<String, PoiAttributeInfo>(poiType2PoiAttributeMapSize);
        for (int i = 0; i < poiType2PoiAttributeMapSize; i++) {
            String key = in.readString();
            PoiAttributeInfo value = in.readParcelable(PoiAttributeInfo.class.getClassLoader());
            this.poiType2PoiAttributeMap.put(key, value);
        }
    }

    public static final Parcelable.Creator<PoiAttributeSettings> CREATOR = new Parcelable.Creator<PoiAttributeSettings>() {
        @Override
        public PoiAttributeSettings createFromParcel(Parcel source) {
            return new PoiAttributeSettings(source);
        }

        @Override
        public PoiAttributeSettings[] newArray(int size) {
            return new PoiAttributeSettings[size];
        }
    };

}
