package it.infoblu.mobilemap.helper.config.poiattribute;

/**
 * Created on 21/03/18.
 */

public interface IPoiAttributeSettings {

    PoiAttributeInfo getPoiAttributeInfo(String poiType);

}
