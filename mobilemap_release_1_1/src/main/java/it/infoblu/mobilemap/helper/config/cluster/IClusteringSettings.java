package it.infoblu.mobilemap.helper.config.cluster;

/**
 * Created on 21/03/18.
 */

public interface IClusteringSettings {

    ClusterInfo getClusterInfo(String poiType);

}
