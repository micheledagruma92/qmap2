package it.infoblu.mobilemap.helper.mapbox.structures;

import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;

public class MarkerWrapper {

    private Marker marker;

    private MarkerOptions options;

    private boolean visible;

    public MarkerWrapper(Marker marker, MarkerOptions options) {
        this.marker = marker;
        this.options = options;
        this.visible = true;
    }

    public Marker getMarker() {
        return marker;
    }

    public void setMarker(Marker marker) {
        this.marker = marker;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public MarkerOptions getOptions() {
        return options;
    }

    public void setOptions(MarkerOptions options) {
        this.options = options;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MarkerWrapper that = (MarkerWrapper) o;

        return marker != null ? marker.equals(that.marker) : that.marker == null;
    }

    @Override
    public int hashCode() {
        return marker != null ? marker.hashCode() : 0;
    }
}