package it.infoblu.mobilemap.helper.listeners;

/**
 * Created on 07/05/18.
 */
public interface IMapHelperPermissionListener {

    void onPermissionRequest(String permission, boolean granted);

}
