package it.infoblu.mobilemap.helper.config.poigeometry;

/**
 * Created on 21/03/18.
 */

public interface IPoiGeometrySettings {

    PoiGeometryInfo getPoiGeometryInfo(String poiType);

}
