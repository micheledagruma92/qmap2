package it.infoblu.mobilemap.helper.mapbox.action;

import org.apache.commons.lang3.exception.ExceptionUtils;

import io.reactivex.Observable;
import it.infoblu.mobilemap.helper.interfaces.IMapAction;
import it.infoblu.mobilemap.helper.mapbox.interfaces.IMapsMapBoxActionBinder;
import it.infoblu.mobilemap.utils.InternalLogger;

/**
 * Created on 28/06/18.
 */
public class MapBoxErrorAction extends AMapBoxAction {

    private final Throwable mThrowable;

    public MapBoxErrorAction(Throwable throwable, IMapsMapBoxActionBinder binder) {
        super(binder);
        mThrowable = throwable;
    }

    @Override
    public Observable<IMapAction> exec() {
        InternalLogger.e("MapBoxErrorAction: %s", ExceptionUtils.getStackTrace(mThrowable));
        return Observable.just(this);
    }

}
