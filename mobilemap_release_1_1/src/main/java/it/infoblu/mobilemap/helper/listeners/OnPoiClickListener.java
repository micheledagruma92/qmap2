package it.infoblu.mobilemap.helper.listeners;

import it.infoblu.mobilemap.model.data.Poi;

/**
 * Created on 20/03/18.
 */

public interface OnPoiClickListener {

    void onPoiClick(Poi poi);

}
