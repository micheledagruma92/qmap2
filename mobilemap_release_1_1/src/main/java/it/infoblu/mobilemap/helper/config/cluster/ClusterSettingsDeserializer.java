package it.infoblu.mobilemap.helper.config.cluster;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.util.Map;

public class ClusterSettingsDeserializer extends JsonDeserializer<ClusteringSettings> {

    @Override
    public ClusteringSettings deserialize(JsonParser jp, DeserializationContext ctxt)
            throws IOException, JsonProcessingException {

        ClusteringSettings instance = new ClusteringSettings();
        Map<String, ClusterInfo> internalStructure = jp.readValueAs(new TypeReference<Map<String, ClusterInfo>>() {
        });
        instance.poiType2ClusterInfoMap.putAll(internalStructure);

        return instance;
    }
}