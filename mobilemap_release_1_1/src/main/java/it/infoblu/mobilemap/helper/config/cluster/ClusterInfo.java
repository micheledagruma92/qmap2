package it.infoblu.mobilemap.helper.config.cluster;

import android.text.TextUtils;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.collect.ImmutableMap;

import java.util.HashMap;
import java.util.Map;

import it.infoblu.mobilemap.utils.deserializer.DrawableNameToResIdDeserializer;

/**
 * Created on 21/03/18.
 */

public class ClusterInfo {

    @JsonProperty("rangeInfo")
    private Map<Integer, RangeInfo> range2InfoMap = new HashMap<>();

    private int zoomMax;

    private int radius;

    private String textFont;

    public ClusterInfo() {
    }

    public ImmutableMap<Integer, RangeInfo> getRange2InfoMap() {
        return ImmutableMap.copyOf(range2InfoMap);
    }

    public int getZoomMax() {
        return zoomMax;
    }

    public int getRadius() {
        return radius;
    }

    public String getTextFont() {
        return textFont;
    }

    @Override
    public String toString() {
        return "ClusterInfo{" +
                "range2InfoMap=" + range2InfoMap +
                ", zoomMax=" + zoomMax +
                ", radius=" + radius +
                ", textFont='" + textFont + '\'' +
                '}';
    }


    public static class Builder {

        private ClusterInfo clusterInfo;

        public Builder() {
            clusterInfo = new ClusterInfo();
        }

        public Builder addRangeInfo(int range, RangeInfo info) {
            clusterInfo.range2InfoMap.put(range, info);
            return this;
        }

        public Builder zoomMax(int zoomMax) {
            clusterInfo.zoomMax = zoomMax;
            return this;
        }

        public Builder radius(int radius) {
            clusterInfo.radius = radius;
            return this;
        }

        public Builder textFont(String textFont) {
            clusterInfo.textFont = textFont;
            return this;
        }

        public ClusterInfo build() {
            if (clusterInfo.getZoomMax() <= 0) {
                throw new RuntimeException(String.format("ClusterInfo zoomMax is %s, it should be major than 0", clusterInfo.getZoomMax()));
            }
            if (clusterInfo.getRadius() <= 0) {
                throw new RuntimeException(String.format("ClusterInfo radius is %s, it should be major than 0", clusterInfo.getRadius()));
            }
            if (TextUtils.isEmpty(clusterInfo.getTextFont())) {
                throw new RuntimeException("ClusterInfo textFont is empty");
            }
            if (clusterInfo.getRange2InfoMap().isEmpty()) {
                throw new RuntimeException("RangeInfos not provided");
            }
            return clusterInfo;
        }

    }


    public static class RangeInfo {

        @JsonProperty("iconResName")
        @JsonDeserialize(using = DrawableNameToResIdDeserializer.class)
        private int iconResId;

        private float textSize;

        private String textColor;

        protected RangeInfo() {
        }

        public RangeInfo(int iconResId, float textSize, String textColor) {
            this.iconResId = iconResId;
            this.textSize = textSize;
            this.textColor = textColor;
        }

        public int getIconResId() {
            return iconResId;
        }

        public float getTextSize() {
            return textSize;
        }

        public String getTextColor() {
            return textColor;
        }

        @Override
        public String toString() {
            return "RangeInfo{" +
                    "iconResId=" + iconResId +
                    ", textSize=" + textSize +
                    ", textColor=" + textColor +
                    '}';
        }
    }

}
