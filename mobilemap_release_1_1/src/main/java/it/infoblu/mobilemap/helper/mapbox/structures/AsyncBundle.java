package it.infoblu.mobilemap.helper.mapbox.structures;

import com.mapbox.mapboxsdk.maps.MapboxMap;

import java.util.List;

import it.infoblu.mobilemap.model.data.RemoteConfig;

public class AsyncBundle {

    public final RemoteConfig config;

    public final List<RemoteConfig.MapFilter> filterList;

    public final MapboxMap mapboxMap;

    public AsyncBundle(RemoteConfig config, List<RemoteConfig.MapFilter> filterList, MapboxMap mapboxMap) {
        this.config = config;
        this.filterList = filterList;
        this.mapboxMap = mapboxMap;
    }
}