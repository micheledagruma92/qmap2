package it.infoblu.mobilemap.helper.config.poigeometry;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.util.Map;

public class PoiGeometrySettingsDeserializer extends JsonDeserializer<PoiGeometrySettings> {

    @Override
    public PoiGeometrySettings deserialize(JsonParser jp, DeserializationContext ctxt)
            throws IOException, JsonProcessingException {

        PoiGeometrySettings instance = new PoiGeometrySettings();
        Map<String, PoiGeometryInfo> internalStructure = jp.readValueAs(new TypeReference<Map<String, PoiGeometryInfo>>() {
        });
        instance.poiType2PoiGeometryInfoMap.putAll(internalStructure);

        return instance;
    }
}