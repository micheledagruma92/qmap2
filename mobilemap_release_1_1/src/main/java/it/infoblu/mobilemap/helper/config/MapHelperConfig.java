package it.infoblu.mobilemap.helper.config;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.IOException;
import java.io.InputStream;

import it.infoblu.mobilemap.helper.config.cluster.ClusteringSettings;
import it.infoblu.mobilemap.helper.config.cluster.IClusteringSettings;
import it.infoblu.mobilemap.helper.config.cluster.NoClusteringSettings;
import it.infoblu.mobilemap.helper.config.poiattribute.IPoiAttributeSettings;
import it.infoblu.mobilemap.helper.config.poiattribute.NoPoiAttributeSettings;
import it.infoblu.mobilemap.helper.config.poiattribute.PoiAttributeSettings;
import it.infoblu.mobilemap.helper.config.poigeometry.IPoiGeometrySettings;
import it.infoblu.mobilemap.helper.config.poigeometry.NoPoiGeometrySettings;
import it.infoblu.mobilemap.helper.config.poigeometry.PoiGeometrySettings;
import it.infoblu.mobilemap.utils.InternalLogger;
import it.infoblu.mobilemap.utils.JsonMapperManager;

/**
 * Created on 27/03/18.
 */
public class MapHelperConfig {

    private String mapStyle;

    @JsonDeserialize(as = ClusteringSettings.class)
    private IClusteringSettings clusterSettings;

    @JsonDeserialize(as = PoiGeometrySettings.class)
    private IPoiGeometrySettings poiGeometrySettings;

    @JsonDeserialize(as = PoiAttributeSettings.class)
    private IPoiAttributeSettings poiAttributeSettings;

    private MapHelperConfig() {
    }

    public String getMapStyle() {
        return mapStyle;
    }

    private void setMapStyle(String mapStyle) {
        this.mapStyle = mapStyle;
    }

    public IClusteringSettings getClusterSettings() {
        return clusterSettings;
    }

    private void setClusterSettings(IClusteringSettings clusterSettings) {
        this.clusterSettings = clusterSettings;
    }

    public IPoiGeometrySettings getPoiGeometrySettings() {
        return poiGeometrySettings;
    }

    private void setPoiGeometrySettings(IPoiGeometrySettings poiGeometrySettings) {
        this.poiGeometrySettings = poiGeometrySettings;
    }

    public IPoiAttributeSettings getPoiAttributeSettings() {
        return poiAttributeSettings;
    }

    private void setPoiAttributeSettings(IPoiAttributeSettings poiAttributeSettings) {
        this.poiAttributeSettings = poiAttributeSettings;
    }

    public static MapHelperConfig parseJson(InputStream jsonInputStream) {
        try {
            return new Builder(JsonMapperManager.MAPPER.readValue(jsonInputStream, MapHelperConfig.class)).build();
        } catch (IOException e) {
            InternalLogger.e("json is not well formatted: %s", e.getMessage());
        }
        return null; // ERR
    }

    public static MapHelperConfig.Builder parseJsonToBuilder(InputStream jsonInputStream) {
        try {
            return new Builder(JsonMapperManager.MAPPER.readValue(jsonInputStream, MapHelperConfig.class));
        } catch (IOException e) {
            InternalLogger.e("json is not well formatted: %s", e.getMessage());
        }
        return null; // ERR
    }


    @Override
    public String toString() {
        return "MapHelperConfig{" +
                "mapStyle='" + mapStyle + '\'' +
                ", clusterSettings=" + clusterSettings +
                ", poiGeometrySettings=" + poiGeometrySettings +
                '}';
    }


    public static class Builder {

        private final MapHelperConfig configuration;

        public Builder() {
            this(new MapHelperConfig());
        }

        private Builder(MapHelperConfig configuration) {
            this.configuration = configuration;
        }

        public Builder mapStyle(String mapStyle) {
            this.configuration.setMapStyle(mapStyle);
            return this;
        }

        public Builder clusteringSettings(IClusteringSettings clusteringSettings) {
            this.configuration.setClusterSettings(clusteringSettings);
            return this;
        }

        public Builder poiGeometrySettings(IPoiGeometrySettings poiGeometrySettings) {
            this.configuration.setPoiGeometrySettings(poiGeometrySettings);
            return this;
        }

        public Builder poiAttributeSettings(IPoiAttributeSettings poiAttributeSettings) {
            this.configuration.setPoiAttributeSettings(poiAttributeSettings);
            return this;
        }

        public MapHelperConfig build() {
            if (configuration.getClusterSettings() == null) {
                configuration.setClusterSettings(new NoClusteringSettings());
            }
            if (configuration.getPoiGeometrySettings() == null) {
                configuration.setPoiGeometrySettings(new NoPoiGeometrySettings());
            }
            if (configuration.getPoiAttributeSettings() == null) {
                configuration.setPoiAttributeSettings(new NoPoiAttributeSettings());
            }
            return configuration;
        }
    }

}
