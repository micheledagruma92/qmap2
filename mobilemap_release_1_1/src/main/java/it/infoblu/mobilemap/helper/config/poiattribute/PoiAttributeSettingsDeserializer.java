package it.infoblu.mobilemap.helper.config.poiattribute;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.util.Map;

public class PoiAttributeSettingsDeserializer extends JsonDeserializer<PoiAttributeSettings> {

    @Override
    public PoiAttributeSettings deserialize(JsonParser jp, DeserializationContext ctxt)
            throws IOException, JsonProcessingException {

        PoiAttributeSettings instance = new PoiAttributeSettings();
        Map<String, PoiAttributeInfo> internalStructure = jp.readValueAs(new TypeReference<Map<String, PoiAttributeInfo>>() {
        });
        instance.poiType2PoiAttributeMap.putAll(internalStructure);

        return instance;
    }
}