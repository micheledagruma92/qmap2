package it.infoblu.mobilemap.helper.mapbox.utils;

import android.location.Location;

import com.annimon.stream.Stream;
import com.annimon.stream.function.Predicate;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.LineString;
import com.mapbox.geojson.Point;
import com.mapbox.mapboxsdk.annotations.PolylineOptions;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.turf.TurfMeasurement;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.infoblu.mobilemap.model.constants.InternalGeometryArrowProperty;
import it.infoblu.mobilemap.model.constants.PoiProperty;
import it.infoblu.mobilemap.model.data.MMPolyline;
import it.infoblu.mobilemap.model.data.Poi;
import it.infoblu.mobilemap.model.data.PoiUpdateInfo;
import it.infoblu.mobilemap.utils.PolylineEncoding;


/**
 * Created on 16/02/18.
 */

public final class InternalMapBoxUtils {

    private InternalMapBoxUtils() {
    }

    public static List<LatLng> convertPositionToMapboxLatLng(List<Point> positionList) {
        List<LatLng> pointListResponse = new ArrayList<>();
        for (Point point : positionList) {
            pointListResponse.add(new LatLng(point.latitude(), point.longitude()));
        }
        return pointListResponse;
    }

    public static List<Point> convertMapboxLatLngToPosition(List<LatLng> pointList) {
        List<Point> positionList = new ArrayList<>();
        for (LatLng point : pointList) {
            positionList.add(Point.fromLngLat(point.getLongitude(), point.getLatitude()));
        }
        return positionList;
    }

    public static Point convertMapboxLatLngToPosition(LatLng point) {
        return Point.fromLngLat(point.getLongitude(), point.getLatitude());
    }

    public static com.google.android.gms.maps.model.LatLng convertMapboxLocationToGMapLatLng(Location location) {
        return new com.google.android.gms.maps.model.LatLng(location.getLatitude(), location.getLongitude());
    }

    public static List<com.google.android.gms.maps.model.LatLng> convertPositionToGMapLatLng(List<Point> positionList) {
        List<com.google.android.gms.maps.model.LatLng> pointListResponse = new ArrayList<>();
        for (Point position : positionList) {
            pointListResponse.add(new com.google.android.gms.maps.model.LatLng(position.latitude(), position.longitude()));
        }
        return pointListResponse;
    }

    public static List<com.google.android.gms.maps.model.LatLng> convertFeatureToGMapLatLng(List<Feature> featureList) {
        List<com.google.android.gms.maps.model.LatLng> pointListResponse = new ArrayList<>();
        for (Feature feature : featureList) {
            com.google.android.gms.maps.model.LatLng point = convertFeatureToGMapLatLng(feature);
            if (point != null) {
                pointListResponse.add(point);
            }
        }
        return pointListResponse;
    }

    public static com.google.android.gms.maps.model.LatLng convertFeatureToGMapLatLng(Feature feature) {
        if (feature.geometry() instanceof Point) {
            Point point = (Point) feature.geometry();
            return new com.google.android.gms.maps.model.LatLng(point.latitude(), point.longitude());
        }
        return null;
    }

    public static LatLng convertFeatureToLatLng(Feature feature) {
        if (feature.geometry() instanceof Point) {
            Point position = (Point) feature.geometry();
            return new LatLng(position.latitude(), position.longitude());
        }
        return null;
    }

    public static List<LatLng> convertGMapLatLngToMapboxLatLng(List<com.google.android.gms.maps.model.LatLng> pointList) {
        List<LatLng> pointListResponse = new ArrayList<>();
        for (com.google.android.gms.maps.model.LatLng point : pointList) {
            pointListResponse.add(convertGMapLatLngToMapboxLatLng(point));
        }
        return pointListResponse;
    }

    public static LatLng convertGMapLatLngToMapboxLatLng(com.google.android.gms.maps.model.LatLng point) {
        return new LatLng(point.latitude, point.longitude);
    }


    public static com.google.android.gms.maps.model.LatLng convertMapboxLatLngToGMapLatLng(LatLng point) {
        return new com.google.android.gms.maps.model.LatLng(point.getLatitude(), point.getLongitude());
    }

    public static PolylineOptions convertToMapboxPolylineOptions(MMPolyline.Options options) {
        return new PolylineOptions()
                .alpha(options.getAlpha())
                .color(options.getColor())
                .width(options.getWidth());
    }

    public static FeatureCollection convertPoiListToFeatureCollection(List<Poi> poiList) {
        List<Feature> featureList = new ArrayList<>(poiList.size());
        for (Poi poi : poiList) {
            featureList.add(convertPoiToFeatureMinimal(poi));
        }
        return FeatureCollection.fromFeatures(featureList);
    }

    public static FeatureCollection convertGMapLatLngListToFeatureCollection(List<com.google.android.gms.maps.model.LatLng> pointList) {
        List<Feature> featureList = new ArrayList<>();
        List<Point> routeCoordinates = new ArrayList<>();
        for (com.google.android.gms.maps.model.LatLng point : pointList) {
            routeCoordinates.add(Point.fromLngLat(point.longitude, point.latitude));
        }
        LineString lineString = LineString.fromLngLats(routeCoordinates);
        featureList.add(Feature.fromGeometry(lineString));
        return FeatureCollection.fromFeatures(featureList);
    }

    public static Feature convertPoiToFeatureMinimal(Poi poi) {
        Point position = Point.fromLngLat(poi.getPosition().getLng(), poi.getPosition().getLat());
        Feature feature = Feature.fromGeometry(position);
        feature.addStringProperty(PoiProperty.ID, poi.getId());
        feature.addStringProperty(PoiProperty.SUBTYPE_ID, poi.getType());
        feature.addStringProperty(PoiProperty.ICO_RELATIVE_URL, StringUtils.defaultIfBlank(poi.getInfo().getIcoRelativeUrl(), PoiProperty.DEFAULT.ICO_RELATIVE_URL));
        feature.addStringProperty(PoiProperty.ICO_ANCHOR, PoiProperty.VALUES.ICO_ANCHOR_VALUES.contains(poi.getInfo().getIconAnchor())
                ? poi.getInfo().getIconAnchor() : PoiProperty.DEFAULT.ICO_ANCHOR);
        // TODO: 22/03/18 commmentato perché sembra appesantire le query fatte internamente da mapbox, alcune di queste property andranno aggiunte a tendere (es. icona)
//        feature.addStringProperty(PoiProperty.DESCRIPTION, poi.getInfo().getDescription());
//        feature.addStringProperty(PoiProperty.ROAD_NAME, poi.getPosition().getRoadName());
//        feature.addStringProperty(PoiProperty.ROAD_DIRECTION_NAME, poi.getPosition().getDirection());
//        feature.addStringProperty(PoiProperty.ROAD_LCD, poi.getPosition().getRoadLcd());
        // NB: update properties if they will change in FeatureCollectionToPoiConverter class
//        if (poi.getPropertyMap() != null) {
//            for (Map.Entry<String, String> entry : poi.getPropertyMap().entrySet()) {
//                feature.addStringProperty(entry.getKey(), entry.getValue());
//            }
//        }
        return feature;
    }

    public static FeatureCollection convertPoiListToInternalGeometryToFeatureCollection(List<Poi> poiList) {
        List<Feature> featureList = new ArrayList<>(poiList.size());
        for (Poi poi : poiList) {
            Feature internalFeature = convertPoiInternalGeometryToFeature(poi);
            if (internalFeature != null) {
                featureList.add(internalFeature);
            }
        }
        return FeatureCollection.fromFeatures(featureList);
    }

    public static Feature convertPoiInternalGeometryToFeature(Poi poi) {
        List<LatLng> pointList = PolylineEncoding.decode(poi.getPropertyMap().get(PoiProperty.ENC_GEOM));

        if (pointList.isEmpty()) {
            return null; // EXIT
        }
        List<Point> positionList = convertMapboxLatLngToPosition(pointList);
        Feature internalFeature = Feature.fromGeometry(LineString.fromLngLats(positionList));
        internalFeature.addStringProperty(PoiProperty.ID, poi.getId()); // TODO: 22/03/18 forse deve avere un id specifico ma riconducibile a quello del POI
        internalFeature.addStringProperty(PoiProperty.SUBTYPE_ID, poi.getType());
        internalFeature.addNumberProperty(PoiProperty.SEVERITY, poi.getInfo().getSeverity());
        return internalFeature;
    }

    public static FeatureCollection convertPoiListToInternalGeometryArrowToFeatureCollection(List<Poi> poiList) {
        List<Feature> featureList = new ArrayList<>(poiList.size());
        for (Poi poi : poiList) {
            featureList.addAll(convertPoiInternalGeometryArrowToFeature(poi));
        }
        return FeatureCollection.fromFeatures(featureList);
    }


    public static List<Feature> convertPoiInternalGeometryArrowToFeature(Poi poi) {
        List<LatLng> filteredPointList = Stream.of(PolylineEncoding.decode(poi.getPropertyMap().get(PoiProperty.ENC_GEOM)))
                .filter(new Predicate<LatLng>() {
                    private LatLng lastPointAdded;

                    @Override
                    public boolean test(LatLng value) {
                        if (value == null) {
                            return false; // safe check
                        }
                        if (lastPointAdded == null) {
                            lastPointAdded = value;
                            return true; // first step
                        }
                        boolean test = lastPointAdded.distanceTo(value) > 350;
                        if (test) {
                            lastPointAdded = value;
                        }
                        return test;
                    }
                }).toList();
        if (filteredPointList.isEmpty()) {
            return new ArrayList<>(); // EXIT
        }
        List<Point> positionList = convertMapboxLatLngToPosition(filteredPointList);

        if (positionList.size() <= 1) {
            return new ArrayList<>(); // EXIT: bearing impossible to calculate
        }

        List<Feature> featureList = new ArrayList<>();

        for (int i = 0; i < positionList.size(); i++) {
            Point current = positionList.get(i);
            double bearing = 0;
            if (i + 1 >= positionList.size()) { // final step: no successor
                Point prev = positionList.get(i - 1);
                bearing = calculateBearing(prev, current);
            } else {
                Point next = positionList.get(i + 1);
                bearing = calculateBearing(current, next);
            }
            Feature feature = Feature.fromGeometry(current);
            feature.addNumberProperty(InternalGeometryArrowProperty.ROTATION, bearing);
            featureList.add(feature);
        }

        return featureList;
    }

    public static List<PoiUpdateInfo> splitToPoiUpdateInfoList(Collection<Poi> pois) {
        if (pois == null) {
            return new ArrayList<>(); // EXIT
        }
        Map<String, PoiUpdateInfo> internalMap = new HashMap<>();
        for (Poi poi : pois) {
            PoiUpdateInfo poiUpdateInfo = internalMap.get(poi.getType());
            if (poiUpdateInfo == null) {
                poiUpdateInfo = new PoiUpdateInfo(poi.getType());
                internalMap.put(poi.getType(), poiUpdateInfo);
            }
            poiUpdateInfo.getPoiList().add(poi);
        }
        return new ArrayList<>(internalMap.values());
    }

    private static double calculateBearing(Point prevPosition, Point position) {
        com.mapbox.geojson.Point p0 = com.mapbox.geojson.Point.fromLngLat(prevPosition.longitude(), prevPosition.latitude());
        com.mapbox.geojson.Point p1 = com.mapbox.geojson.Point.fromLngLat(position.longitude(), position.latitude());
        return TurfMeasurement.bearing(p0, p1);
    }

    public static com.google.android.gms.maps.model.LatLngBounds convertMapboxLatLngBoundToGMapLatLngBound(LatLngBounds latLngBounds) {
        return new com.google.android.gms.maps.model.LatLngBounds(
                new com.google.android.gms.maps.model.LatLng(latLngBounds.getSouthWest().getLatitude(), latLngBounds.getSouthWest().getLongitude())
                , new com.google.android.gms.maps.model.LatLng(latLngBounds.getNorthEast().getLatitude(), latLngBounds.getNorthEast().getLongitude()));
    }

    public static LatLngBounds convertGMapLatLngBoundToMapboxLatLngBound(com.google.android.gms.maps.model.LatLngBounds latLngBounds) {
        return LatLngBounds.from(latLngBounds.northeast.latitude, latLngBounds.northeast.longitude
                , latLngBounds.southwest.latitude, latLngBounds.southwest.longitude);
    }

}
