package it.infoblu.mobilemap.helper.mapbox.manager;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PointF;
import android.text.TextUtils;

import com.mapbox.geojson.Feature;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.style.expressions.Expression;
import com.mapbox.mapboxsdk.style.layers.Property;
import com.mapbox.mapboxsdk.style.layers.PropertyFactory;
import com.mapbox.mapboxsdk.style.layers.PropertyValue;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonOptions;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import it.infoblu.mobilemap.R;
import it.infoblu.mobilemap.helper.config.cluster.ClusterInfo;
import it.infoblu.mobilemap.helper.config.cluster.IClusteringSettings;
import it.infoblu.mobilemap.helper.mapbox.utils.InternalMapBoxUtils;
import it.infoblu.mobilemap.helper.mapbox.utils.MarkerLayerUtils;
import it.infoblu.mobilemap.utils.InternalLogger;
import it.infoblu.mobilemap.utils.ViewUtils;

/**
 * Created on 21/03/18.
 */

public class ClusterManager {

    private static final String COUNT_PROPERTY_NAME = "point_count";

    private final IClusteringSettings clusteringSettings;

    private final List<String> clusterLayerIdList = new ArrayList<>();


    public ClusterManager(IClusteringSettings clusteringSettings) {
        this.clusteringSettings = clusteringSettings;
    }


    public GeoJsonOptions decorateGeoJsonOptions(String poiType, GeoJsonOptions options) {
        ClusterInfo clusterInfo = clusteringSettings.getClusterInfo(poiType);
        if (clusterInfo == null) {
            options.withCluster(false);
            return options; // EXIT
        }
        options.withCluster(true);
        if (clusterInfo.getZoomMax() > 0) {
            options.withClusterMaxZoom(clusterInfo.getZoomMax());
        }
        if (clusterInfo.getRadius() > 0) {
            options.withClusterRadius(clusterInfo.getRadius());
        }
        return options;
    }

    public GeoJsonOptions getGeoJsonOptions(String poiType) {
        return decorateGeoJsonOptions(poiType, new GeoJsonOptions());
    }

    public void addClusterLayers(Context context, MapboxMap mapboxMap, String poiType
            , boolean isFilterVisible, int zoomMin, int zoomMax) {
        ClusterInfo clusterInfo = clusteringSettings.getClusterInfo(poiType);
        if (clusterInfo == null) {
            return; // EXIT
        }

        String sourceId = MarkerLayerUtils.getSourceId(poiType);
        String clusterLayerId = MarkerLayerUtils.getClusterMarkerImageLayerId(poiType);

        if (mapboxMap.getLayer(clusterLayerId) != null) {
            return; // EXIT: layer already added
        }

        clusterLayerIdList.add(clusterLayerId);
        SymbolLayer clusterLayer = new SymbolLayer(clusterLayerId, sourceId);

        boolean isClusterFontEnabled = !TextUtils.isEmpty(clusterInfo.getTextFont());

        int stopSize = clusterInfo.getRange2InfoMap().size();
        Expression.Stop[] iconImageStops = new Expression.Stop[stopSize];
        List<Expression.Stop> textColorStopList = new ArrayList<>();
        List<Expression.Stop> textSizeStopList = new ArrayList<>();

        int i = 0;
        for (Map.Entry<Integer, ClusterInfo.RangeInfo> entry : clusterInfo.getRange2InfoMap().entrySet()) {
            int range = entry.getKey();
            ClusterInfo.RangeInfo rangeInfo = entry.getValue();
            String iconImageId = MarkerLayerUtils.getClusterMarkerImageLayerIconId(poiType, range);
            if (mapboxMap.getImage(iconImageId) == null) { // add icon image if needed
                // NB: potrebbe essere il caso di gestirlo al cambio di stile
                Bitmap markerImage = BitmapFactory.decodeResource(context.getResources(), rangeInfo.getIconResId());
                if (markerImage == null) { // to manage shapes
                    int clusterIconResId = rangeInfo.getIconResId() != 0 ? rangeInfo.getIconResId() : R.drawable.mobile_map_shape_cluster_default;
                    markerImage = ViewUtils.drawableToBitmap(context.getResources().getDrawable(clusterIconResId));
                }
                if (markerImage != null) {
                    mapboxMap.addImage(iconImageId, markerImage);
                } else {
                    InternalLogger.d("no local cluster icon found");
                }
            }
            iconImageStops[i] = Expression.stop(range, iconImageId);
            if (isClusterFontEnabled) {
                if (!TextUtils.isEmpty(rangeInfo.getTextColor())) {
                    textColorStopList.add(Expression.stop(range, rangeInfo.getTextColor()));
                }
                textSizeStopList.add(Expression.stop(range, rangeInfo.getTextSize()));
            }
            i++;
        }

        List<PropertyValue<?>> propertyList = new ArrayList<>();
        // icon properties
//        propertyList.add(PropertyFactory.iconImage(Function.property(COUNT_PROPERTY_NAME, Stops.interval(iconImageStops))));
        propertyList.add(PropertyFactory.iconImage(Expression.step(Expression.get(COUNT_PROPERTY_NAME), Expression.literal(""), iconImageStops)));
        propertyList.add(PropertyFactory.iconAllowOverlap(true));
        propertyList.add(PropertyFactory.iconIgnorePlacement(true));
        propertyList.add(PropertyFactory.iconOpacity(1F));
        // label properties
        if (isClusterFontEnabled) { // todo: correggere logica -> non allineata a meccanismo step/stop (vd. iconImage)
            propertyList.add(PropertyFactory.textField("{" + COUNT_PROPERTY_NAME + "}"));
            if (!textSizeStopList.isEmpty()) {
//                propertyList.add(PropertyFactory.textSize(Function.property(COUNT_PROPERTY_NAME, Stops.interval(textSizeStopList.toArray(new Stop[0])))));
                propertyList.add(PropertyFactory.textSize( // todo: modificare, non corretto
                        Expression.interpolate(Expression.linear()
                                , Expression.get(COUNT_PROPERTY_NAME)
                                , textSizeStopList.toArray(new Expression.Stop[0]))
                ));
            }
            if (!textColorStopList.isEmpty()) {
//                propertyList.add(PropertyFactory.textColor(Function.property(COUNT_PROPERTY_NAME, Stops.interval(textColorStopList.toArray(new Stop[0])))));
                propertyList.add(PropertyFactory.textColor( // todo: modificare, non corretto
                        Expression.interpolate(Expression.linear()
                                , Expression.get(COUNT_PROPERTY_NAME)
                                , textColorStopList.toArray(new Expression.Stop[0]))
                ));
            }
            propertyList.add(PropertyFactory.textFont(new String[]{clusterInfo.getTextFont()}));
            propertyList.add(PropertyFactory.textAllowOverlap(true));
            propertyList.add(PropertyFactory.textIgnorePlacement(true));
        }

        clusterLayer.setProperties(propertyList.toArray(new PropertyValue<?>[0]));

        if (zoomMin > 0) {
            clusterLayer.setMinZoom(zoomMin - 1/*fix to avoid 0 items issue*/);
        }
        if (zoomMax > 0) {
            clusterLayer.setMaxZoom(zoomMax);
        }

        String anchorLayerId = MarkerLayerUtils.getLayerId(poiType);

        if (TextUtils.isEmpty(anchorLayerId)) {
            mapboxMap.addLayer(clusterLayer);
        } else {
            mapboxMap.addLayerAbove(clusterLayer, anchorLayerId);
        }

        if (!isFilterVisible) {
            clusterLayer.setProperties(PropertyFactory.visibility(Property.NONE));
        }
    }

    public boolean interceptClickEvent(MapboxMap mapboxMap, PointF screenPoint) {
        if (clusterLayerIdList.isEmpty()) {
            return false; // EXIT
        }

        List<Feature> features = mapboxMap.queryRenderedFeatures(screenPoint, clusterLayerIdList.toArray(new String[0]));
        if (!features.isEmpty()) {
            Feature selectedFeature = features.get(0);

            int newZoom = Math.max(1, (int) (mapboxMap.getCameraPosition().zoom + 1)); // TODO: 21/03/18 aggiungere alle config

            mapboxMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    InternalMapBoxUtils.convertFeatureToLatLng(selectedFeature)
                    , newZoom), 700);

            return true; // EXIT
        }

        return false;
    }

    /**
     * @param poiType
     * @return clusterZoomMax for the input poiType. Cluster mechanism is available for the interval [0, clusterZoomMax + 1)
     */
    public int getClusterZoomMax(String poiType) {
        ClusterInfo clusterInfo = clusteringSettings.getClusterInfo(poiType);
        return clusterInfo == null ? 0 : clusterInfo.getZoomMax();
    }

}
