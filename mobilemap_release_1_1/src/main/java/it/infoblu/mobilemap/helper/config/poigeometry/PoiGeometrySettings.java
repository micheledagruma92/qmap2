package it.infoblu.mobilemap.helper.config.poigeometry;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import it.infoblu.mobilemap.utils.InternalLogger;
import it.infoblu.mobilemap.utils.JsonMapperManager;

/**
 * Created on 21/03/18.
 */
@JsonDeserialize(using = PoiGeometrySettingsDeserializer.class)
public class PoiGeometrySettings implements IPoiGeometrySettings {

    private final static String KEY_DEFAULT = "def";

    protected final Map<String, PoiGeometryInfo> poiType2PoiGeometryInfoMap = new HashMap<>();


    protected PoiGeometrySettings() {
    }


    @Override
    public PoiGeometryInfo getPoiGeometryInfo(String poiType) {
        PoiGeometryInfo poiGeometryInfo = poiType2PoiGeometryInfoMap.get(poiType);
        return poiGeometryInfo == null ? poiType2PoiGeometryInfoMap.get(KEY_DEFAULT) : poiGeometryInfo;
    }


    public static PoiGeometrySettings parseJson(InputStream jsonInputStream) {
        try {
            return JsonMapperManager.MAPPER.readValue(jsonInputStream, PoiGeometrySettings.class);
        } catch (IOException e) {
            InternalLogger.e("json is not well formatted: %s", e.getMessage());
        }
        return null; // ERR
    }


    @Override
    public String toString() {
        return "PoiGeometrySettings{" +
                "poiType2PoiGeometryInfoMap=" + poiType2PoiGeometryInfoMap +
                '}';
    }

    public static class Builder {

        private final PoiGeometrySettings settings;

        public Builder() {
            settings = new PoiGeometrySettings();
        }

        public Builder addPoiGeometryInfo(String poiType, PoiGeometryInfo poiGeometryInfo) {
            settings.poiType2PoiGeometryInfoMap.put(poiType, poiGeometryInfo);
            return this;
        }

        public Builder defaultPoiGeometryInfo(PoiGeometryInfo poiGeometryInfo) {
            settings.poiType2PoiGeometryInfoMap.put(KEY_DEFAULT, poiGeometryInfo);
            return this;
        }

        public PoiGeometrySettings build() {
            return settings;
        }

    }

}
