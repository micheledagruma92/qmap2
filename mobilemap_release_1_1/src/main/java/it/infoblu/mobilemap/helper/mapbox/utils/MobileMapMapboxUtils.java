package it.infoblu.mobilemap.helper.mapbox.utils;

import android.graphics.Bitmap;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.maps.MapboxMap;

import io.reactivex.Completable;
import it.infoblu.mobilemap.MobileMap;

/**
 * Created on 27/02/18.
 */

public final class MobileMapMapboxUtils {

    private MobileMapMapboxUtils() {
    }


    public static void captureScreen(MapboxMap mapboxMap, final ISnapshotListener listener) {
        mapboxMap.snapshot(snapshot -> {
            if (listener != null) {
                listener.onSnapshotCaptured(snapshot);
            }
        });
    }

    public static Completable resetConfigZoom(MapboxMap mapboxMap) {
        return MobileMap.getSession().getRemoteConfigObservable()
                .take(1).singleOrError()
                .flatMapCompletable(remoteConfig -> Completable.fromRunnable(() -> {
                    if (remoteConfig.getInitParameters() != null) {
                        zoom(mapboxMap
                                , remoteConfig.getInitParameters().getPoint()
                                , remoteConfig.getInitParameters().getZoom());
                    }
                }));
    }

    public static void zoom(MapboxMap mapboxMap, LatLng point, int zoom) {
        mapboxMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                new com.mapbox.mapboxsdk.geometry.LatLng(point.latitude, point.longitude), zoom));
    }

    public static void zoom(MapboxMap mapboxMap, LatLngBounds latLngBounds) {
        mapboxMap.moveCamera(CameraUpdateFactory.newLatLngBounds(
                InternalMapBoxUtils.convertGMapLatLngBoundToMapboxLatLngBound(latLngBounds)
                , 0));
    }


    /*
     * Listener
     */

    public interface ISnapshotListener {

        void onSnapshotCaptured(Bitmap bitmap);
    }

}
