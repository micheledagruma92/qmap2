package it.infoblu.mobilemap.helper.mapbox.action;

import com.mapbox.mapboxsdk.maps.MapboxMap;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import it.infoblu.mobilemap.helper.interfaces.IMapAction;
import it.infoblu.mobilemap.helper.mapbox.interfaces.IMapsMapBoxActionBinder;
import it.infoblu.mobilemap.helper.mapbox.utils.PolylineLayerUtils;

/**
 * Created on 09/07/18.
 */
public class MapBoxRemovePolylineLayerAction implements IMapAction {

    private final IMapsMapBoxActionBinder mBinder;

    private final Bundle mBundle;

    public MapBoxRemovePolylineLayerAction(IMapsMapBoxActionBinder mBinder, Bundle mBundle) {
        this.mBinder = mBinder;
        this.mBundle = mBundle;
    }

    @Override
    public Observable<IMapAction> exec() {
        return mBinder.getMapboxMapObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .map(mapboxMap -> {
                    removePolylineSync(mapboxMap);
                    return MapBoxRemovePolylineLayerAction.this;
                });
    }

    private void removePolylineSync(MapboxMap mapboxMap) {
        String sourceId = PolylineLayerUtils.getSourceId(mBundle.tag);
        String layerId = PolylineLayerUtils.getSourceId(mBundle.tag);

        if (mapboxMap.getSource(sourceId) != null) {
            mapboxMap.removeSource(sourceId);
        }
        if (mapboxMap.getLayer(layerId) != null) {
            mapboxMap.removeLayer(layerId);
        }
    }


    /*
     * Structures
     */

    public static class  Bundle {

        private String tag;

        public Bundle(String tag) {
            this.tag = tag;
        }
    }

}
