package it.infoblu.mobilemap.helper.config.cluster;

/**
 * Created on 21/03/18.
 */

public class NoClusteringSettings implements IClusteringSettings {

    @Override
    public ClusterInfo getClusterInfo(String poiType) {
        return null;
    }

}
