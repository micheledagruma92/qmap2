package it.infoblu.mobilemap.helper.config.cluster;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import it.infoblu.mobilemap.utils.InternalLogger;
import it.infoblu.mobilemap.utils.JsonMapperManager;

/**
 * Created on 21/03/18.
 */

@JsonDeserialize(using = ClusterSettingsDeserializer.class)
public class ClusteringSettings implements IClusteringSettings {

    private final static String KEY_DEFAULT = "def";

    protected final Map<String, ClusterInfo> poiType2ClusterInfoMap = new HashMap<>();


    protected ClusteringSettings() {
    }

    @Override
    public ClusterInfo getClusterInfo(String poiType) {
        ClusterInfo clusterInfoSettings = poiType2ClusterInfoMap.get(poiType);
        return clusterInfoSettings == null ? poiType2ClusterInfoMap.get(KEY_DEFAULT) : clusterInfoSettings;
    }

    public static ClusteringSettings parseJson(InputStream jsonInputStream) {
        try {
            return JsonMapperManager.MAPPER.readValue(jsonInputStream, ClusteringSettings.class);
        } catch (IOException e) {
            InternalLogger.e("json is not well formatted: %s", e.getMessage());
        }
        return null; // ERR
    }


    @Override
    public String toString() {
        return "ClusteringSettings{" +
                "poiType2ClusterInfoMap=" + poiType2ClusterInfoMap +
                '}';
    }

    public static class Builder {

        private final ClusteringSettings settings;

        public Builder() {
            settings = new ClusteringSettings();
        }

        public ClusteringSettings.Builder addClusterInfo(String poiType, ClusterInfo clusterInfo) {
            settings.poiType2ClusterInfoMap.put(poiType, clusterInfo);
            return this;
        }

        public ClusteringSettings.Builder defaultPoiGeometryInfo(ClusterInfo poiGeometryInfo) {
            settings.poiType2ClusterInfoMap.put(KEY_DEFAULT, poiGeometryInfo);
            return this;
        }

        public ClusteringSettings build() {
            return settings;
        }

    }

}
