package it.infoblu.mobilemap.helper.mapbox.action;


import it.infoblu.mobilemap.helper.interfaces.IMapAction;
import it.infoblu.mobilemap.helper.mapbox.interfaces.IMapsMapBoxActionBinder;

/**
 * Created on 15/02/18.
 */

public abstract class AMapBoxAction implements IMapAction {

    private IMapsMapBoxActionBinder binder;

    public AMapBoxAction(IMapsMapBoxActionBinder binder) {
        this.binder = binder;
    }

    protected IMapsMapBoxActionBinder getBinder() {
        return binder;
    }

}
