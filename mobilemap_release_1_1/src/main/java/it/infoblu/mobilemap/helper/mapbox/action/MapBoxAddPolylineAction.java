package it.infoblu.mobilemap.helper.mapbox.action;

import com.mapbox.mapboxsdk.annotations.Polyline;
import com.mapbox.mapboxsdk.annotations.PolylineOptions;

import io.reactivex.Observable;
import it.infoblu.mobilemap.helper.interfaces.IMapAction;
import it.infoblu.mobilemap.helper.mapbox.interfaces.IMapsMapBoxActionBinder;
import it.infoblu.mobilemap.helper.mapbox.structures.AsyncBundle;
import it.infoblu.mobilemap.helper.mapbox.structures.PolylineInfoStructure;
import it.infoblu.mobilemap.helper.mapbox.structures.PolylineWrapper;
import it.infoblu.mobilemap.helper.mapbox.utils.InternalMapBoxUtils;
import it.infoblu.mobilemap.model.data.MMPolyline;


/**
 * Created on 15/02/18.
 */

public class MapBoxAddPolylineAction extends AMapBoxAction {

    private final Bundle mBundle;


    public MapBoxAddPolylineAction(IMapsMapBoxActionBinder binder, Bundle bundle) {
        super(binder);
        mBundle = bundle;
    }

    @Override
    public Observable<IMapAction> exec() {
        return getBinder().getAsyncBundleObservable()
                .map(asyncBundle -> {
                    addPolylineSync(asyncBundle, mBundle.polyline);
                    return MapBoxAddPolylineAction.this;
                });
    }


    public void addPolylineSync(AsyncBundle asyncBundle, MMPolyline polyline) {
        PolylineOptions polylineOptions = InternalMapBoxUtils.convertToMapboxPolylineOptions(mBundle.polyline.getOptions())
                .addAll(InternalMapBoxUtils.convertGMapLatLngToMapboxLatLng(mBundle.polyline.getPoints()));
        Polyline mapboxPolyline = asyncBundle.mapboxMap.addPolyline(polylineOptions);
        PolylineWrapper polylineWrapper = new PolylineWrapper(mapboxPolyline);
        mBundle.polylineInfoStructure.putPolylineRelation(polyline, polylineWrapper);
    }

    @Override
    public String toString() {
        return "Action -> add Polyline";
    }


    /*
     * Structures
     */

    public static class Bundle {

        private final MMPolyline polyline;

        private final PolylineInfoStructure polylineInfoStructure;

        public Bundle(MMPolyline polyline, PolylineInfoStructure polylineInfoStructure) {
            this.polyline = polyline;
            this.polylineInfoStructure = polylineInfoStructure;
        }
    }

}
