package it.infoblu.mobilemap.helper.mapbox.settings;

import android.annotation.SuppressLint;

import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;

import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.plugins.locationlayer.LocationLayerPlugin;
import com.mapbox.mapboxsdk.plugins.locationlayer.modes.CameraMode;
import com.mapbox.mapboxsdk.plugins.locationlayer.modes.RenderMode;

import io.reactivex.disposables.CompositeDisposable;
import it.infoblu.mobilemap.constants.MobileMapConstants;
import it.infoblu.mobilemap.helper.interfaces.IMapsHelper;
import it.infoblu.mobilemap.helper.interfaces.IMapsSettingsConfiguration;
import it.infoblu.mobilemap.helper.mapbox.MapsMapBoxHelper;
import it.infoblu.mobilemap.helper.mapbox.interfaces.IMapSettingsBinder;

/**
 * Created on 23/02/18.
 */
public class MapSettingsNav implements IMapsSettingsConfiguration {

    private final IMapSettingsBinder mBinder;
    private final Lifecycle mLifecycle;
    private final MapView mMapView;
    private final double mTilt;

    private CompositeDisposable mDisposables;

    private LocationLayerPlugin mLocationLayerPlugin;


    public MapSettingsNav(IMapSettingsBinder binder, Lifecycle lifecycle, MapView mapView, double tilt) {
        mBinder = binder;
        mLifecycle = lifecycle;
        mTilt = tilt;
        mMapView = mapView;
    }


    @Override
    public void apply() {
        if (mDisposables != null) mDisposables.dispose();
        mDisposables = new CompositeDisposable();

        mMapView.getMapAsync(mapboxMap -> mapboxMap.setPadding(0, mMapView.getHeight() / 2, 0, 0));

        mBinder.getFollowStateSubject().onNext(IMapsHelper.FOLLOW_STATE.FOLLOW); // default

        mLifecycle.addObserver(new LifecycleObserver() {
            @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
            public void onDestroy() {
                mLifecycle.removeObserver(this);
                mDisposables.dispose();
            }
        });

        mDisposables.add(mBinder.getLocationLayerPluginSingle()
                .subscribe(locationLayerPlugin -> {
                    mLocationLayerPlugin = locationLayerPlugin;
                    locationLayerPlugin.setRenderMode(RenderMode.GPS);
                }));
    }

    @Override
    public void revert() {
        if (mDisposables != null) mDisposables.dispose();

        mMapView.getMapAsync(mapboxMap -> {
            mapboxMap.setPadding(0, 0, 0, 0);
        });

        if (mLocationLayerPlugin != null) {
            mLocationLayerPlugin.setRenderMode(RenderMode.NORMAL);
        }
    }

    @Override
    @SuppressLint("MissingPermission")
    public boolean manageFollowState(MapsMapBoxHelper.LocationLayerBundle locationLayerBundle, int state) {
        switch (state) {
            case IMapsHelper.FOLLOW_STATE.FOLLOW:
                locationLayerBundle.locationLayerPlugin.setCameraMode(CameraMode.TRACKING_GPS);
                locationLayerBundle.mapboxMap.moveCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                        .tilt(mTilt)
                        .zoom(MobileMapConstants.CENTER_INIT_ZOOM)
                        .build()));
                break;
            case IMapsHelper.FOLLOW_STATE.FREE:
                locationLayerBundle.locationLayerPlugin.setCameraMode(CameraMode.NONE);
                break;
        }
        return true;
    }

}

