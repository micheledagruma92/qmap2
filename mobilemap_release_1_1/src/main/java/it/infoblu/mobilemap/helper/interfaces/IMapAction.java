package it.infoblu.mobilemap.helper.interfaces;

import io.reactivex.Observable;

/**
 * Created on 15/02/18.
 */

public interface IMapAction {

    Observable<IMapAction> exec();

}
