package it.infoblu.mobilemap.helper.mapbox.action;

import android.content.Context;
import android.graphics.Bitmap;

import com.mapbox.mapboxsdk.annotations.BaseMarkerOptions;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.geometry.LatLng;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import it.infoblu.mobilemap.helper.interfaces.IMapAction;
import it.infoblu.mobilemap.helper.mapbox.interfaces.IMapsMapBoxActionBinder;
import it.infoblu.mobilemap.helper.mapbox.structures.AsyncBundle;
import it.infoblu.mobilemap.helper.mapbox.structures.MarkerWrapper;
import it.infoblu.mobilemap.helper.mapbox.structures.MarkersInfoStructure;
import it.infoblu.mobilemap.manager.IMarkerImageLoader;
import it.infoblu.mobilemap.model.data.Poi;
import it.infoblu.mobilemap.model.data.PoiMarkerDownloadResult;
import it.infoblu.mobilemap.utils.InternalLogger;

/**
 * Created on 15/02/18.
 */

public class MapBoxAddMarkersAction extends AMapBoxAction {

    private final Bundle mBundle;


    public MapBoxAddMarkersAction(IMapsMapBoxActionBinder binder, Bundle bundle) {
        super(binder);
        mBundle = bundle;
    }

    @Override
    public Observable<IMapAction> exec() {
        return getBinder().getAsyncBundleObservable()
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(new Function<AsyncBundle, ObservableSource<AsyncBundle>>() {
                    @Override
                    public ObservableSource<AsyncBundle> apply(final AsyncBundle asyncBundle) throws Exception {
                        return mBundle.markerImageLoaderManager.downloadMarkerBitmapsBy(mBundle.poiList)
                                .map(new Function<List<PoiMarkerDownloadResult>, AsyncBundle>() {
                                    @Override
                                    public AsyncBundle apply(List<PoiMarkerDownloadResult> poiMarkerDownloadResults) throws Exception {
                                        return asyncBundle;
                                    }
                                });
                    }
                })
                .map(new Function<AsyncBundle, IMapAction>() {
                    @Override
                    public IMapAction apply(AsyncBundle asyncBundle) throws Exception {
                        addMarkersSync(asyncBundle, mBundle.poiList);
                        return MapBoxAddMarkersAction.this;
                    }
                });
    }


    public void addMarkersSync(AsyncBundle asyncBundle, List<Poi> poiList) {
        Map<Poi, MarkerInfoBundle> markerWrapperToAddMap = new HashMap<>();
        List<Marker> markerToRemoveList = new ArrayList<>();

        // iterate poiList to construct markerOptions tmp structure and to obsolete markers list
        for (Poi poi : poiList) {

            final MarkerOptions markerOptions = new MarkerOptions()
                    .position(new LatLng(poi.getPosition().getLat(), poi.getPosition().getLng()))
                    .title(poi.getInfo().getTitle())
                    .snippet(poi.getType());

            // retrieve bitmap
            Bitmap iconBitmap = mBundle.markerImageLoaderManager.getMarkerBitmapSyncFromCacheBy(poi);
            if (iconBitmap != null) {
                markerOptions.setIcon(IconFactory.getInstance(mBundle.context).fromBitmap(iconBitmap));
            } else {
                InternalLogger.d("iconBitmap not found for POI: %s", poi);
            }

            MarkerWrapper markerWrapper = mBundle.markersInfoStructure.getMarkerWrapper(poi);

            markerWrapperToAddMap.put(poi, new MarkerInfoBundle(markerOptions
                    // FIXME: 15/03/18 attualmente se il marker non è mai stato aggiunto viene considerato visibile (errato)
                    , markerWrapper == null || markerWrapper.isVisible()));

            if (markerWrapper != null) {
                if (markerWrapper.getMarker() != null) {
                    markerToRemoveList.add(markerWrapper.getMarker());
                }
            }
        }

        // add new markers to map
        List<MarkerOptions> markerOptionsList = new ArrayList<>();
        for (MarkerInfoBundle markerInfoBundle : markerWrapperToAddMap.values()) {
            if (markerInfoBundle.visible) {
                markerOptionsList.add(markerInfoBundle.markerOptions);
            }
        }
        List<Marker> newMarkerList = asyncBundle.mapboxMap.addMarkers(new ArrayList<BaseMarkerOptions>(markerOptionsList));

        // remove old markers
        for (Marker marker : markerToRemoveList) {
            asyncBundle.mapboxMap.removeMarker(marker);
        }

        // update session markerWrapper structures
        int addedMarkerIndex = 0;
        for (Map.Entry<Poi, MarkerInfoBundle> entry : markerWrapperToAddMap.entrySet()) {
            Poi poi = entry.getKey();
            MarkerOptions newMarkerOption = entry.getValue().markerOptions;

            MarkerWrapper markerWrapper = mBundle.markersInfoStructure.getMarkerWrapper(poi);

            Marker newMarker = null;

            if (entry.getValue().visible) {
                newMarker = newMarkerList.get(addedMarkerIndex);
                addedMarkerIndex++;
            }

            if (markerWrapper == null) {
                markerWrapper = new MarkerWrapper(newMarker, newMarkerOption);
                mBundle.markersInfoStructure.putPoiMarkerRelation(poi, markerWrapper);
            } else {
                markerWrapper.setMarker(newMarker);
                markerWrapper.setOptions(newMarkerOption);
            }
        }
    }

    @Override
    public String toString() {
        return "Action -> add Poi list: " + mBundle.poiList.toString();
    }


    /*
     * Structures
     */

    public static class Bundle {

        private final Context context;

        private final IMarkerImageLoader markerImageLoaderManager;

        private final MarkersInfoStructure markersInfoStructure;

        private final List<Poi> poiList;

        public Bundle(Context context, MarkersInfoStructure markersInfoStructure
                , IMarkerImageLoader markerImageLoaderManager, List<Poi> poiList) {
            this.context = context;
            this.markerImageLoaderManager = markerImageLoaderManager;
            this.markersInfoStructure = markersInfoStructure;
            this.poiList = poiList;
        }
    }

    private static class MarkerInfoBundle {

        private final MarkerOptions markerOptions;

        private final boolean visible;

        public MarkerInfoBundle(MarkerOptions markerOptions, boolean visible) {
            this.markerOptions = markerOptions;
            this.visible = visible;
        }
    }

    private static class InternalBundle {

        private final AsyncBundle asyncBundle;

        private final Bitmap iconBitmap;

        public InternalBundle(AsyncBundle asyncBundle, Bitmap iconBitmap) {
            this.asyncBundle = asyncBundle;
            this.iconBitmap = iconBitmap;
        }
    }

}
