package it.infoblu.mobilemap.helper.mapbox.action;

import com.mapbox.mapboxsdk.annotations.Polyline;

import io.reactivex.Observable;
import io.reactivex.functions.Function;
import it.infoblu.mobilemap.helper.interfaces.IMapAction;
import it.infoblu.mobilemap.helper.mapbox.interfaces.IMapsMapBoxActionBinder;
import it.infoblu.mobilemap.helper.mapbox.structures.AsyncBundle;
import it.infoblu.mobilemap.helper.mapbox.structures.PolylineInfoStructure;
import it.infoblu.mobilemap.helper.mapbox.structures.PolylineWrapper;
import it.infoblu.mobilemap.model.data.MMPolyline;

/**
 * Created on 15/02/18.
 */

public class MapBoxRemovePolylineAction extends AMapBoxAction {

    private final Bundle mBundle;


    public MapBoxRemovePolylineAction(IMapsMapBoxActionBinder binder, Bundle bundle) {
        super(binder);
        mBundle = bundle;
    }

    @Override
    public Observable<IMapAction> exec() {
        return getBinder().getAsyncBundleObservable()
                .map(new Function<AsyncBundle, IMapAction>() {
                    @Override
                    public IMapAction apply(AsyncBundle asyncBundle) throws Exception {
                        removePolylineSync(asyncBundle, mBundle.polyline);
                        return MapBoxRemovePolylineAction.this;
                    }
                });
    }

    public Polyline removePolylineSync(AsyncBundle asyncBundle, MMPolyline polyline) {
        PolylineWrapper polylineWrapper = mBundle.polylineInfoStructure.removePolylineRelation(polyline);
        if (polylineWrapper == null || polylineWrapper.getPolyline() == null) {
            return null; // EXIT: polyline not found
        }
        Polyline mapboxPolyline = polylineWrapper.getPolyline();
        asyncBundle.mapboxMap.removePolyline(mapboxPolyline);
        return mapboxPolyline;
    }


    /*
     * Structures
     */

    public static class Bundle {

        private final MMPolyline polyline;

        private final PolylineInfoStructure polylineInfoStructure;

        public Bundle(MMPolyline polyline, PolylineInfoStructure polylineInfoStructure) {
            this.polyline = polyline;
            this.polylineInfoStructure = polylineInfoStructure;
        }
    }


}
