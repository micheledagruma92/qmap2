package it.infoblu.mobilemap.helper.mapbox.action;

import android.content.Context;
import android.graphics.Bitmap;

import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.geometry.LatLng;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import it.infoblu.mobilemap.helper.interfaces.IMapAction;
import it.infoblu.mobilemap.helper.mapbox.interfaces.IMapsMapBoxActionBinder;
import it.infoblu.mobilemap.helper.mapbox.structures.AsyncBundle;
import it.infoblu.mobilemap.helper.mapbox.structures.MarkerWrapper;
import it.infoblu.mobilemap.helper.mapbox.structures.MarkersInfoStructure;
import it.infoblu.mobilemap.manager.IMarkerImageLoader;
import it.infoblu.mobilemap.model.data.Poi;

/**
 * Created on 15/02/18.
 */

public class MapBoxAddMarkerAction extends AMapBoxAction {

    private final Bundle mBundle;


    public MapBoxAddMarkerAction(IMapsMapBoxActionBinder binder, Bundle bundle) {
        super(binder);
        mBundle = bundle;
    }

    @Override
    public Observable<IMapAction> exec() {
        return getBinder().getAsyncBundleObservable()
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .flatMap(new Function<AsyncBundle, ObservableSource<InternalBundle>>() {
                    @Override
                    public ObservableSource<InternalBundle> apply(final AsyncBundle asyncBundle) throws Exception {
                        return mBundle.markerImageLoaderManager.getMarkerBitmapBy(mBundle.poi)
                                .map(new Function<Bitmap, InternalBundle>() {
                                    @Override
                                    public InternalBundle apply(Bitmap bitmap) throws Exception {
                                        return new InternalBundle(asyncBundle, bitmap);
                                    }
                                });
                    }
                })
                .map(new Function<InternalBundle, IMapAction>() {
                    @Override
                    public IMapAction apply(InternalBundle internalBundle) throws Exception {
                        addMarkerSync(internalBundle.asyncBundle, mBundle.poi, internalBundle.iconBitmap);
                        return MapBoxAddMarkerAction.this;
                    }
                });
    }


    public void addMarkerSync(AsyncBundle asyncBundle, Poi poi, Bitmap iconBitmap) {
        final MarkerOptions markerOptions = new MarkerOptions()
                .position(new LatLng(poi.getPosition().getLat(), poi.getPosition().getLng()))
                .title(poi.getInfo().getTitle())
                .snippet(poi.getType());

        if (iconBitmap != null) {
            markerOptions.setIcon(IconFactory.getInstance(mBundle.context).fromBitmap(iconBitmap));
        }

        MarkerWrapper markerWrapper = mBundle.markersInfoStructure.getMarkerWrapper(poi);
        Marker newMarker = null;

        if (markerWrapper == null || markerWrapper.isVisible()) { // if marker is visible, replace it
            newMarker = asyncBundle.mapboxMap.addMarker(markerOptions);
        } // if not visible it will be added when the visibility flag change

        if (markerWrapper == null) {
            markerWrapper = new MarkerWrapper(newMarker, markerOptions);
            mBundle.markersInfoStructure.putPoiMarkerRelation(poi, markerWrapper);
        } else {
            if (markerWrapper.getMarker() != null) { // remove old marker
                asyncBundle.mapboxMap.removeMarker(markerWrapper.getMarker());
            }
            markerWrapper.setMarker(newMarker);
            markerWrapper.setOptions(markerOptions);
        }
    }

    @Override
    public String toString() {
        return "Action -> add Poi: " + mBundle.poi.toString();
    }


    /*
     * Structures
     */

    public static class Bundle {

        private final Context context;

        private final IMarkerImageLoader markerImageLoaderManager;

        private final MarkersInfoStructure markersInfoStructure;

        private final Poi poi;

        public Bundle(Context context, MarkersInfoStructure markersInfoStructure, IMarkerImageLoader markerImageLoaderManager, Poi poi) {
            this.context = context;
            this.markerImageLoaderManager = markerImageLoaderManager;
            this.markersInfoStructure = markersInfoStructure;
            this.poi = poi;
        }
    }

    private static class InternalBundle {

        private final AsyncBundle asyncBundle;

        private final Bitmap iconBitmap;

        public InternalBundle(AsyncBundle asyncBundle, Bitmap iconBitmap) {
            this.asyncBundle = asyncBundle;
            this.iconBitmap = iconBitmap;
        }
    }

}
