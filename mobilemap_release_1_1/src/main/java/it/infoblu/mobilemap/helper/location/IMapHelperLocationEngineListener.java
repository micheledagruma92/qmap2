package it.infoblu.mobilemap.helper.location;

import android.location.Location;

/**
 * Created on 28/06/18.
 */
public interface IMapHelperLocationEngineListener {

    void onLocationChange(Location location);

}
