package it.infoblu.mobilemap.helper.mapbox.constants;

/**
 * Created on 09/07/18.
 */
public final class MapBoxConstants {

    private MapBoxConstants() {
    }

    public static final String USER_LOCATION_BOTTOM_LAYER_ID = "mapbox-location-shadow";

    public static final String ANNOTATION_LAYER_ID = "com.mapbox.annotations.points";

}
