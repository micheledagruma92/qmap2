package it.infoblu.mobilemap.helper.config.cluster;

import java.util.HashMap;
import java.util.Map;

/**
 * Created on 22/03/18.
 */

public class ClusterSpecificInfoBundle {

    private Map<String, ClusterInfo> poiType2ClusterInfoMap = new HashMap<>();

    private ClusterSpecificInfoBundle() {
    }

    public Map<String, ClusterInfo> getPoiType2ClusterInfoMap() {
        return poiType2ClusterInfoMap;
    }

    @Override
    public String toString() {
        return "ClusterInfoBundle{" +
                "poiType2ClusterInfoMap=" + poiType2ClusterInfoMap +
                '}';
    }


    public static class Builder {

        private final ClusterSpecificInfoBundle clusterInfoBundle;

        public Builder() {
            clusterInfoBundle = new ClusterSpecificInfoBundle();
        }

        public Builder addClusterInfo(String poiType, ClusterInfo clusterInfo) {
            clusterInfoBundle.getPoiType2ClusterInfoMap().put(poiType, clusterInfo);
            return this;
        }

        public ClusterSpecificInfoBundle build() {
            return clusterInfoBundle;
        }

    }

}
