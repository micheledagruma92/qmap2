package it.infoblu.mobilemap.helper.config.poigeometry;

/**
 * Created on 21/03/18.
 */

public class NoPoiGeometrySettings implements IPoiGeometrySettings {

    @Override
    public PoiGeometryInfo getPoiGeometryInfo(String poiType) {
        return null;
    }

}
