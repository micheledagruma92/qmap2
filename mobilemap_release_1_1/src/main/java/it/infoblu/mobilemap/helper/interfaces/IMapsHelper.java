package it.infoblu.mobilemap.helper.interfaces;

import android.os.Bundle;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.Single;
import it.infoblu.mobilemap.helper.listeners.IMapHelperPermissionListener;
import it.infoblu.mobilemap.helper.listeners.OnMapClickListener;
import it.infoblu.mobilemap.helper.listeners.OnPoiClickListener;
import it.infoblu.mobilemap.model.data.MMPolyline;
import it.infoblu.mobilemap.model.data.Poi;
import it.infoblu.mobilemap.model.data.RemoteConfig;

/**
 * Created on 13/02/18.
 */

public interface IMapsHelper {

    interface FOLLOW_STATE {
        int FOLLOW = 0;
        int FREE = -1;
    }

    void onCreate(Bundle savedInstanceState);

    void onSaveInstanceState(Bundle outState);

    void onClear();

    void addHook(IMapHelperHook hook);

    void removeHook(IMapHelperHook hook);

    void setMapStyle(final String styleLabel);

    Observable<String> getCurrentMapStyleObservable();

    void setMarkerIconPlaceholders(Map<String, Integer> markerIconPlaceholderMap);

    void addOnPoiClickListener(OnPoiClickListener listener);

    void removeOnPoiClickListener(OnPoiClickListener listener);

    void addOnMapClickListener(OnMapClickListener listener);

    void removeOnMapClickListener(OnMapClickListener listener);

    void addOnPermissionListener(IMapHelperPermissionListener listener);

    void removeOnPermissionListener(IMapHelperPermissionListener listener);

    void addMarker(Poi poi);

    void addMarkers(List<Poi> poiList);

    void removeMarker(Poi poi);

    void removeMarkers(List<Poi> poiList);

    void updateMarkersLayer(String poiType, List<Poi> poiList);

    void setFilterEnabled(String filterId, boolean enabled);

    Single<List<RemoteConfig.MapFilter>> getFilterListSingle();

    MMPolyline addPolyline(String id, String geoJson, MMPolyline.Options options);

    void addPolyline(MMPolyline polyline);

    void removePolyline(MMPolyline polyline);

    void updatePolylineLayer(String tag, int width, int colorInt, List<com.google.android.gms.maps.model.LatLng> polylineList);

    void removePolylineLayer(String tag);

    void addMapSettingsConfig(String id, IMapsSettingsConfiguration mapsSettingsConfiguration);

    void selectMapSettingsConfig(String id);

    void deselectCurrentMapSettingsConfig();

    void followPosition(boolean follow);

    Observable<Integer> getFollowStateObservable();

    boolean isFollowPosition();

    void centerPosition(float zoom, long duration);

    void centerPosition(LatLng position, float zoom, long duration);

    void centerPosition(List<LatLng> positionList, int padding, long duration);

    void centerPosition(LatLngBounds bounds, int padding, long duration);

    LatLng getLastPosition();

    Observable<Boolean> getTrafficVisibilityObservable();

    void showTraffic(boolean show);

}
