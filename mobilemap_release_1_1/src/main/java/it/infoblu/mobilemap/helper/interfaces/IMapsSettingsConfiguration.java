package it.infoblu.mobilemap.helper.interfaces;

import it.infoblu.mobilemap.helper.mapbox.MapsMapBoxHelper;

/**
 * Created on 23/02/18.
 */

public interface IMapsSettingsConfiguration {

    void apply();

    void revert();

    boolean manageFollowState(MapsMapBoxHelper.LocationLayerBundle locationLayerBundle, int state);

}
