package it.infoblu.mobilemap.helper.mapbox.structures;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

/**
 * Created on 18/04/18.
 */
public class MapState implements Parcelable {

    public LatLng lastPosition;

    public LatLngBounds mapBound;

    public Double zoom;

    @Override
    public String toString() {
        return "MapState{" +
                "lastPosition=" + lastPosition +
                ", mapBound=" + mapBound +
                ", zoom=" + zoom +
                '}';
    }


    /*
     * Parcelable
     */

    public MapState() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.lastPosition, flags);
        dest.writeParcelable(this.mapBound, flags);
        dest.writeValue(this.zoom);
    }

    protected MapState(Parcel in) {
        this.lastPosition = in.readParcelable(LatLng.class.getClassLoader());
        this.mapBound = in.readParcelable(LatLngBounds.class.getClassLoader());
        this.zoom = (Double) in.readValue(Double.class.getClassLoader());
    }

    public static final Creator<MapState> CREATOR = new Creator<MapState>() {
        @Override
        public MapState createFromParcel(Parcel source) {
            return new MapState(source);
        }

        @Override
        public MapState[] newArray(int size) {
            return new MapState[size];
        }
    };
}
