package it.infoblu.mobilemap.helper.mapbox.utils;

/**
 * Created on 20/03/18.
 */

public final class PolylineLayerUtils {

    private PolylineLayerUtils() {
    }


    public static String getSourceId(String tag) {
        return "path_line_source_" + tag;
    }

    public static String getLayerId(String tag) {
        return "path_line_layer_" + tag;
    }

}
