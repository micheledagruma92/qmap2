package it.infoblu.mobilemap.helper.config.poiattribute;

/**
 * Created on 21/03/18.
 */

public class NoPoiAttributeSettings implements IPoiAttributeSettings {

    @Override
    public PoiAttributeInfo getPoiAttributeInfo(String poiType) {
        return null;
    }

}
