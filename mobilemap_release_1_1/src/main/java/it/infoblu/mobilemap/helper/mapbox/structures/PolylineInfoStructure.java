package it.infoblu.mobilemap.helper.mapbox.structures;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

import it.infoblu.mobilemap.model.data.MMPolyline;

/**
 * Created on 20/03/18.
 */

public class PolylineInfoStructure {

    private final BiMap<MMPolyline, PolylineWrapper> mPolyline2PolylineWrapperBiMap;

    public PolylineInfoStructure(int estimatedPolylineMaxNumber) {
        this.mPolyline2PolylineWrapperBiMap = HashBiMap.create(estimatedPolylineMaxNumber);;
    }

    public PolylineWrapper getPolylineWrapper(MMPolyline polyline) {
        return mPolyline2PolylineWrapperBiMap.get(polyline);
    }

    public PolylineWrapper putPolylineRelation(MMPolyline polyline, PolylineWrapper polylineWrapper) {
        return mPolyline2PolylineWrapperBiMap.put(polyline, polylineWrapper);
    }

    public PolylineWrapper removePolylineRelation(MMPolyline polyline) {
        return mPolyline2PolylineWrapperBiMap.remove(polyline);
    }

}
