package it.infoblu.mobilemap.helper.mapbox.manager;

import com.annimon.stream.Stream;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import it.infoblu.mobilemap.model.data.FilterStateInfo;
import it.infoblu.mobilemap.model.data.RemoteConfig;

/**
 * Created on 16/04/18.
 */
public class FilterStateManager {

    private List<FilterStateInfo> mFilterStateList = Collections.synchronizedList(new ArrayList<FilterStateInfo>());

    public FilterStateManager(List<FilterStateInfo> filterInfoList) {
        refresh(filterInfoList);
    }

    public void refresh(List<FilterStateInfo> filterInfoList) {
        if (filterInfoList == null) {
            return; // EXIT
        }
        mFilterStateList.clear();
        mFilterStateList.addAll(filterInfoList);
    }

    public ArrayList<FilterStateInfo> getFilterStateList() {
        synchronized (mFilterStateList) {
            return new ArrayList<>(mFilterStateList);
        }
    }

    public List<RemoteConfig.MapFilter> getMapFilters(RemoteConfig config) {
        return Stream.of(config.getFilters())
                // create a copy
                .map(new com.annimon.stream.function.Function<RemoteConfig.MapFilter, RemoteConfig.MapFilter>() {
                    @Override
                    public RemoteConfig.MapFilter apply(RemoteConfig.MapFilter mapFilter) {
                        return mapFilter.copy();
                    }
                })
                // filter and modify copy
                .map(new com.annimon.stream.function.Function<RemoteConfig.MapFilter, RemoteConfig.MapFilter>() {
                    @Override
                    public RemoteConfig.MapFilter apply(RemoteConfig.MapFilter mapFilter) {
                        for (FilterStateInfo filterState : mFilterStateList) {
                            if (StringUtils.equals(mapFilter.getId(), filterState.getId())) {
                                // copy modification
                                mapFilter.setEnabled(filterState.isEnabled());
                            }
                        }
                        return mapFilter;
                    }
                }).toList();
    }

    public void setFilterEnabled(RemoteConfig.MapFilter selectedFilter) {
        synchronized (mFilterStateList) {
            for (FilterStateInfo filterStateInfo : mFilterStateList) {
                if (StringUtils.equals(filterStateInfo.getId(), selectedFilter.getId())) {
                    // filter is already added
                    filterStateInfo.setEnabled(selectedFilter.isEnabled());
                    break; // EXIT
                }
            }
            // filter isn't added yet
            mFilterStateList.add(new FilterStateInfo(selectedFilter.getId(), selectedFilter.isEnabled()));
        }
    }

}
