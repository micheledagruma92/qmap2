package it.infoblu.mobilemap.helper.mapbox.interfaces;

import com.mapbox.android.core.location.LocationEngine;
import com.mapbox.mapboxsdk.plugins.locationlayer.LocationLayerPlugin;

import io.reactivex.Single;
import io.reactivex.subjects.BehaviorSubject;

/**
 * Created on 15/03/18.
 */

public interface IMapSettingsBinder {

    BehaviorSubject<Integer> getFollowStateSubject();

    LocationEngine getLocationEngine();

    Single<LocationLayerPlugin> getLocationLayerPluginSingle();

}
