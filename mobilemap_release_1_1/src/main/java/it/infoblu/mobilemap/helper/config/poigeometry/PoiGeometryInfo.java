package it.infoblu.mobilemap.helper.config.poigeometry;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

/**
 * Created on 21/03/18.
 */

public class PoiGeometryInfo {

    private final static String KEY_DEFAULT = "def";

    @JsonProperty("severityInfo")
    protected Map<String, SeverityInfo> severity2InfoMap = new HashMap<>();


    protected PoiGeometryInfo() {
    }

    public SeverityInfo getSeverityInfo(int severity) {
        SeverityInfo severityInfo = severity2InfoMap.get(severity);
        return severityInfo != null ? severityInfo : severity2InfoMap.get(KEY_DEFAULT);
    }

    @Override
    public String toString() {
        return "PoiGeometryInfo{" +
                "severity2InfoMap=" + severity2InfoMap +
                '}';
    }


    public static class Builder {

        private PoiGeometryInfo poiGeometryInfo;

        public Builder() {
            poiGeometryInfo = new PoiGeometryInfo();
        }

        public Builder addSeverityInfo(int severity, SeverityInfo info) {
            poiGeometryInfo.severity2InfoMap.put(Integer.toString(severity), info);
            return this;
        }

        public Builder defaultSeverityInfo(SeverityInfo info) {
            poiGeometryInfo.severity2InfoMap.put(KEY_DEFAULT, info);
            return this;
        }

        public PoiGeometryInfo build() {
            if (poiGeometryInfo.severity2InfoMap.get(KEY_DEFAULT) == null) {
                throw new RuntimeException("Default SeverityInfo must not be null");
            }
            return poiGeometryInfo;
        }

    }


    public static class SeverityInfo {

        private float width;

        private String color;

        protected SeverityInfo() {
        }

        public SeverityInfo(float width, String color) {
            this.width = width;
            this.color = color;
        }

        public float getWidth() {
            return width;
        }

        public void setWidth(int width) {
            this.width = width;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }

        @Override
        public String toString() {
            return "SeverityInfo{" +
                    "width=" + width +
                    ", color=" + color +
                    '}';
        }
    }

}
