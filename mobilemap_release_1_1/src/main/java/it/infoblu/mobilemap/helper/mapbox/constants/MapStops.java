package it.infoblu.mobilemap.helper.mapbox.constants;

import com.mapbox.mapboxsdk.style.expressions.Expression;
import com.mapbox.mapboxsdk.style.layers.Property;

/**
 * Created on 13/04/18.
 */
public final class MapStops {

    private MapStops() {
    }

    public static final Expression.Stop[] ICON_ANCHOR_STOPS = new Expression.Stop[2];

    static {
        ICON_ANCHOR_STOPS[0] = Expression.stop("center", Property.ICON_ANCHOR_CENTER);
        ICON_ANCHOR_STOPS[1] = Expression.stop("bottom", Property.ICON_ANCHOR_BOTTOM);
    }

}
