package it.infoblu.mobilemap.helper.mapbox.interfaces;

import com.mapbox.mapboxsdk.maps.MapboxMap;

import io.reactivex.Observable;
import it.infoblu.mobilemap.helper.mapbox.structures.AsyncBundle;

/**
 * Created on 15/02/18.
 */

public interface IMapsMapBoxActionBinder {

    Observable<AsyncBundle> getAsyncBundleObservable();

    Observable<MapboxMap> getMapboxMapObservable();

}
