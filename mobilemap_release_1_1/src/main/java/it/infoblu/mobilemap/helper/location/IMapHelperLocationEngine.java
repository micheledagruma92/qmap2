package it.infoblu.mobilemap.helper.location;

import android.location.Location;

/**
 * Created on 28/06/18.
 */
public interface IMapHelperLocationEngine {

    void activate();

    void deactivate();

    void start();

    void stop();

    Location getLastLocation();

    void addLocationUpdateListener(IMapHelperLocationEngineListener locationListener);

    void removeLocationUpdateListener(IMapHelperLocationEngineListener locationListener);

    boolean isConnected();
}
