package it.infoblu.mobilemap.helper.mapbox.structures;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.mapbox.mapboxsdk.annotations.Marker;

import it.infoblu.mobilemap.model.data.Poi;

/**
 * Created on 20/03/18.
 */

public class MarkersInfoStructure {

    private final BiMap<Poi, MarkerWrapper> mPoi2MarkerBiMap;

    public MarkersInfoStructure(int estimatedPoiMaxNumber) {
        this.mPoi2MarkerBiMap = HashBiMap.create(estimatedPoiMaxNumber);
    }


    public MarkerWrapper getMarkerWrapper(Poi poi) {
        return mPoi2MarkerBiMap.get(poi);
    }

    public Poi getPoi(MarkerWrapper markerWrapper) {
        return mPoi2MarkerBiMap.inverse().get(markerWrapper);
    }

    public Poi getPoi(Marker marker) {
        return getPoi(new MarkerWrapper(marker, null));
    }

    public MarkerWrapper putPoiMarkerRelation(Poi poi, MarkerWrapper markerWrapper) {
        return mPoi2MarkerBiMap.put(poi, markerWrapper);
    }

    public MarkerWrapper removePoiMarkerRelation(Poi poi) {
        return mPoi2MarkerBiMap.remove(poi);
    }

}
