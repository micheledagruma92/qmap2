package it.infoblu.mobilemap.helper.mapbox.action;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.Pair;

import com.mapbox.geojson.FeatureCollection;
import com.mapbox.mapboxsdk.style.expressions.Expression;
import com.mapbox.mapboxsdk.style.layers.Layer;
import com.mapbox.mapboxsdk.style.layers.LineLayer;
import com.mapbox.mapboxsdk.style.layers.Property;
import com.mapbox.mapboxsdk.style.layers.PropertyFactory;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.orhanobut.logger.Logger;

import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import it.infoblu.mobilemap.R;
import it.infoblu.mobilemap.helper.config.poigeometry.PoiGeometryInfo;
import it.infoblu.mobilemap.helper.interfaces.IMapAction;
import it.infoblu.mobilemap.helper.mapbox.constants.MapBoxConstants;
import it.infoblu.mobilemap.helper.mapbox.constants.MapStops;
import it.infoblu.mobilemap.helper.mapbox.interfaces.IMapsMapBoxActionBinder;
import it.infoblu.mobilemap.helper.mapbox.manager.ClusterManager;
import it.infoblu.mobilemap.helper.mapbox.structures.AsyncBundle;
import it.infoblu.mobilemap.helper.mapbox.structures.MarkerLayersInfoStructure;
import it.infoblu.mobilemap.helper.mapbox.utils.InternalMapBoxUtils;
import it.infoblu.mobilemap.helper.mapbox.utils.MarkerLayerUtils;
import it.infoblu.mobilemap.manager.IMarkerImageLoader;
import it.infoblu.mobilemap.model.constants.InternalGeometryArrowProperty;
import it.infoblu.mobilemap.model.constants.PoiProperty;
import it.infoblu.mobilemap.model.data.Poi;
import it.infoblu.mobilemap.utils.FilterUtils;
import it.infoblu.mobilemap.utils.ViewUtils;


/**
 * Created on 15/02/18.
 * <p>
 * https://blog.mapbox.com/a-guide-to-the-android-symbollayer-api-5daac7b66f2c
 * https://github.com/mapbox/mapbox-gl-native/issues/7461
 */

public class MapBoxUpdateMarkersLayerAction extends AMapBoxAction {

    private final Bundle mBundle;


    public MapBoxUpdateMarkersLayerAction(IMapsMapBoxActionBinder binder, Bundle bundle) {
        super(binder);
        mBundle = bundle;
    }

    @Override
    public Observable<IMapAction> exec() {
        return getBinder().getAsyncBundleObservable()
                .subscribeOn(Schedulers.computation())
                .observeOn(Schedulers.computation())
                .flatMap((Function<AsyncBundle, ObservableSource<AsyncBundle>>) asyncBundle ->
                        mBundle.markerImageLoaderManager.downloadMarkerBitmapsBy(mBundle.poiList)
                                .map(poiMarkerDownloadResults -> asyncBundle))
                .observeOn(Schedulers.computation())
                .map(asyncBundle -> {
                    // marker icon
                    final Map<String, Bitmap> markerIconMap = new HashMap<>();
                    for (Poi poi : mBundle.poiList) {
                        String key = StringUtils.defaultIfBlank(poi.getInfo().getIcoRelativeUrl(), PoiProperty.DEFAULT.ICO_RELATIVE_URL);
                        if (markerIconMap.containsKey(key)) {
                            continue; // SKIP
                        }
                        Bitmap markerIcon = mBundle.markerImageLoaderManager.getMarkerBitmapSyncFromCacheBy(poi);
                        if (markerIcon == null) {
                            continue; // SKIP
                        }
                        markerIconMap.put(key, markerIcon);
                    }
                    // feature collections
                    FeatureCollection markerFeatureCollection = InternalMapBoxUtils.convertPoiListToFeatureCollection(mBundle.poiList);
                    FeatureCollection internalGeometryFeatureCollection = InternalMapBoxUtils.convertPoiListToInternalGeometryToFeatureCollection(mBundle.poiList);
                    FeatureCollection internalGeometryArrowFeatureCollection = InternalMapBoxUtils.convertPoiListToInternalGeometryArrowToFeatureCollection(mBundle.poiList);
                    // severity
                    Pair<Integer, Integer> poiSeverityInterval = createPoiSeverityInterval(mBundle.poiList);
                    // filter
                    boolean isFilterVisible = FilterUtils.isPoiTypeVisible(asyncBundle.filterList, mBundle.type);
                    return new InternalBundle(asyncBundle, markerIconMap, isFilterVisible
                            , markerFeatureCollection
                            , internalGeometryFeatureCollection
                            , internalGeometryArrowFeatureCollection
                            , poiSeverityInterval);
                })
                .observeOn(AndroidSchedulers.mainThread())
                .map(internalBundle -> {
                    addMarkersLayerSync(internalBundle, mBundle.poiList);
                    return MapBoxUpdateMarkersLayerAction.this;
                });
    }

    private void addMarkersLayerSync(InternalBundle internalBundle, List<Poi> poiList) {

        int zoomMin = internalBundle.asyncBundle.config.getPlace(mBundle.type) == null ? 0
                : internalBundle.asyncBundle.config.getPlace(mBundle.type).getZoomMin();
        int zoomMax = internalBundle.asyncBundle.config.getPlace(mBundle.type) == null ? 0
                : internalBundle.asyncBundle.config.getPlace(mBundle.type).getZoomMax();

        String markerLayerId = performAddMarkerLayer(
                internalBundle.asyncBundle
                , internalBundle.key2imageMap
                , internalBundle.isFilterVisible
                , internalBundle.markerFeatureCollection
                , zoomMin, zoomMax);

        // useful to add markerLayerId reference
        mBundle.markerLayersInfo.updateMarkerLayer(poiList, mBundle.type);

        String internalGeometryLayerId = performAddInternalGeometryLayer(
                internalBundle.asyncBundle
                , internalBundle.isFilterVisible
                , internalBundle.internalGeometryFeatureCollection
                , internalBundle.poiSeverityInterval
                , zoomMin, zoomMax);

        mBundle.markerLayersInfo.updateMarkerGeometryLayer(mBundle.type, internalGeometryLayerId);

        String internalGeometryArrowLayerId = performAddInternalGeometryArrowLayer(
                internalBundle.asyncBundle
                , internalBundle.isFilterVisible
                , internalBundle.internalGeometryArrowFeatureCollection
                , internalGeometryLayerId
                , zoomMin, zoomMax);

        mBundle.markerLayersInfo.updateMarkerGeometryArrowLayer(mBundle.type, internalGeometryArrowLayerId);

        mBundle.clusterManager.addClusterLayers(mBundle.context, internalBundle.asyncBundle.mapboxMap, mBundle.type, internalBundle.isFilterVisible, zoomMin, zoomMax);
    }

    @Override
    public String toString() {
        return "Action -> update Poi list as layer: " + mBundle.poiList.toString();
    }


    /*
     * Internal methods
     */

    private String performAddMarkerLayer(final AsyncBundle asyncBundle, Map<String, Bitmap> key2imageMap
            , boolean isFilterVisible, FeatureCollection markerFeatureCollection, int zoomMin, int zoomMax) {

        // init images
        for (Map.Entry<String, Bitmap> entry : key2imageMap.entrySet()) {
            if (asyncBundle.mapboxMap.getImage(entry.getKey()) == null) {
                asyncBundle.mapboxMap.addImage(entry.getKey(), entry.getValue());
            }
        }

        String markerSourceId = MarkerLayerUtils.getSourceId(mBundle.type);
        String markerLayerId = MarkerLayerUtils.getLayerId(mBundle.type);

        GeoJsonSource markersSource = (GeoJsonSource) asyncBundle.mapboxMap.getSource(markerSourceId);

        if (markersSource == null) {
            markersSource = new GeoJsonSource(markerSourceId, markerFeatureCollection
                    , mBundle.clusterManager.getGeoJsonOptions(mBundle.type)
            );
            asyncBundle.mapboxMap.addSource(markersSource);
        } else {
            markersSource.setGeoJson(markerFeatureCollection);
        }

        Layer markersLayer = asyncBundle.mapboxMap.getLayer(markerLayerId);
        if (markersLayer == null) {
            markersLayer = new SymbolLayer(markerLayerId, markerSourceId)
                    .withProperties(
                            PropertyFactory.iconAllowOverlap(true) //
                            , PropertyFactory.iconIgnorePlacement(true)
                            , PropertyFactory.iconImage(Expression.get(PoiProperty.ICO_RELATIVE_URL))
                            , PropertyFactory.iconAnchor(Expression.match(Expression.get(PoiProperty.ICO_ANCHOR)
                                    , Expression.literal(Property.ICON_ANCHOR_CENTER), MapStops.ICON_ANCHOR_STOPS))
                    );
            if (zoomMin > 0) {
                markersLayer.setMinZoom(zoomMin);
            }
            if (zoomMax > 0) {
                markersLayer.setMaxZoom(zoomMax);
            }

            String anchorLayerId = mBundle.markerLayersInfo.getMarkerTopLayerId();

            if (TextUtils.isEmpty(anchorLayerId)) {
                asyncBundle.mapboxMap.addLayer(markersLayer);
            } else {
                asyncBundle.mapboxMap.addLayerAbove(markersLayer, anchorLayerId);
            }
        }

        if (!isFilterVisible) {
            markersLayer.setProperties(PropertyFactory.visibility(Property.NONE));
        }

        return markerLayerId;
    }

    private String performAddInternalGeometryLayer(AsyncBundle asyncBundle, boolean isFilterVisible
            , FeatureCollection internalGeometryFeatureCollection, Pair<Integer, Integer> poiSeverityInterval
            , int zoomMin, int zoomMax) {

        if (internalGeometryFeatureCollection.features() == null
                || (internalGeometryFeatureCollection.features().isEmpty()
                && !mBundle.markerLayersInfo.hasPoiGeometryLayer(mBundle.type))) {
            return null; // EXIT: there isn't any internal geometry layer for this poiList
        }

        String internalGeometrySourceId = MarkerLayerUtils.getInternalGeometrySourceId(mBundle.type);
        String internalGeometryLayerId = MarkerLayerUtils.getInternalGeometryLayerId(mBundle.type);

        GeoJsonSource internalGeometrySource = (GeoJsonSource) asyncBundle.mapboxMap.getSource(internalGeometrySourceId);

        if (internalGeometrySource == null) {
            internalGeometrySource = new GeoJsonSource(internalGeometrySourceId, internalGeometryFeatureCollection);
            asyncBundle.mapboxMap.addSource(internalGeometrySource);
        } else {
            internalGeometrySource.setGeoJson(internalGeometryFeatureCollection);
        }

        int severityRange = mBundle.poiGeometryInfo == null ? 0 // safe check
                : poiSeverityInterval.second - poiSeverityInterval.first + 1;

        Expression.Stop[] severityWidthStops = new Expression.Stop[severityRange];
        Expression.Stop[] severityColorStops = new Expression.Stop[severityRange];

        if (mBundle.poiGeometryInfo != null) {
            for (int severity = poiSeverityInterval.first; severity <= poiSeverityInterval.second; severity++) {
                PoiGeometryInfo.SeverityInfo severityInfo = mBundle.poiGeometryInfo.getSeverityInfo(severity);
                severityWidthStops[severity] = Expression.stop(severity, severityInfo.getWidth());
                severityColorStops[severity] = Expression.stop(severity, Expression.literal(severityInfo.getColor()));
            }
        }

        Layer internalGeometryLayer = asyncBundle.mapboxMap.getLayer(internalGeometryLayerId);
        if (internalGeometryLayer == null) {
            internalGeometryLayer = new LineLayer(internalGeometryLayerId, internalGeometrySourceId)
                    .withProperties(
                            PropertyFactory.lineCap(Property.LINE_CAP_ROUND)
                            , PropertyFactory.lineJoin(Property.LINE_JOIN_ROUND)
                            , PropertyFactory.lineWidth(Expression.step(Expression.get(PoiProperty.SEVERITY), 0, severityWidthStops))
                            , PropertyFactory.lineColor(Expression.step(Expression.get(PoiProperty.SEVERITY), Expression.literal("#fff"), severityColorStops))
                    );

            int internalGeometryZoomMin = asyncBundle.config.getPlace(mBundle.type) == null ? 0
                    : asyncBundle.config.getPlace(mBundle.type).getPoiGeometryZoomMin();

            int normalizedZoomMin = Math.max(zoomMin, Math.max(internalGeometryZoomMin, mBundle.clusterManager.getClusterZoomMax(mBundle.type) + 1));

            if (normalizedZoomMin > 0) {
                internalGeometryLayer.setMinZoom(normalizedZoomMin);
            }
            if (zoomMax > 0) {
                internalGeometryLayer.setMaxZoom(zoomMax);
            }

            String anchorLayerId = MapBoxConstants.ANNOTATION_LAYER_ID;

            // removed to test other try (see below)
//            if (asyncBundle.mapboxMap.getLayer(MapBoxConstants.USER_LOCATION_BOTTOM_LAYER_ID) != null) {
//                anchorLayerId = MapBoxConstants.USER_LOCATION_BOTTOM_LAYER_ID;
//            } else {
//                anchorLayerId = StringUtils.defaultString(mBundle.markerLayersInfo.getMarkerBottomLayerId(), MarkerLayerUtils.getLayerId(mBundle.type));
//            }

            asyncBundle.mapboxMap.addLayerBelow(internalGeometryLayer, anchorLayerId);
        }

        if (!isFilterVisible) {
            internalGeometryLayer.setProperties(PropertyFactory.visibility(Property.NONE));
        }

        return internalGeometryLayerId;
    }

    private String performAddInternalGeometryArrowLayer(AsyncBundle asyncBundle, boolean isFilterVisible
            , FeatureCollection internalGeometryArrowFeatureCollection, String internalGeometryLayerId
            , int zoomMin, int zoomMax) {

        if (internalGeometryArrowFeatureCollection.features() == null
                || (internalGeometryArrowFeatureCollection.features().isEmpty()
                && !mBundle.markerLayersInfo.hasPoiGeometryArrowLayer(mBundle.type))) {
            return null; // EXIT: there isn't any internal geometry layer for this poiList
        }

        if (internalGeometryLayerId == null) {
            Logger.e(String.format("ERROR: internalGeometryLayerId -> %s", internalGeometryLayerId));
        }

        // TODO: 09/04/18 rendere customizzabile (immagine e colore)
        if (asyncBundle.mapboxMap.getImage("internal_geometry_arrow") == null) {
            asyncBundle.mapboxMap.addImage("internal_geometry_arrow", ViewUtils.drawableToBitmap(mBundle.context.getDrawable(R.drawable.internal_geometry_arrow)));
        }

        String internalGeometryArrowSourceId = MarkerLayerUtils.getInternalGeometryArrowSourceId(mBundle.type);
        String internalGeometryArrowLayerId = MarkerLayerUtils.getInternalGeometryArrowLayerId(mBundle.type);

        GeoJsonSource internalGeometryArrowSource = (GeoJsonSource) asyncBundle.mapboxMap.getSource(internalGeometryArrowSourceId);

        if (internalGeometryArrowSource == null) {
            internalGeometryArrowSource = new GeoJsonSource(internalGeometryArrowSourceId, internalGeometryArrowFeatureCollection);
            asyncBundle.mapboxMap.addSource(internalGeometryArrowSource);
        } else {
            internalGeometryArrowSource.setGeoJson(internalGeometryArrowFeatureCollection);
        }

        Layer internalGeometryArrowLayer = asyncBundle.mapboxMap.getLayer(internalGeometryArrowLayerId);
        if (internalGeometryArrowLayer == null) {
            internalGeometryArrowLayer = new SymbolLayer(internalGeometryArrowLayerId, internalGeometryArrowSourceId)
                    .withProperties(
                            PropertyFactory.iconImage("internal_geometry_arrow")
                            , PropertyFactory.iconRotate(Expression.get(InternalGeometryArrowProperty.ROTATION))
                            , PropertyFactory.iconRotationAlignment(Property.ICON_ROTATION_ALIGNMENT_MAP)
                            , PropertyFactory.iconAllowOverlap(true)
                            , PropertyFactory.iconIgnorePlacement(true)
                    );

            int internalGeometryZoomMin = asyncBundle.config.getPlace(mBundle.type) == null ? 0
                    : asyncBundle.config.getPlace(mBundle.type).getPoiGeometryZoomMin();

            int normalizedZoomMin = Math.max(zoomMin, Math.max(internalGeometryZoomMin, mBundle.clusterManager.getClusterZoomMax(mBundle.type) + 1));

            if (normalizedZoomMin > 0) {
                internalGeometryArrowLayer.setMinZoom(normalizedZoomMin);
            }
            if (zoomMax > 0) {
                internalGeometryArrowLayer.setMaxZoom(zoomMax);
            }

            asyncBundle.mapboxMap.addLayerAbove(internalGeometryArrowLayer, internalGeometryLayerId);
        }

        if (!isFilterVisible) {
            internalGeometryArrowLayer.setProperties(PropertyFactory.visibility(Property.NONE));
        }

        return internalGeometryArrowLayerId;
    }


    /*
     * Utility methods
     */

    private static Pair<Integer, Integer> createPoiSeverityInterval(List<Poi> poiList) {
        int min = 0;
        int max = 0;
        for (Poi poi : poiList) {
            min = Math.min(min, poi.getInfo().getSeverity());
            max = Math.max(max, poi.getInfo().getSeverity());
        }
        return Pair.create(min, max);
    }


    /*
     * Structures
     */

    public static class Bundle {

        private final Context context;

        private final IMarkerImageLoader markerImageLoaderManager;

        private final ClusterManager clusterManager;

        private final MarkerLayersInfoStructure markerLayersInfo;

        private final PoiGeometryInfo poiGeometryInfo;

        private final String type;

        private final List<Poi> poiList;

        public Bundle(Context context, MarkerLayersInfoStructure markerLayersInfo
                , IMarkerImageLoader markerImageLoaderManager, ClusterManager clusterManager, PoiGeometryInfo poiGeometryInfo, String type, List<Poi> poiList) {
            this.context = context;
            this.markerImageLoaderManager = markerImageLoaderManager;
            this.clusterManager = clusterManager;
            this.markerLayersInfo = markerLayersInfo;
            this.poiGeometryInfo = poiGeometryInfo;
            this.type = type;
            this.poiList = poiList;
        }
    }

    private static class InternalBundle {

        private final AsyncBundle asyncBundle;

        private final Map<String, Bitmap> key2imageMap;

        private final boolean isFilterVisible;

        private final FeatureCollection markerFeatureCollection;

        private final FeatureCollection internalGeometryFeatureCollection;

        private final FeatureCollection internalGeometryArrowFeatureCollection;

        private final Pair<Integer, Integer> poiSeverityInterval;

        public InternalBundle(AsyncBundle asyncBundle, Map<String, Bitmap> key2imageMap, boolean isFilterVisible
                , FeatureCollection markerFeatureCollection, FeatureCollection internalGeometryFeatureCollection
                , FeatureCollection internalGeometryArrowFeatureCollection, Pair<Integer, Integer> poiSeverityInterval) {
            this.asyncBundle = asyncBundle;
            this.key2imageMap = key2imageMap;
            this.isFilterVisible = isFilterVisible;
            this.markerFeatureCollection = markerFeatureCollection;
            this.internalGeometryFeatureCollection = internalGeometryFeatureCollection;
            this.internalGeometryArrowFeatureCollection = internalGeometryArrowFeatureCollection;
            this.poiSeverityInterval = poiSeverityInterval;
        }
    }

}
