package it.infoblu.mobilemap.helper.mapbox.structures;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentSkipListMap;

import it.infoblu.mobilemap.helper.mapbox.utils.MarkerLayerUtils;
import it.infoblu.mobilemap.model.data.Poi;

/**
 * Created on 20/03/18.
 */

public class MarkerLayersInfoStructure {

    private final ConcurrentSkipListMap<String, MarkerLayerInternal> markerLayerId2PoisMap = new ConcurrentSkipListMap<>();


    public MarkerLayersInfoStructure() {
    }


    public List<String> getMarkerLayerIdList() {
        if (markerLayerId2PoisMap.keySet() == null) {
            return new ArrayList<>(); // EXIT
        }
        synchronized (markerLayerId2PoisMap) {
            return new ArrayList<>(markerLayerId2PoisMap.keySet());
        }
    }

    public String[] getMarkerLayerIds() {
        return getMarkerLayerIdList().toArray(new String[0]);
    }

    public boolean updateMarkerLayer(List<Poi> poiList, String poiType) {
        if (poiList == null) { // safe check
            poiList = new ArrayList<>();
        }
        MarkerLayerInternal markerLayerInternal = new MarkerLayerInternal();
        synchronized (poiList) {
            for (Poi poi : poiList) {
                markerLayerInternal.addPoi(poi.getId(), poi);
            }
        }
        String markerLayerId = MarkerLayerUtils.getLayerId(poiType);
        MarkerLayerInternal prev = markerLayerId2PoisMap.put(markerLayerId, markerLayerInternal);
        return prev != null; // return true if updated
    }

    public boolean updateMarkerGeometryLayer(String poiType, String internalGeometryLayerId) {
        String markerLayerId = MarkerLayerUtils.getLayerId(poiType);
        MarkerLayerInternal markerLayerInternal = markerLayerId2PoisMap.get(markerLayerId);
        if (markerLayerInternal == null) return false; // EXIT
        boolean hasPrev = markerLayerInternal.getPoiGeometryLayerId() != null;
        markerLayerInternal.setPoiGeometryLayerId(internalGeometryLayerId);
        return hasPrev;
    }

    public boolean updateMarkerGeometryArrowLayer(String poiType, String internalGeometryArrowLayerId) {
        String markerLayerId = MarkerLayerUtils.getLayerId(poiType);
        MarkerLayerInternal markerLayerInternal = markerLayerId2PoisMap.get(markerLayerId);
        if (markerLayerInternal == null) return false; // EXIT
        boolean hasPrev = markerLayerInternal.getPoiGeometryArrowLayerId() != null;
        markerLayerInternal.setPoiGeometryArrowLayerId(internalGeometryArrowLayerId);
        return hasPrev;
    }

    public Poi getPoi(String poiType, String poiId) {
        MarkerLayerInternal markerLayerInternal = markerLayerId2PoisMap.get(MarkerLayerUtils.getLayerId(poiType));
        return markerLayerInternal != null ? markerLayerInternal.getId2PoiMap().get(poiId) : null;
    }

    public String getMarkerTopLayerId() {
        return markerLayerId2PoisMap.isEmpty() ? null
                : markerLayerId2PoisMap.lastKey();
    }

    public String getMarkerBottomLayerId() {
        return markerLayerId2PoisMap.isEmpty() ? null
                : markerLayerId2PoisMap.firstKey();
    }

    public String getPoiGeometryTopLayerId() {
        return markerLayerId2PoisMap.isEmpty() ? null
                : markerLayerId2PoisMap.lastEntry().getValue().getPoiGeometryLayerId();
    }

    public String getPoiGeometryBottomLayerId() {
        return markerLayerId2PoisMap.isEmpty() ? null
                : markerLayerId2PoisMap.firstEntry().getValue().getPoiGeometryLayerId();
    }

    public boolean hasPoiLayer(String poiType) {
        String markerLayerId = MarkerLayerUtils.getLayerId(poiType);
        return markerLayerId2PoisMap.containsKey(markerLayerId);
    }

    public boolean hasPoiGeometryLayer(String poiType) {
        String markerLayerId = MarkerLayerUtils.getLayerId(poiType);
        MarkerLayerInternal markerLayerInternal = markerLayerId2PoisMap.get(markerLayerId);
        return markerLayerInternal != null
                && markerLayerInternal.getPoiGeometryLayerId() != null;
    }

    public boolean hasPoiGeometryArrowLayer(String poiType) {
        String markerLayerId = MarkerLayerUtils.getLayerId(poiType);
        MarkerLayerInternal markerLayerInternal = markerLayerId2PoisMap.get(markerLayerId);
        return markerLayerInternal != null
                && markerLayerInternal.getPoiGeometryArrowLayerId() != null;
    }

    public void clear() {
        markerLayerId2PoisMap.clear();
    }


    /*
     * Internal structure
     */

    private static class MarkerLayerInternal {

        private Map<String, Poi> id2PoiMap = new HashMap<>();

        private String poiGeometryLayerId;

        private String poiGeometryArrowLayerId;

        public Map<String, Poi> getId2PoiMap() {
            return id2PoiMap;
        }

        public void addPoi(String poiId, Poi poi) {
            id2PoiMap.put(poiId, poi);
        }

        public String getPoiGeometryLayerId() {
            return poiGeometryLayerId;
        }

        public void setPoiGeometryLayerId(String poiGeometryLayerId) {
            this.poiGeometryLayerId = poiGeometryLayerId;
        }

        public String getPoiGeometryArrowLayerId() {
            return poiGeometryArrowLayerId;
        }

        public void setPoiGeometryArrowLayerId(String poiGeometryArrowLayerId) {
            this.poiGeometryArrowLayerId = poiGeometryArrowLayerId;
        }
    }

}
