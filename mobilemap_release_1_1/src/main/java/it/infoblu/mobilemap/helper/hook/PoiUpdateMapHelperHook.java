package it.infoblu.mobilemap.helper.hook;

import com.annimon.stream.Stream;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import it.infoblu.mobilemap.IMobileMapSession;
import it.infoblu.mobilemap.helper.interfaces.IMapHelperHook;
import it.infoblu.mobilemap.helper.interfaces.IMapsHelper;
import it.infoblu.mobilemap.helper.mapbox.utils.InternalMapBoxUtils;
import it.infoblu.mobilemap.model.data.Poi;
import it.infoblu.mobilemap.model.data.PoiUpdateInfo;
import it.infoblu.mobilemap.utils.InternalLogger;

/**
 * Created on 21/02/18.
 */

public class PoiUpdateMapHelperHook implements IMapHelperHook {

    private final List<String> mAvailablePoiTypeList;

    private CompositeDisposable mDisposables;

    private boolean mIsInitialized;


    public PoiUpdateMapHelperHook(String... poiTypes) {
        mAvailablePoiTypeList = Arrays.asList(poiTypes == null ? new String[0] : poiTypes);
    }

    @Override
    public void bind(final IMapsHelper mapsHelper, final IMobileMapSession session) {
        unbind(); // flush

        if (mDisposables == null) {
            mDisposables = new CompositeDisposable();
        }

        mDisposables.add(mapsHelper.getCurrentMapStyleObservable()
                .flatMap((Function<String, ObservableSource<HashSet<Poi>>>) mapStyleLabel -> session.getPoiListObservable())
                .subscribeOn(Schedulers.computation())
                .observeOn(Schedulers.computation())
                .map(pois -> {
                    List<PoiUpdateInfo> poiUpdateList = InternalMapBoxUtils.splitToPoiUpdateInfoList(pois);
                    if (mAvailablePoiTypeList.isEmpty()) {
                        return poiUpdateList;
                    }
                    return Stream.of(poiUpdateList).filter(value ->
                            mAvailablePoiTypeList.contains(value.getType())).toList();
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(poiUpdateInfoList -> {
                    mIsInitialized = true;
                    for (PoiUpdateInfo poiUpdateInfo : poiUpdateInfoList) {
                        mapsHelper.updateMarkersLayer(poiUpdateInfo.getType(), poiUpdateInfo.getPoiList());
                    }
                }));

        mDisposables.add(session.getPoiUpdateObservable()
                .subscribeOn(Schedulers.computation())
                .observeOn(Schedulers.computation())
                .filter(poiUpdateInfo -> mIsInitialized)
                .filter(poiUpdateInfo -> mAvailablePoiTypeList.isEmpty() || mAvailablePoiTypeList.contains(poiUpdateInfo.getType()))
                .subscribe(poiUpdateInfo -> {
                    InternalLogger.d("update %s poi markers of type: %s", poiUpdateInfo.getPoiList().size(), poiUpdateInfo.getType());
//                        mapsHelper.addMarkers(poiUpdateInfo.getPoiList());
                    mapsHelper.updateMarkersLayer(poiUpdateInfo.getType(), poiUpdateInfo.getPoiList());
                }));
    }

    @Override
    public void unbind() {
        if (mDisposables != null && !mDisposables.isDisposed()) {
            mDisposables.dispose();
            mDisposables = null;
        }
    }

}
