package it.infoblu.mobilemap.helper.mapbox.utils;

/**
 * Created on 20/03/18.
 */

public final class MarkerLayerUtils {

    private MarkerLayerUtils() {
    }


    public static String getSourceId(String poiType) {
        return "marker_source_" + poiType;
    }

    public static String getLayerId(String poiType) {
        return "marker_layer_" + poiType;
    }


    public static String getInternalGeometrySourceId(String poiType) {
        return "internal_geometry_source_" + poiType;
    }

    public static String getInternalGeometryLayerId(String poiType) {
        return "internal_geometry_layer_" + poiType;
    }

    public static String getInternalGeometryArrowSourceId(String poiType) {
        return "internal_geometry_arrow_source_" + poiType;
    }

    public static String getInternalGeometryArrowLayerId(String poiType) {
        return "internal_geometry_arrow_layer_" + poiType;
    }

    public static String getClusterMarkerImageLayerId(String poiType) {
        return "cluster-" + poiType;
    }

    public static String getClusterMarkerImageLayerIconId(String poiType, int range) {
        return "img-cluster-" + poiType + "-" + range;
    }

}
