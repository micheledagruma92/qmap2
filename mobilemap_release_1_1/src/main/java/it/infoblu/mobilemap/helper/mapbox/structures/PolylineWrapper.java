package it.infoblu.mobilemap.helper.mapbox.structures;

import com.mapbox.mapboxsdk.annotations.Polyline;

public class PolylineWrapper {

    private Polyline polyline;

    private boolean visible;

    public PolylineWrapper(Polyline polyline) {
        this.polyline = polyline;
        this.visible = true;
    }

    public Polyline getPolyline() {
        return polyline;
    }

    public void setPolyline(Polyline polyline) {
        this.polyline = polyline;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

}