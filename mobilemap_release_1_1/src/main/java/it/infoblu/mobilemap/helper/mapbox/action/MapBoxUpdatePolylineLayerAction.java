package it.infoblu.mobilemap.helper.mapbox.action;

import android.text.TextUtils;

import com.google.android.gms.maps.model.LatLng;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.style.layers.LineLayer;
import com.mapbox.mapboxsdk.style.layers.PropertyFactory;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import it.infoblu.mobilemap.helper.interfaces.IMapAction;
import it.infoblu.mobilemap.helper.mapbox.constants.MapBoxConstants;
import it.infoblu.mobilemap.helper.mapbox.interfaces.IMapsMapBoxActionBinder;
import it.infoblu.mobilemap.helper.mapbox.structures.MarkerLayersInfoStructure;
import it.infoblu.mobilemap.helper.mapbox.utils.InternalMapBoxUtils;
import it.infoblu.mobilemap.helper.mapbox.utils.PolylineLayerUtils;

/**
 * Created on 09/07/18.
 */
public class MapBoxUpdatePolylineLayerAction implements IMapAction {

    private final IMapsMapBoxActionBinder mBinder;
    private final Bundle mBundle;


    public MapBoxUpdatePolylineLayerAction(IMapsMapBoxActionBinder binder, Bundle bundle) {
        mBinder = binder;
        mBundle = bundle;
    }

    @Override
    public Observable<IMapAction> exec() {
        return mBinder.getMapboxMapObservable()
                .map(mapboxMap -> new InternalBundle(mapboxMap, InternalMapBoxUtils.convertGMapLatLngListToFeatureCollection(mBundle.polylineList)))
                .observeOn(AndroidSchedulers.mainThread())
                .map(internalBundle -> {
                    addPolylineAsync(internalBundle);
                    return MapBoxUpdatePolylineLayerAction.this;
                });
    }


    private void addPolylineAsync(InternalBundle internalBundle) {
        String sourceId = PolylineLayerUtils.getSourceId(mBundle.tag);
        String layerId = PolylineLayerUtils.getSourceId(mBundle.tag);

        GeoJsonSource pathsSource = (GeoJsonSource) internalBundle.mapboxMap.getSource(sourceId);
        if (pathsSource == null) {
            pathsSource = new GeoJsonSource(sourceId, internalBundle.featureCollection);
            internalBundle.mapboxMap.addSource(pathsSource);
        } else {
            pathsSource.setGeoJson(internalBundle.featureCollection);
        }

        if (internalBundle.mapboxMap.getLayer(layerId) != null) {
            return;  // EXIT: layer already added
        }

        LineLayer lineLayerExternal = new LineLayer(layerId, sourceId)
                .withProperties(
                        PropertyFactory.lineWidth((float) mBundle.widthPixel)
                        , PropertyFactory.lineColor(mBundle.colorInt)
                );

        String pivotLayerName = getPivotLayerId(internalBundle.mapboxMap);
        if (TextUtils.isEmpty(pivotLayerName)) {
            internalBundle.mapboxMap.addLayer(lineLayerExternal);
        } else {
            internalBundle.mapboxMap.addLayerBelow(lineLayerExternal, pivotLayerName);
        }
    }

    private String getPivotLayerId(MapboxMap mapboxMap) {
        String pivot = mBundle.markerLayersInfo.getPoiGeometryBottomLayerId();
        if (pivot != null) {
            return pivot;
        }
        if (mapboxMap.getLayer(MapBoxConstants.ANNOTATION_LAYER_ID) != null) {
            return MapBoxConstants.ANNOTATION_LAYER_ID;
        }
        return null; // not found
    }


    /*
     * Structures
     */

    public static class Bundle {

        private final MarkerLayersInfoStructure markerLayersInfo;

        private final String tag;

        private final List<LatLng> polylineList;

        private final int widthPixel;

        private final int colorInt;

        public Bundle(MarkerLayersInfoStructure markerLayersInfo
                , String tag, int widthPixel
                , int colorInt, List<LatLng> polylineList) {
            this.markerLayersInfo = markerLayersInfo;
            this.tag = tag;
            this.polylineList = polylineList;
            this.widthPixel = widthPixel;
            this.colorInt = colorInt;
        }
    }

    private static class InternalBundle {

        private MapboxMap mapboxMap;

        private FeatureCollection featureCollection;

        InternalBundle(MapboxMap mapboxMap, FeatureCollection featureCollection) {
            this.mapboxMap = mapboxMap;
            this.featureCollection = featureCollection;
        }
    }

}
