package it.infoblu.mobilemap.helper.mapbox.action;

import com.mapbox.mapboxsdk.style.layers.Layer;
import com.mapbox.mapboxsdk.style.layers.Property;
import com.mapbox.mapboxsdk.style.layers.PropertyFactory;
import com.mapbox.mapboxsdk.style.layers.PropertyValue;

import org.apache.commons.lang3.StringUtils;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import it.infoblu.mobilemap.constants.FilterConstants;
import it.infoblu.mobilemap.helper.interfaces.IMapAction;
import it.infoblu.mobilemap.helper.interfaces.IMapsHelper;
import it.infoblu.mobilemap.helper.mapbox.interfaces.IMapsMapBoxActionBinder;
import it.infoblu.mobilemap.helper.mapbox.manager.FilterStateManager;
import it.infoblu.mobilemap.helper.mapbox.structures.AsyncBundle;
import it.infoblu.mobilemap.helper.mapbox.utils.MarkerLayerUtils;
import it.infoblu.mobilemap.model.data.FilterStateInfo;
import it.infoblu.mobilemap.model.data.RemoteConfig;

/**
 * Created on 15/02/18.
 */

public class MapBoxFilterChangeAction extends AMapBoxAction {

    private final Bundle mBundle;


    public MapBoxFilterChangeAction(IMapsMapBoxActionBinder binder, Bundle bundle) {
        super(binder);
        mBundle = bundle;
    }

    @Override
    public Observable<IMapAction> exec() {
        return getBinder().getAsyncBundleObservable()
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .map(new Function<AsyncBundle, IMapAction>() {
                    @Override
                    public IMapAction apply(AsyncBundle asyncBundle) throws Exception {
                        changeFilter(asyncBundle, mBundle.filterStateInfo);
                        return MapBoxFilterChangeAction.this;
                    }
                });
    }

    private void changeFilter(AsyncBundle asyncBundle, FilterStateInfo filterStateInfo) {
        RemoteConfig.MapFilter selectedFilter = null;
        for (RemoteConfig.MapFilter mapFilter : asyncBundle.filterList) {
            if (StringUtils.equals(mapFilter.getId(), filterStateInfo.getId())) {
                selectedFilter = mapFilter;
                break; // SKIP
            }
        }
        if (selectedFilter == null || selectedFilter.isEnabled() == filterStateInfo.isEnabled()) {
            return; // EXIT
        }
        selectedFilter.setEnabled(filterStateInfo.isEnabled());
        mBundle.filterStateManager.setFilterEnabled(selectedFilter);

        if (selectedFilter.getPoiTypeList() == null) {
            return; // EXIT
        }

        PropertyValue<String> propertyFactory = PropertyFactory.visibility(selectedFilter.isEnabled() ? Property.VISIBLE : Property.NONE);

        for (String poiType : selectedFilter.getPoiTypeList()) {
            Layer markerLayer = asyncBundle.mapboxMap.getLayer(MarkerLayerUtils.getLayerId(poiType));
            if (markerLayer != null) {
                markerLayer.setProperties(propertyFactory);
            }
            Layer internalGeometryLayer = asyncBundle.mapboxMap.getLayer(MarkerLayerUtils.getInternalGeometryLayerId(poiType));
            if (internalGeometryLayer != null) {
                internalGeometryLayer.setProperties(propertyFactory);
            }
            Layer internalGeometryArrowLayer = asyncBundle.mapboxMap.getLayer(MarkerLayerUtils.getInternalGeometryArrowLayerId(poiType));
            if (internalGeometryArrowLayer != null) {
                internalGeometryArrowLayer.setProperties(propertyFactory);
            }
            Layer clusterLayer = asyncBundle.mapboxMap.getLayer(MarkerLayerUtils.getClusterMarkerImageLayerId(poiType));
            if (clusterLayer != null) {
                clusterLayer.setProperties(propertyFactory);
            }
        }

        if (selectedFilter.getLayerList() != null) {
            for (String layer : selectedFilter.getLayerList()) {
                if (FilterConstants.FILTER_TRAFFIC_LAYER_ID.equals(layer)) {
                    mBundle.mapsHelper.showTraffic(selectedFilter.isEnabled());
                }
                // NB: no other layer filters managed
            }
        }
    }


    @Override
    public String toString() {
        return "Action -> filter change: " + mBundle.filterStateInfo.toString();
    }


    /*
     * Structures
     */

    public static class Bundle {

        private final IMapsHelper mapsHelper;

        private final FilterStateManager filterStateManager;

        private final FilterStateInfo filterStateInfo;

        public Bundle(IMapsHelper mapsHelper
                , FilterStateManager filterStateManager
                , FilterStateInfo filterStateInfo) {
            this.mapsHelper = mapsHelper;
            this.filterStateManager = filterStateManager;
            this.filterStateInfo = filterStateInfo;
        }
    }

}
