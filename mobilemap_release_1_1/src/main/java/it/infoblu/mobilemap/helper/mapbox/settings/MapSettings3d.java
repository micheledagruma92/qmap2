package it.infoblu.mobilemap.helper.mapbox.settings;

import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;

import it.infoblu.mobilemap.helper.interfaces.IMapsHelper;
import it.infoblu.mobilemap.helper.interfaces.IMapsSettingsConfiguration;
import it.infoblu.mobilemap.helper.mapbox.MapsMapBoxHelper;
import it.infoblu.mobilemap.helper.mapbox.interfaces.IMapSettingsBinder;


/**
 * Created on 23/02/18.
 */

public class MapSettings3d implements IMapsSettingsConfiguration {

    private final IMapSettingsBinder mBinder;
    private final MapView mMapView;
    private final int mTilt;
    private final int mAnimDuration;


    public MapSettings3d(IMapSettingsBinder binder, MapView mapView, int tilt, int animDuration) {
        mBinder = binder;
        mMapView = mapView;
        mTilt = tilt;
        mAnimDuration = animDuration;
    }


    @Override
    public void apply() {
        mBinder.getFollowStateSubject().onNext(IMapsHelper.FOLLOW_STATE.FREE);
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
                mapboxMap.getUiSettings().setTiltGesturesEnabled(false);
                CameraPosition position = new CameraPosition.Builder()
                        .tilt(mTilt)
                        .build();
                mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(position), mAnimDuration);
            }
        });
    }

    @Override
    public void revert() {
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
                mapboxMap.getUiSettings().setTiltGesturesEnabled(true);
            }
        });
    }

    @Override
    public boolean manageFollowState(MapsMapBoxHelper.LocationLayerBundle locationLayerBundle, int state) {
        return false;
    }

}
