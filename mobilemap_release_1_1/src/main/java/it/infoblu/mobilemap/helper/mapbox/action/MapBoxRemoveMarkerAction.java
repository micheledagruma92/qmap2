package it.infoblu.mobilemap.helper.mapbox.action;

import com.mapbox.mapboxsdk.annotations.Marker;

import io.reactivex.Observable;
import io.reactivex.functions.Function;
import it.infoblu.mobilemap.helper.interfaces.IMapAction;
import it.infoblu.mobilemap.helper.mapbox.interfaces.IMapsMapBoxActionBinder;
import it.infoblu.mobilemap.helper.mapbox.structures.AsyncBundle;
import it.infoblu.mobilemap.helper.mapbox.structures.MarkerWrapper;
import it.infoblu.mobilemap.helper.mapbox.structures.MarkersInfoStructure;
import it.infoblu.mobilemap.model.data.Poi;

/**
 * Created on 15/02/18.
 */

public class MapBoxRemoveMarkerAction extends AMapBoxAction {

    private final Bundle mBundle;


    public MapBoxRemoveMarkerAction(IMapsMapBoxActionBinder binder, Bundle bundle) {
        super(binder);
        mBundle = bundle;
    }

    @Override
    public Observable<IMapAction> exec() {
        return getBinder().getAsyncBundleObservable()
                .map(new Function<AsyncBundle, IMapAction>() {
                    @Override
                    public IMapAction apply(AsyncBundle asyncBundle) throws Exception {
                        removeMarkerSync(asyncBundle, mBundle.poi);
                        return MapBoxRemoveMarkerAction.this;
                    }
                });
    }

    public Marker removeMarkerSync(AsyncBundle asyncBundle, Poi poi) {
        MarkerWrapper markerWrapper = mBundle.markersInfoStructure.removePoiMarkerRelation(poi);
        if (markerWrapper == null || markerWrapper.getMarker() == null) {
            return null; // EXIT: marker not found
        }
        Marker marker = markerWrapper.getMarker();
        asyncBundle.mapboxMap.removeMarker(marker);
        return marker;
    }


    /*
     * Structures
     */

    public static class Bundle {

        private final MarkersInfoStructure markersInfoStructure;

        private final Poi poi;

        public Bundle(MarkersInfoStructure markersInfoStructure, Poi poi) {
            this.markersInfoStructure = markersInfoStructure;
            this.poi = poi;
        }
    }


}
