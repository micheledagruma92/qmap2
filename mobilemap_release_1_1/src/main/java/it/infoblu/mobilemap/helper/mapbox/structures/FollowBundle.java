package it.infoblu.mobilemap.helper.mapbox.structures;

import com.mapbox.mapboxsdk.maps.MapboxMap;

/**
 * Created on 22/06/18.
 */
public class FollowBundle {

    private MapboxMap mapboxMap;

    private int followState;

    public FollowBundle(MapboxMap mapboxMap, int followState) {
        this.mapboxMap = mapboxMap;
        this.followState = followState;
    }

    public MapboxMap getMapboxMap() {
        return mapboxMap;
    }

    public int getFollowState() {
        return followState;
    }

    @Override
    public String toString() {
        return "FollowBundle{" +
                "mapboxMap=" + mapboxMap +
                ", followState=" + followState +
                '}';
    }

}
