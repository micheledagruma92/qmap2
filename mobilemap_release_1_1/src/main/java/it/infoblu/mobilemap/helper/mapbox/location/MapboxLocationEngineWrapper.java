package it.infoblu.mobilemap.helper.mapbox.location;

import android.annotation.SuppressLint;
import android.location.Location;

import com.mapbox.android.core.location.LocationEngine;
import com.mapbox.android.core.location.LocationEngineListener;

import it.infoblu.mobilemap.helper.location.IMapHelperLocationEngine;
import it.infoblu.mobilemap.helper.location.IMapHelperLocationEngineListener;

/**
 * Created on 28/06/18.
 */
public class MapboxLocationEngineWrapper extends LocationEngine {

    private final IMapHelperLocationEngine mEngine;

    private IMapHelperLocationEngineListener listener = location -> {
        for (LocationEngineListener locationListener : locationListeners) {
            locationListener.onLocationChanged(location);
        }
    };

    private MapboxLocationEngineWrapper(IMapHelperLocationEngine engine) {
        mEngine = engine;
    }

    public static MapboxLocationEngineWrapper from(IMapHelperLocationEngine engine) {
        return new MapboxLocationEngineWrapper(engine);
    }


    @Override
    public void activate() {
        mEngine.activate();
    }

    @Override
    public void deactivate() {
        mEngine.deactivate();
    }

    @Override
    public boolean isConnected() {
        for (LocationEngineListener locationListener : locationListeners) {
            locationListener.onConnected();
        }
        return mEngine.isConnected();
    }

    @Override
    @SuppressLint("MissingPermission")
    public Location getLastLocation() {
        return mEngine.getLastLocation();
    }

    @Override
    public void requestLocationUpdates() {
        mEngine.addLocationUpdateListener(listener);
        mEngine.start();
    }

    @Override
    public void removeLocationUpdates() {
        mEngine.removeLocationUpdateListener(listener);
        mEngine.stop();
    }

    @Override
    public Type obtainType() {
        return Type.ANDROID;
    }

}
