package it.infoblu.mobilemap.helper.mapbox;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.PointF;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleObserver;
import androidx.lifecycle.OnLifecycleEvent;

import com.google.android.gms.maps.model.LatLngBounds;
import com.mapbox.android.core.location.LocationEngine;
import com.mapbox.android.core.location.LocationEngineListener;
import com.mapbox.android.core.location.LocationEnginePriority;
import com.mapbox.android.core.location.LocationEngineProvider;
import com.mapbox.android.gestures.AndroidGesturesManager;
import com.mapbox.android.gestures.MoveGestureDetector;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdate;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.plugins.locationlayer.LocationLayerPlugin;
import com.mapbox.mapboxsdk.plugins.locationlayer.modes.CameraMode;
import com.mapbox.mapboxsdk.plugins.locationlayer.modes.RenderMode;
import com.mapbox.mapboxsdk.style.layers.Layer;
import com.mapbox.mapboxsdk.style.layers.PropertyValue;
import com.tbruyelle.rxpermissions2.RxPermissions;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;
import it.infoblu.mobilemap.constants.MobileMapConstants;
import it.infoblu.mobilemap.constants.MobileMapSettingsConfig;
import it.infoblu.mobilemap.helper.config.MapHelperConfig;
import it.infoblu.mobilemap.helper.config.poiattribute.PoiAttributeInfo;
import it.infoblu.mobilemap.helper.interfaces.IMapAction;
import it.infoblu.mobilemap.helper.interfaces.IMapHelperHook;
import it.infoblu.mobilemap.helper.interfaces.IMapsHelper;
import it.infoblu.mobilemap.helper.interfaces.IMapsSettingsConfiguration;
import it.infoblu.mobilemap.helper.listeners.IMapHelperPermissionListener;
import it.infoblu.mobilemap.helper.listeners.OnMapClickListener;
import it.infoblu.mobilemap.helper.listeners.OnPoiClickListener;
import it.infoblu.mobilemap.helper.location.IMapHelperLocationEngine;
import it.infoblu.mobilemap.helper.mapbox.action.MapBoxAddMarkerAction;
import it.infoblu.mobilemap.helper.mapbox.action.MapBoxAddMarkersAction;
import it.infoblu.mobilemap.helper.mapbox.action.MapBoxAddPolylineAction;
import it.infoblu.mobilemap.helper.mapbox.action.MapBoxErrorAction;
import it.infoblu.mobilemap.helper.mapbox.action.MapBoxFilterChangeAction;
import it.infoblu.mobilemap.helper.mapbox.action.MapBoxRemoveMarkerAction;
import it.infoblu.mobilemap.helper.mapbox.action.MapBoxRemovePolylineAction;
import it.infoblu.mobilemap.helper.mapbox.action.MapBoxRemovePolylineLayerAction;
import it.infoblu.mobilemap.helper.mapbox.action.MapBoxUpdateMarkersLayerAction;
import it.infoblu.mobilemap.helper.mapbox.action.MapBoxUpdatePolylineLayerAction;
import it.infoblu.mobilemap.helper.mapbox.interfaces.IMapSettingsBinder;
import it.infoblu.mobilemap.helper.mapbox.interfaces.IMapsMapBoxActionBinder;
import it.infoblu.mobilemap.helper.mapbox.location.MapboxLocationEngineWrapper;
import it.infoblu.mobilemap.helper.mapbox.manager.ClusterManager;
import it.infoblu.mobilemap.helper.mapbox.manager.FilterStateManager;
import it.infoblu.mobilemap.helper.mapbox.settings.MapSettings2d;
import it.infoblu.mobilemap.helper.mapbox.settings.MapSettings3d;
import it.infoblu.mobilemap.helper.mapbox.settings.MapSettingsNav;
import it.infoblu.mobilemap.helper.mapbox.structures.AsyncBundle;
import it.infoblu.mobilemap.helper.mapbox.structures.MapState;
import it.infoblu.mobilemap.helper.mapbox.structures.MarkerLayersInfoStructure;
import it.infoblu.mobilemap.helper.mapbox.structures.MarkersInfoStructure;
import it.infoblu.mobilemap.helper.mapbox.structures.PolylineInfoStructure;
import it.infoblu.mobilemap.helper.mapbox.utils.InternalMapBoxUtils;
import it.infoblu.mobilemap.manager.IMarkerImageLoader;
import it.infoblu.mobilemap.manager.MarkerImageLoader;
import it.infoblu.mobilemap.model.constants.PoiProperty;
import it.infoblu.mobilemap.model.data.FilterStateInfo;
import it.infoblu.mobilemap.model.data.MMPolyline;
import it.infoblu.mobilemap.model.data.Poi;
import it.infoblu.mobilemap.model.data.RemoteConfig;
import it.infoblu.mobilemap.model.repository.SessionRepository;
import it.infoblu.mobilemap.utils.AndroidUtils;
import it.infoblu.mobilemap.utils.InternalLogger;

/**
 * Created on 13/02/18.
 */

public class MapsMapBoxHelper implements IMapsHelper
        , IMapsMapBoxActionBinder
        , IMapSettingsBinder {

    private static final String KEY_PERSISTENCE_CURRENT_MAP_STYLE = "persistence_current_map_style";
    private static final String KEY_PERSISTENCE_FILTER_STATE = "persistence_filter_state";
    private static final String KEY_PERSISTENCE_MAP_STATE = "persistence_map_state";

    private static int DEFAULT_ESTIMATED_POI_MAX_NUMBER = 6000;
    private static int DEFAULT_ESTIMATED_POLYLINE_MAX_NUMBER = 50;

    private static final String TRAFFIC_SOURCE_ID = "traffic";
    private static final String TRAFFIC_LAYER_PIVOT = "traffic-layer";

    private final CompositeDisposable mDisposables;
    private final CompositeDisposable mMapActionDisposables;
    private Disposable mFollowStateDisposable;
    private Disposable mModifyMapStyleDisposable;

    private final MapHelperConfig mInternalConfiguration;

    private final Lifecycle mLifecycle;
    private BehaviorSubject<Boolean> mForegroundObservable = BehaviorSubject.createDefault(true);
    private final MapView mMapView;
    private final Observable<RemoteConfig> mConfigObservable;
    private final BehaviorSubject<String> mCurrentMapStyleObservable = BehaviorSubject.create();
    private final RxPermissions mPermissionManager;
    private final BehaviorSubject<Double> mCameraIdleSubject = BehaviorSubject.createDefault(0.0);
    private final PublishSubject<IMapAction> mMarkerActionSubject = PublishSubject.create();
    private final PublishSubject<IMapAction> mPolylineActionSubject = PublishSubject.create();
    private final BehaviorSubject<Location> mLocationSubject = BehaviorSubject.create();
    private final BehaviorSubject<Integer> mFollowSubject = BehaviorSubject.createDefault(FOLLOW_STATE.FREE);
    private final IMarkerImageLoader mMarkerImageLoaderManager;
    private final ClusterManager mClusterManager;
    private final FilterStateManager mFilterStateManager;

    private final IMapHelperLocationEngine mMapHelperLocationEngine;

    private final BehaviorSubject<LocationLayerPlugin> mLocationLayerSubject = BehaviorSubject.create();
    public LocationEngine mLocationEngine;

    private final LinkedHashMap<String, IMapsSettingsConfiguration> mSettingsConfigMap = new LinkedHashMap<>();

    private final MarkersInfoStructure mMarkersInfoStructure;
    private final MarkerLayersInfoStructure mMarkerLayersInfoStructure;
    private final PolylineInfoStructure mPolylineInfoStructure;

    private IMapsSettingsConfiguration mCurrentSettingsConfig;
    private final List<IMapHelperHook> mHookList = new ArrayList<>();
    private final ArrayList<OnPoiClickListener> mPoiClickListenerList = new ArrayList<>();
    private final ArrayList<OnMapClickListener> mMapClickListenerList = new ArrayList<>();
    private final ArrayList<IMapHelperPermissionListener> mPermissionListenerList = new ArrayList<>();

    private MapState mMapState = new MapState();


    public MapsMapBoxHelper(@NonNull Lifecycle lifecycle, @NonNull MapView mapView) {
        this(lifecycle, mapView, null, null, null, SessionRepository.getInstance().getRemoteConfigObservable());
    }

    public MapsMapBoxHelper(@NonNull Lifecycle lifecycle, @NonNull MapView mapView, MapHelperConfig config) {
        this(lifecycle, mapView, config, null, null, SessionRepository.getInstance().getRemoteConfigObservable());
    }

    public MapsMapBoxHelper(@NonNull Lifecycle lifecycle, @NonNull MapView mapView, MapHelperConfig config
            , IMapHelperLocationEngine mapHelperLocationEngine) {
        this(lifecycle, mapView, config, mapHelperLocationEngine, null, SessionRepository.getInstance().getRemoteConfigObservable());
    }

    public MapsMapBoxHelper(@NonNull Lifecycle lifecycle, @NonNull MapView mapView, MapHelperConfig config
            , IMapHelperLocationEngine mapHelperLocationEngine
            , List<FilterStateInfo> initFilterInfo) {
        this(lifecycle, mapView, config, mapHelperLocationEngine, initFilterInfo, SessionRepository.getInstance().getRemoteConfigObservable());
    }

    protected MapsMapBoxHelper(@NonNull Lifecycle lifecycle, @NonNull MapView mapView, MapHelperConfig config
            , IMapHelperLocationEngine mapHelperLocationEngine
            , List<FilterStateInfo> filterInfoList
            , @NonNull Observable<RemoteConfig> remoteConfigObservable) {
        mDisposables = new CompositeDisposable();
        mMapActionDisposables = new CompositeDisposable();
        mLifecycle = lifecycle;
        mMapView = mapView;
        mInternalConfiguration = config != null ? config : new MapHelperConfig.Builder().build();
        mConfigObservable = remoteConfigObservable
                .map(remoteConfig -> decorateRemoteConfig(remoteConfig, mInternalConfiguration))
                .replay(1).autoConnect();
        mPermissionManager = new RxPermissions(AndroidUtils.extractActivity(mMapView));
        mMarkersInfoStructure = new MarkersInfoStructure(DEFAULT_ESTIMATED_POI_MAX_NUMBER);
        mMarkerLayersInfoStructure = new MarkerLayersInfoStructure();
        mPolylineInfoStructure = new PolylineInfoStructure(DEFAULT_ESTIMATED_POLYLINE_MAX_NUMBER);
        mMarkerImageLoaderManager = new MarkerImageLoader(mapView.getContext());
        mClusterManager = new ClusterManager(mInternalConfiguration.getClusterSettings());
        mFilterStateManager = new FilterStateManager(filterInfoList);
        mMapHelperLocationEngine = mapHelperLocationEngine;
        mFollowSubject.distinct();
        initSettingsConfig();
        initObservables();
        initGestureRecognizer();
        initForegroundObservable(lifecycle);
        requestLocationEngine(); // TODO: 15/03/18 dovrebbe essere facoltativo?
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        restoreData(savedInstanceState);
        initMap(savedInstanceState);
    }

    private void restoreData(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            return; // EXIT
        }
        if (savedInstanceState.containsKey(KEY_PERSISTENCE_FILTER_STATE)) {
            mFilterStateManager.refresh(savedInstanceState.<FilterStateInfo>getParcelableArrayList(KEY_PERSISTENCE_FILTER_STATE));
        }
        if (savedInstanceState.containsKey(KEY_PERSISTENCE_MAP_STATE)) {
            mMapState = savedInstanceState.getParcelable(KEY_PERSISTENCE_MAP_STATE);
        }
        mDisposables.add(getMapboxMapObservable()
                .subscribe(mapboxMap -> {
                    CameraPosition.Builder cameraPositionBuilder = new CameraPosition.Builder();
                    if (mMapState.mapBound != null) {
                        cameraPositionBuilder.target(InternalMapBoxUtils.convertGMapLatLngToMapboxLatLng(mMapState.mapBound.getCenter()));
                    }
                    if (mMapState.zoom > 0) {
                        cameraPositionBuilder.zoom(mMapState.zoom);
                    }
                    CameraPosition cameraPosition = cameraPositionBuilder.build();
                    mapboxMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                }));
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString(KEY_PERSISTENCE_CURRENT_MAP_STYLE, mCurrentMapStyleObservable.getValue());
        outState.putParcelableArrayList(KEY_PERSISTENCE_FILTER_STATE, mFilterStateManager.getFilterStateList());
        outState.putParcelable(KEY_PERSISTENCE_MAP_STATE, mMapState);
    }

    @Override
    public void onClear() {
        if (mDisposables != null) {
            mDisposables.dispose();
        }
        if (mDisposables != null) {
            mMapActionDisposables.dispose();
        }
        // clear hooks
        synchronized (mHookList) {
            for (IMapHelperHook hook : mHookList) {
                hook.unbind();
            }
            mHookList.clear();
        }
        if (mLocationEngine != null) {
            mLocationEngine.deactivate();
        }
        if (getLocationLayerPlugin() != null) {
            getLocationLayerPlugin().setLocationLayerEnabled(false);
        }
    }

    @Override
    public void addHook(IMapHelperHook hook) {
        mHookList.add(hook);
        hook.bind(this, SessionRepository.getInstance());
    }

    @Override
    public void setMapStyle(final String styleLabel) {
        setMapStyle(styleLabel, false);
    }

    private void setMapStyle(final String styleId, boolean isFirst) {
        if (mModifyMapStyleDisposable != null) {
            mModifyMapStyleDisposable.dispose();
        }

        Single<AsyncBundle> customAsyncBundleSingle = (isFirst
                // TODO: 16/04/18 ATTENZIONE: GESTIRE FILTRO DEL TRAFFICO? nel caso va refattorizzato e centralizzato in getAsyncBundleObservable()
                ? mConfigObservable.map(config -> new AsyncBundle(config, new ArrayList<>(), null))
                : getAsyncBundleObservable())
                .take(1).singleOrError();

        mModifyMapStyleDisposable = Single.zip(customAsyncBundleSingle
                , mForegroundObservable.filter(isForeground -> isForeground).take(1).singleOrError()
                , (asyncBundle, isForeground) -> asyncBundle)
                .filter(asyncBundle ->
                        !StringUtils.equals(styleId, mCurrentMapStyleObservable.getValue())
                                && asyncBundle.config.getMapStyleList() != null)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(asyncBundle -> {
                    String mapStyleEndpoint = asyncBundle.config.getMapStyleEndpoint(styleId);
                    if (TextUtils.isEmpty(mapStyleEndpoint)) {
                        InternalLogger.e("%s style not found", styleId);
                        return; // EXIT
                    }
                    mMarkerLayersInfoStructure.clear(); // TODO: 12/04/18: potrebbe permanere un problema di accessi simultanei
                    MapboxMap.OnStyleLoadedListener listener = style ->
                            mCurrentMapStyleObservable.onNext(styleId);
                    if (asyncBundle.mapboxMap == null) { // == isFirst
                        mMapView.setStyleUrl(mapStyleEndpoint);
                        listener.onStyleLoaded(styleId);
                    } else {
                        asyncBundle.mapboxMap.setStyleUrl(mapStyleEndpoint, listener);
                    }
                });

        mDisposables.add(mModifyMapStyleDisposable);
    }

    public Observable<String> getCurrentMapStyleObservable() {
        return mCurrentMapStyleObservable;
    }

    @Override
    public void setMarkerIconPlaceholders(Map<String, Integer> markerIconPlaceholderMap) {
        mMarkerImageLoaderManager.setMarkerIconPlaceholders(markerIconPlaceholderMap);
        // TODO: 26/03/18 probabilmente vanno aggiornate le immagini dopo questa modifica
    }

    @Override
    public void addOnPoiClickListener(OnPoiClickListener listener) {
        mPoiClickListenerList.add(listener);
    }

    @Override
    public void removeOnPoiClickListener(OnPoiClickListener listener) {
        mPoiClickListenerList.remove(listener);
    }

    /*
     * This listener doesn't trigger if user click on a symbolic layer element
     */
    @Override
    public void addOnMapClickListener(OnMapClickListener listener) {
        mMapClickListenerList.add(listener);
    }

    @Override
    public void removeOnMapClickListener(OnMapClickListener listener) {
        mMapClickListenerList.remove(listener);
    }

    @Override
    public void addOnPermissionListener(IMapHelperPermissionListener listener) {
        mPermissionListenerList.add(listener);
    }

    @Override
    public void removeOnPermissionListener(IMapHelperPermissionListener listener) {
        mPermissionListenerList.remove(listener);
    }

    @Override
    public void removeHook(IMapHelperHook hook) {
        boolean isRemoved = mHookList.remove(hook);
        if (isRemoved) {
            hook.unbind();
        }
    }

    @Override
    public void addMarker(final Poi poi) {
        // TODO: 19/04/18 va gestito sotto struttura interna per mantenerli anche al refresh
        mMarkerActionSubject.onNext(new MapBoxAddMarkerAction(this
                , new MapBoxAddMarkerAction.Bundle(mMapView.getContext()
                , mMarkersInfoStructure, mMarkerImageLoaderManager, poi)));
    }

    @Override
    public void addMarkers(final List<Poi> poiList) {
        // TODO: 19/04/18 va gestito sotto struttura interna per mantenerli anche al refresh
        mMarkerActionSubject.onNext(new MapBoxAddMarkersAction(this
                , new MapBoxAddMarkersAction.Bundle(mMapView.getContext()
                , mMarkersInfoStructure, mMarkerImageLoaderManager, poiList)));
    }

    @Override
    public void removeMarker(final Poi poi) {
        // TODO: 19/04/18 va gestito sotto struttura interna per mantenerli anche al refresh
        mMarkerActionSubject.onNext(new MapBoxRemoveMarkerAction(this
                , new MapBoxRemoveMarkerAction.Bundle(mMarkersInfoStructure, poi)));
    }

    @Override
    public void removeMarkers(final List<Poi> poiList) {
        // TODO: 19/04/18 va gestito sotto struttura interna per mantenerli anche al refresh
        synchronized (poiList) {
            for (Poi poi : poiList) {
                removeMarker(poi);
            }
        }
    }

    @Override
    public void updateMarkersLayer(String poiType, List<Poi> poiList) {
        mMarkerActionSubject.onNext(new MapBoxUpdateMarkersLayerAction(this
                , new MapBoxUpdateMarkersLayerAction.Bundle(mMapView.getContext()
                , mMarkerLayersInfoStructure, mMarkerImageLoaderManager, mClusterManager
                , mInternalConfiguration.getPoiGeometrySettings().getPoiGeometryInfo(poiType)
                , poiType, poiList)));
    }

    @Override
    public void setFilterEnabled(String filterId, boolean enabled) {
        mMarkerActionSubject.onNext(new MapBoxFilterChangeAction(this
                , new MapBoxFilterChangeAction.Bundle(this, mFilterStateManager
                , new FilterStateInfo(filterId, enabled))));
    }

    @Override
    public Single<List<RemoteConfig.MapFilter>> getFilterListSingle() {
        return mConfigObservable
                .map(config -> mFilterStateManager.getMapFilters(config))
                .take(1)
                .single(new ArrayList<>());
    }

    @Override
    public MMPolyline addPolyline(String id, String geoJson, MMPolyline.Options options) {
        FeatureCollection featureCollection = FeatureCollection.fromJson(geoJson);
        MMPolyline polyline = new MMPolyline()
                .addAll(InternalMapBoxUtils.convertFeatureToGMapLatLng(featureCollection.features()))
                .options(options)
                .id(id);
        addPolyline(polyline);
        return polyline;
    }

    @Override
    public void updatePolylineLayer(String tag, int width, int colorInt, List<com.google.android.gms.maps.model.LatLng> polylineList) {
        mPolylineActionSubject.onNext(new MapBoxUpdatePolylineLayerAction(this
                , new MapBoxUpdatePolylineLayerAction.Bundle(mMarkerLayersInfoStructure
                , tag, width, colorInt, polylineList)))
        ;
    }

    @Override
    public void removePolylineLayer(String tag) {
        mPolylineActionSubject.onNext(new MapBoxRemovePolylineLayerAction(this
                , new MapBoxRemovePolylineLayerAction.Bundle(tag)));
    }

    @Override
    public void addPolyline(MMPolyline polyline) {
        mMarkerActionSubject.onNext(new MapBoxAddPolylineAction(this
                , new MapBoxAddPolylineAction.Bundle(polyline, mPolylineInfoStructure)));
    }

    @Override
    public void removePolyline(MMPolyline polyline) {
        mMarkerActionSubject.onNext(new MapBoxRemovePolylineAction(this
                , new MapBoxRemovePolylineAction.Bundle(polyline, mPolylineInfoStructure)));
    }

    @Override
    public void addMapSettingsConfig(String id, IMapsSettingsConfiguration mapsSettingsConfiguration) {
        if (TextUtils.isEmpty(id)) {
            throw new RuntimeException("Passed map setting id is null or empty!");
        }
        if (mapsSettingsConfiguration == null) {
            throw new RuntimeException("Passed map setting is null!");
        }
        mSettingsConfigMap.put(id, mapsSettingsConfiguration);
    }

    @Override
    public void selectMapSettingsConfig(String id) {
        deselectCurrentMapSettingsConfig();
        mCurrentSettingsConfig = mSettingsConfigMap.get(id);
        if (mCurrentSettingsConfig != null) {
            mCurrentSettingsConfig.apply();
        }
    }

    @Override
    public void deselectCurrentMapSettingsConfig() {
        if (mCurrentSettingsConfig != null) {
            mCurrentSettingsConfig.revert();
        }
    }

    @SuppressLint("MissingPermission")
    public void followPosition(boolean follow) {
        if (!follow) {
            mFollowSubject.onNext(FOLLOW_STATE.FREE);
            return; // EXIT
        }
        mDisposables.add(mPermissionManager.request(Manifest.permission.ACCESS_FINE_LOCATION)
                .map(createPermissionRequestFunction())
                .filter(accepted -> accepted)
                .flatMap((Function<Boolean, ObservableSource<MapboxMap>>) accepted -> getMapboxMapObservable())
                .subscribe(mapboxMap -> mFollowSubject.onNext(FOLLOW_STATE.FOLLOW)));
    }

    @Override
    public void centerPosition(final float zoom, final long duration) {
        mDisposables.add(getMapboxMapObservable()
                .subscribe(mapboxMap -> {
                    @SuppressLint("MissingPermission")
                    Location location = mLocationEngine.getLastLocation();
                    if (location != null) {
                        if (duration > 0) {
                            mapboxMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude()
                                    , location.getLongitude()), zoom), (int) duration);
                        } else {
                            mapboxMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude()
                                    , location.getLongitude()), zoom));
                        }
                    }
                }));
    }

    @Override
    public void centerPosition(final com.google.android.gms.maps.model.LatLng position, final float zoom, final long duration) {
        mDisposables.add(getMapboxMapObservable()
                .subscribe(mapboxMap -> {
                    if (duration > 0) {
                        mapboxMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(position.latitude
                                , position.longitude), zoom), (int) duration);
                    } else {
                        mapboxMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(position.latitude
                                , position.longitude), zoom));
                    }
                }));
    }

    @Override
    public void centerPosition(final List<com.google.android.gms.maps.model.LatLng> positionList, final int padding, final long duration) {
        if (positionList == null || positionList.size() < 2) {
            InternalLogger.d("Not enough items to perform a bound zoom.");
            return; // EXIT
        }
        LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
        for (com.google.android.gms.maps.model.LatLng point : positionList) {
            boundsBuilder.include(point);
        }
        centerPosition(boundsBuilder.build(), padding, duration);
    }

    @Override
    public void centerPosition(final LatLngBounds bounds, final int padding, final long duration) {
        mDisposables.add(getMapboxMapObservable()
                .subscribe(mapboxMap -> {
                    try {
                        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(
                                InternalMapBoxUtils.convertGMapLatLngBoundToMapboxLatLngBound(bounds), padding);
                        if (duration > 0) {
                            mapboxMap.animateCamera(cameraUpdate, (int) duration);
                        } else {
                            mapboxMap.moveCamera(cameraUpdate);
                        }
                    } catch (Exception e) {
                        InternalLogger.e(e);
                    }
                }));
    }

    @Override
    public Observable<Integer> getFollowStateObservable() {
        return mFollowSubject;
    }

    @Override
    public boolean isFollowPosition() {
        return getFollowStateSubject().getValue() == FOLLOW_STATE.FOLLOW;
    }

    @Override
    @SuppressLint("MissingPermission")
    public com.google.android.gms.maps.model.LatLng getLastPosition() {
        if (mLocationEngine == null) {
            return null; // EXIT
        }
        Location location = mLocationEngine.getLastLocation();
        return new com.google.android.gms.maps.model.LatLng(location.getLatitude(), location.getLongitude());
    }

    @Override
    public Observable<Boolean> getTrafficVisibilityObservable() {
        return getMapboxMapObservable()
                .map(mapboxMap -> {
                    boolean hasTrafficLayer = false;
                    List<Layer> layers = mapboxMap.getLayers();
                    for (Layer layer : layers) {
                        if (layer.getId().contains(TRAFFIC_LAYER_PIVOT)) {
                            hasTrafficLayer = true;
                            if (layer.getVisibility().getValue() != null
                                    && layer.getVisibility().getValue().equalsIgnoreCase("visible")) {
                                return true;
                            }
                        }
                    }
                    if (!hasTrafficLayer) {
                        return false;
                    }
                    return false;
                });
    }

    @Override
    public void showTraffic(final boolean show) {
        mDisposables.add(getMapboxMapObservable()
                .subscribe(mapboxMap -> {
                    final String pivotString = TRAFFIC_LAYER_PIVOT;
                    List<Layer> layers = mapboxMap.getLayers();
                    for (Layer layer : layers) { // iterate all traffic layers
                        if (layer.getId().contains(pivotString)) {
                            layer.setProperties(new PropertyValue<>("visibility", show ? "visible" : "none"));
                        }
                    }
                }));
    }


    /*
     * Action binder methods
     */

    @Override
    public Observable<AsyncBundle> getAsyncBundleObservable() {
        return Observable.zip(getMapboxMapObservable()
                , mConfigObservable
                , (mapboxMap, config) ->
                        new AsyncBundle(config, mFilterStateManager.getMapFilters(config), mapboxMap));
    }

    @Override
    public Observable<MapboxMap> getMapboxMapObservable() {
        return Observable.create(emitter ->
                mMapView.getMapAsync(mapboxMap -> {
                    emitter.onNext(mapboxMap);
                    emitter.onComplete();
                }));
    }

    public LocationLayerPlugin getLocationLayerPlugin() {
        return mLocationLayerSubject.getValue();
    }


    /*
     * Settings binder methods
     */

    @Override
    public BehaviorSubject<Integer> getFollowStateSubject() {
        return mFollowSubject;
    }

    @Override
    public LocationEngine getLocationEngine() {
        return mLocationEngine;
    }

    @Override
    public Single<LocationLayerPlugin> getLocationLayerPluginSingle() {
        return mLocationLayerSubject.take(1).singleOrError();
    }

    /*
     * Internal methods
     */

    private void initSettingsConfig() {
        addMapSettingsConfig(MobileMapSettingsConfig.SETTING_2D, new MapSettings2d(this, mMapView, 2000)); // TODO: 23/02/18 parametri da prendere da config
        addMapSettingsConfig(MobileMapSettingsConfig.SETTING_3D, new MapSettings3d(this, mMapView, 60, 2000)); // TODO: 23/02/18 parametri da prendere da config
        addMapSettingsConfig(MobileMapSettingsConfig.SETTING_NAV, new MapSettingsNav(this, mLifecycle, mMapView, 60));
    }

    private void initObservables() {
        mDisposables.add(mLocationSubject
                .subscribe(location -> mMapState.lastPosition = InternalMapBoxUtils.convertMapboxLocationToGMapLatLng(location)));

        mMapActionDisposables.add(mMarkerActionSubject
                .concatMap((Function<IMapAction, ObservableSource<IMapAction>>) mapAction ->
                        mapAction.exec()
                                .onErrorResumeNext(throwable -> {
                                    // fallback
                                    return new MapBoxErrorAction(throwable, null).exec();
                                }))
                .subscribe(mapAction -> {
//                        Logger.d("Map action completed: %s", mapAction);
                }));

        mMapActionDisposables.add(mPolylineActionSubject
                .concatMap((Function<IMapAction, ObservableSource<IMapAction>>) mapAction ->
                        mapAction.exec()
                                .onErrorResumeNext(throwable -> {
                                    // fallback
                                    return new MapBoxErrorAction(throwable, null).exec();
                                }))
                .subscribe(mapAction -> {
//                        Logger.d("Map action completed: %s", mapAction);
                }));

        mDisposables.add(getMapboxMapObservable()
                .subscribe(mapboxMap -> mapboxMap.addOnCameraIdleListener(() -> {
                    try {
                        mMapState.mapBound = InternalMapBoxUtils.convertMapboxLatLngBoundToGMapLatLngBound(
                                mapboxMap.getProjection().getVisibleRegion().latLngBounds);
                    } catch (Exception e) {
                        // hard fix to avoid strange "1 argument" error
                    }
                    mCameraIdleSubject.onNext(mapboxMap.getCameraPosition().zoom);
                })));

        mDisposables.add(getCameraIdleObservable()
                .subscribe(zoom -> mMapState.zoom = zoom));

        mDisposables.add(getFollowStateObservable()
                .filter(integer -> getLocationLayerPlugin() != null)
                .subscribe(this::applyFollowState));
    }

    private void initGestureRecognizer() {
        final AndroidGesturesManager manager = new AndroidGesturesManager(mMapView.getContext());
        manager.setMoveGestureListener(new MoveGestureDetector.SimpleOnMoveGestureListener() {
            @Override
            public boolean onMoveBegin(MoveGestureDetector detector) {
                mFollowSubject.onNext(FOLLOW_STATE.FREE);
                return super.onMoveBegin(detector);
            }
        });
        mMapView.setOnTouchListener((view, motionEvent) -> {
            manager.onTouchEvent(motionEvent);
            return false;
        });
    }

    private void initForegroundObservable(final Lifecycle lifecycle) {
        lifecycle.addObserver(new LifecycleObserver() {
            @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
            void onResume() {
                mForegroundObservable.onNext(true);
            }

            @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
            void onPause() {
                mForegroundObservable.onNext(false);
            }
        });
    }

    private void initMap(final Bundle savedInstanceState) {
        mDisposables.add(mConfigObservable
                .observeOn(AndroidSchedulers.mainThread())
                .filter(config -> config.getMapStyleList() != null && !config.getMapStyleList().isEmpty())
                .subscribe(config -> {
                    String defMapStyle = ObjectUtils.firstNonNull(
                            savedInstanceState == null ? null
                                    : savedInstanceState.getString(KEY_PERSISTENCE_CURRENT_MAP_STYLE) // restore previous value
                            , !TextUtils.isEmpty(config.getMapStyleEndpoint(mInternalConfiguration.getMapStyle())) // safe check
                                    ? mInternalConfiguration.getMapStyle() : null
                            , config.getMapDefault()
                    );
                    setMapStyle(defMapStyle, true);
                }));

        mDisposables.add(getMapboxMapObservable()
                .subscribe(mapboxMap ->
                        mapboxMap.setOnMarkerClickListener(marker -> {
                            Poi poi = mMarkersInfoStructure.getPoi(marker);
                            if (poi == null) {
                                InternalLogger.d("no poi found for marker: %s", marker);
                                return false; // EXIT
                            }
                            performPoiClickEvent(poi);
                            return true;
                        })));

        mDisposables.add(getMapboxMapObservable()
                .subscribe(mapboxMap ->
                        mapboxMap.addOnMapClickListener(point -> {
                            PointF screenPoint = mapboxMap.getProjection().toScreenLocation(point);

                            boolean isCluster = mClusterManager.interceptClickEvent(mapboxMap, screenPoint);
                            if (isCluster) {
                                return; // EXIT
                            }

                            List<Feature> features = mapboxMap.queryRenderedFeatures(screenPoint, mMarkerLayersInfoStructure.getMarkerLayerIds());
                            if (!features.isEmpty()) {
                                Feature selectedFeature = features.get(0);
                                String poiId = selectedFeature.hasNonNullValueForProperty(PoiProperty.ID)
                                        ? selectedFeature.getStringProperty(PoiProperty.ID) : "";
                                String poiType = selectedFeature.hasNonNullValueForProperty(PoiProperty.SUBTYPE_ID)
                                        ? selectedFeature.getStringProperty(PoiProperty.SUBTYPE_ID) : "";
                                Poi poi = mMarkerLayersInfoStructure.getPoi(poiType, poiId);
                                if (poi == null) {
                                    InternalLogger.d("no poi found for id: %s and type %s", poiId, poiType);
                                    return; // EXIT
                                }
                                performPoiClickEvent(poi);
                                return; // EXIT
                            }

                            // if there aren't markers it will be managed as a map click event
                            performMapClickEvent(point);
                        })));
    }

    private void requestLocationEngine() {
        mDisposables.add(mPermissionManager.request(Manifest.permission.ACCESS_FINE_LOCATION)
                .map(createPermissionRequestFunction())
                .filter(accepted -> accepted)
                .flatMap((Function<Boolean, ObservableSource<MapboxMap>>) accepted -> getMapboxMapObservable())
                .subscribe(mapboxMap -> initLocationEngine(mapboxMap)));
    }

    @SuppressLint("MissingPermission")
    private void initLocationEngine(MapboxMap mapboxMap) {
        if (mLocationEngine != null) {
            return; // EXIT: prevent multiple initialization (and consequent crashes)
        }

        if (mMapHelperLocationEngine != null) {
            mLocationEngine = MapboxLocationEngineWrapper.from(mMapHelperLocationEngine);
        } else {
            mLocationEngine = createDefaultLocationEngine(mMapView.getContext());
        }

        mLocationEngine.activate();
        mLocationEngine.requestLocationUpdates();

        // location layer
        LocationLayerPlugin locationLayerPlugin = new LocationLayerPlugin(mMapView, mapboxMap, mLocationEngine);
        locationLayerPlugin.setLocationLayerEnabled(true);
        mLifecycle.addObserver(locationLayerPlugin); // TODO: 15/03/18 probabilmente deve essere rilasciato
        applyFollowState(mFollowSubject.getValue());
        locationLayerPlugin.setRenderMode(RenderMode.NORMAL);

        mLocationLayerSubject.onNext(locationLayerPlugin);

        getLocationEngine().addLocationEngineListener(new LocationEngineListener() {
            @Override
            public void onConnected() {
                // nothing to do here
            }

            @Override
            public void onLocationChanged(Location location) {
                if (location == null) return;
                mLocationSubject.onNext(location);
            }
        });
    }

    private Observable<Double> getCameraIdleObservable() {
        return mCameraIdleSubject;
    }

    private void performPoiClickEvent(Poi poi) {
        synchronized (mPoiClickListenerList) {
            for (OnPoiClickListener listener : mPoiClickListenerList) {
                listener.onPoiClick(poi);
            }
        }
    }

    private void performMapClickEvent(LatLng point) {
        com.google.android.gms.maps.model.LatLng gMapPoint = InternalMapBoxUtils.convertMapboxLatLngToGMapLatLng(point);
        synchronized (mMapClickListenerList) {
            for (OnMapClickListener listener : mMapClickListenerList) {
                listener.onMapClick(gMapPoint);
            }
        }
    }

    @SuppressLint("MissingPermission")
    private void applyFollowState(int state) {
        if (mFollowStateDisposable != null) mFollowStateDisposable.dispose();
        mFollowStateDisposable = Observable.combineLatest(
                getMapboxMapObservable()
                , getLocationLayerPluginSingle().toObservable()
                , (mobileMap, locationLayerPlugin) -> new LocationLayerBundle(mobileMap, locationLayerPlugin))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(locationLayerBundle -> {
                    if (mCurrentSettingsConfig != null) {
                        boolean managed = mCurrentSettingsConfig.manageFollowState(locationLayerBundle, state);
                        if (managed) return; // EXIT
                    }

                    // default behavior
                    switch (state) {
                        case FOLLOW_STATE.FOLLOW:
                            locationLayerBundle.locationLayerPlugin.setCameraMode(CameraMode.TRACKING);
                            if (mLocationEngine.getLastLocation() != null) { // there is a location to zoom in
                                locationLayerBundle.mapboxMap.easeCamera(CameraUpdateFactory.zoomTo(MobileMapConstants.NAV_INIT_ZOOM));
                            }
                            break;
                        case FOLLOW_STATE.FREE:
                            getLocationLayerPlugin().setCameraMode(CameraMode.NONE);
                            break;
                    }
                });
        mDisposables.add(mFollowStateDisposable);
    }

    @NonNull
    private Function<Boolean, Boolean> createPermissionRequestFunction() {
        return aBoolean -> {
            for (IMapHelperPermissionListener listener : mPermissionListenerList) {
                listener.onPermissionRequest(Manifest.permission.ACCESS_FINE_LOCATION, aBoolean);
            }
            return aBoolean;
        };
    }


    /*
     * Utility methods
     */

    private LocationEngine createDefaultLocationEngine(Context context) {
        LocationEngine engine = new LocationEngineProvider(context).obtainBestLocationEngineAvailable();
        engine.setPriority(LocationEnginePriority.HIGH_ACCURACY);
        engine.setFastestInterval(1000);
        engine.setInterval(1000);
        return engine;
    }

    private static RemoteConfig decorateRemoteConfig(RemoteConfig remoteConfig, MapHelperConfig mapHelperConfig) {
        if (mapHelperConfig.getPoiAttributeSettings() == null) {
            return remoteConfig;
        }
        RemoteConfig remoteConfigDecorated = remoteConfig.copy();
        for (RemoteConfig.PlaceService placeService : remoteConfigDecorated.getPlaces()) {
            PoiAttributeInfo poiAttribute = mapHelperConfig.getPoiAttributeSettings().getPoiAttributeInfo(placeService.getPoiType());
            if (poiAttribute == null) {
                continue; // SKIP
            }
            if (poiAttribute.getZoomMin() > 0) {
                placeService.setZoomMin(poiAttribute.getZoomMin());
            }
            if (poiAttribute.getZoomMax() > 0) {
                placeService.setZoomMax(poiAttribute.getZoomMax());
            }
            if (poiAttribute.getPoiGeometryZoomMin() > 0) {
                placeService.setPoiGeometryZoomMin(poiAttribute.getPoiGeometryZoomMin());
            }
            if (!TextUtils.isEmpty(poiAttribute.getIconAnchor())) {
                placeService.setIconAnchor(poiAttribute.getIconAnchor());
            }
        }
        return remoteConfigDecorated;
    }


    /*
     * Internal structures
     */

    public static class LocationLayerBundle {

        public final MapboxMap mapboxMap;
        public final LocationLayerPlugin locationLayerPlugin;

        public LocationLayerBundle(MapboxMap mapboxMap, LocationLayerPlugin locationLayerPlugin) {
            this.mapboxMap = mapboxMap;
            this.locationLayerPlugin = locationLayerPlugin;
        }
    }

}
