package it.infoblu.mobilemap.helper.settings;


import it.infoblu.mobilemap.helper.interfaces.IMapsSettingsConfiguration;
import it.infoblu.mobilemap.helper.mapbox.MapsMapBoxHelper;

/**
 * Created on 23/02/18.
 */

public class MapSettingsComposite implements IMapsSettingsConfiguration {

    private final IMapsSettingsConfiguration[] mSettings;

    public MapSettingsComposite(IMapsSettingsConfiguration... settings) {
        mSettings = settings;
    }

    @Override
    public void apply() {
        for (IMapsSettingsConfiguration setting : mSettings) {
            setting.apply();
        }
    }

    @Override
    public void revert() {
        for (IMapsSettingsConfiguration setting : mSettings) {
            setting.revert();
        }
    }

    @Override
    public boolean manageFollowState(MapsMapBoxHelper.LocationLayerBundle locationLayerBundle, int state) {
        boolean managed = false;
        for (IMapsSettingsConfiguration setting : mSettings) {
            managed = managed || setting.manageFollowState(locationLayerBundle, state);
        }
        return managed;
    }

}
