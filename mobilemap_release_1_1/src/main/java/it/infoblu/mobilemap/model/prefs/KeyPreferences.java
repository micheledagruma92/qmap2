package it.infoblu.mobilemap.model.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import com.f2prateek.rx.preferences2.RxSharedPreferences;

import it.infoblu.mobilemap.BuildConfig;


/**
 * Created on 05/02/18.
 */

public final class KeyPreferences {

    private static final String PREFERENCES_KEY = BuildConfig.APPLICATION_ID + ".prefs";

    private static KeyPreferences _INSTANCE;

    private final SharedPreferences mSharedPreferences;
    private final RxSharedPreferences mRxSharedPreferences;


    public static synchronized void init(Context context) {
        _INSTANCE = new KeyPreferences(context.getSharedPreferences(PREFERENCES_KEY, Context.MODE_PRIVATE));
    }

    public static RxSharedPreferences getPrefs() {
        return _INSTANCE.mRxSharedPreferences;
    }

    private KeyPreferences(SharedPreferences sharedPreferences) {
        mSharedPreferences = sharedPreferences;
        mRxSharedPreferences = RxSharedPreferences.create(mSharedPreferences);
    }

}
