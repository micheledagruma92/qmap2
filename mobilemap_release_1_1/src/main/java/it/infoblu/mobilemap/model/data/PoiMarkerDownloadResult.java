package it.infoblu.mobilemap.model.data;

/**
 * Created on 16/03/18.
 */

public class PoiMarkerDownloadResult {

    private final Poi poi;
    private Throwable cause;

    public PoiMarkerDownloadResult(Poi poi) {
        this(poi, null);
    }

    public PoiMarkerDownloadResult(Poi poi, Throwable cause) {
        this.poi = poi;
        this.cause = cause;
    }

    public Poi getPoi() {
        return poi;
    }

    public Throwable getCause() {
        return cause;
    }
}
