package it.infoblu.mobilemap.model.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.annimon.stream.Stream;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.android.gms.maps.model.LatLng;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.infoblu.mobilemap.helper.config.poiattribute.PoiAttributeInfo;
import it.infoblu.mobilemap.model.remote.deserializer.SimplePositionToLatLngDeserializer;
import it.infoblu.mobilemap.utils.InternalLogger;
import it.infoblu.mobilemap.utils.JsonMapperManager;

/**
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RemoteConfig implements Parcelable {

    @JsonProperty("init")
    private InitParameters initParameters;
    @JsonProperty("maps")
    private List<MapStyle> mapStyleList = new ArrayList<>();
    @JsonProperty("mapDefault")
    private String mapDefault;
    @JsonProperty("pitch")
    private double pitch;
    @JsonProperty("services")
    private Map<String, String> services = new HashMap<>();
    @JsonProperty("places")
    private List<PlaceService> places = new ArrayList<>();
    @JsonProperty("filters")
    private List<MapFilter> filters = new ArrayList<>();


    public RemoteConfig copy() {
        try {
            String configString = JsonMapperManager.MAPPER.writeValueAsString(this);
            return JsonMapperManager.MAPPER.readValue(configString, RemoteConfig.class);
        } catch (IOException e) {
            InternalLogger.e(e);
        }
        return null; // ERROR OCCURRED!
    }


    public InitParameters getInitParameters() {
        return initParameters;
    }

    public void setInitParameters(InitParameters initParameters) {
        this.initParameters = initParameters;
    }

    public List<MapStyle> getMapStyleList() {
        return mapStyleList;
    }

    public void setMapStyleList(List<MapStyle> mapStyleList) {
        this.mapStyleList = mapStyleList == null ? new ArrayList<>() : mapStyleList;
    }

    public String getMapDefault() {
        if (getMapStyleList() == null) {
            return null;
        }
        return StringUtils.defaultString(mapDefault, Stream.of(getMapStyleList())
                .findFirst()
                .map(mapStyle -> mapStyle != null ? mapStyle.id : null).get());
    }

    public void setMapDefault(String mapDefault) {
        this.mapDefault = mapDefault;
    }

    public double getPitch() {
        return pitch;
    }

    public void setPitch(double pitch) {
        this.pitch = pitch;
    }

    public Map<String, String> getServices() {
        return services;
    }

    public void setServices(Map<String, String> services) {
        this.services = services == null ? new HashMap<>() : services;
    }

    public List<PlaceService> getPlaces() {
        return places;
    }

    public void setPlaces(List<PlaceService> places) {
        this.places = places == null ? new ArrayList<>() : places;
    }

    public List<MapFilter> getFilters() {
        return filters;
    }

    public void setFilters(List<MapFilter> filters) {
        this.filters = filters == null ? new ArrayList<>() : filters;
    }


    /*
     * Accessibility methods
     */

    public PlaceService getPlace(final String poiType) {
        if (poiType == null) {
            InternalLogger.e("poiType should not be null!");
            return null; // EXIT
        }
        if (getPlaces() == null) {
            return null; // EXIT
        }
        synchronized (getPlaces()) {
            return Stream.of(getPlaces())
                    .filter(placeService -> poiType.equals(placeService.getPoiType())).findFirst().get();
        }
    }

    public List<MapFilter> getMapFilterList(final String poiType) {
        if (poiType == null) {
            InternalLogger.e("poiType should not be null!");
            return null; // EXIT
        }
        if (getFilters() == null) {
            return null; // EXIT
        }
        synchronized (getFilters()) {
            return Stream.of(getFilters())
                    .filter(mapFilter -> mapFilter.getPoiTypeList() != null
                            && mapFilter.getPoiTypeList().contains(poiType)).toList();
        }
    }

    public String getMapStyleEndpoint(final String styleId) {
        if (getMapStyleList() == null) {
            return null; // EXIT
        }
        for (MapStyle mapStyle : getMapStyleList()) {
            if (StringUtils.equals(mapStyle.getId(), styleId)) {
                return mapStyle.getUrl();
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return "PoiConfig{" +
                "mapStyleList=" + mapStyleList +
                ", pitch=" + pitch +
                ", services=" + services +
                ", places=" + places +
                '}';
    }


    /*
     * Parcelable
     */

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.initParameters, flags);
        dest.writeTypedList(this.mapStyleList);
        dest.writeString(this.mapDefault);
        dest.writeDouble(this.pitch);
        dest.writeInt(this.services.size());
        for (Map.Entry<String, String> entry : this.services.entrySet()) {
            dest.writeString(entry.getKey());
            dest.writeString(entry.getValue());
        }
        dest.writeTypedList(this.places);
        dest.writeTypedList(this.filters);
    }

    public RemoteConfig() {
    }

    protected RemoteConfig(Parcel in) {
        this.initParameters = in.readParcelable(InitParameters.class.getClassLoader());
        this.mapStyleList = in.createTypedArrayList(MapStyle.CREATOR);
        this.mapDefault = in.readString();
        this.pitch = in.readDouble();
        int servicesSize = in.readInt();
        this.services = new HashMap<>(servicesSize);
        for (int i = 0; i < servicesSize; i++) {
            String key = in.readString();
            String value = in.readString();
            this.services.put(key, value);
        }
        this.places = in.createTypedArrayList(PlaceService.CREATOR);
        this.filters = in.createTypedArrayList(MapFilter.CREATOR);
    }

    public static final Parcelable.Creator<RemoteConfig> CREATOR = new Parcelable.Creator<RemoteConfig>() {
        @Override
        public RemoteConfig createFromParcel(Parcel source) {
            return new RemoteConfig(source);
        }

        @Override
        public RemoteConfig[] newArray(int size) {
            return new RemoteConfig[size];
        }
    };


    /*
     * Sub structures
     */

    public static class InitParameters implements Parcelable {

        private int zoom;

        @JsonDeserialize(using = SimplePositionToLatLngDeserializer.class)
        private LatLng point;

        public int getZoom() {
            return zoom;
        }

        public void setZoom(int zoom) {
            this.zoom = zoom;
        }

        public LatLng getPoint() {
            return point;
        }

        public void setPoint(LatLng point) {
            this.point = point;
        }

        @Override
        public String toString() {
            return "InitParameters{" +
                    "zoom=" + zoom +
                    ", point=" + point +
                    '}';
        }


        /*
         * Parcelable
         */

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(this.zoom);
            dest.writeParcelable(this.point, flags);
        }

        public InitParameters() {
        }

        protected InitParameters(Parcel in) {
            this.zoom = in.readInt();
            this.point = in.readParcelable(LatLng.class.getClassLoader());
        }

        public static final Parcelable.Creator<InitParameters> CREATOR = new Parcelable.Creator<InitParameters>() {
            @Override
            public InitParameters createFromParcel(Parcel source) {
                return new InitParameters(source);
            }

            @Override
            public InitParameters[] newArray(int size) {
                return new InitParameters[size];
            }
        };

    }


    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class PlaceService extends PoiAttributeInfo {

        private String name;

        private String endpoint;

        private String poiType;

        private long refresh;

        private long until;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getEndpoint() {
            return endpoint;
        }

        public void setEndpoint(String endpoint) {
            this.endpoint = endpoint;
        }

        public String getPoiType() {
            return poiType;
        }

        public void setPoiType(String poiType) {
            this.poiType = poiType;
        }

        public long getRefresh() {
            return refresh;
        }

        public void setRefresh(long refresh) {
            this.refresh = refresh;
        }

        public long getUntil() {
            return until;
        }

        public void setUntil(long until) {
            this.until = until;
        }

        @Override
        public String toString() {
            return "PlaceService{" +
                    "name='" + name + '\'' +
                    ", endpoint='" + endpoint + '\'' +
                    ", poiType='" + poiType + '\'' +
                    ", refresh=" + refresh +
                    ", until=" + until +
                    "} " + super.toString();
        }

        /*
         * Parcelable
         */

        public PlaceService() {
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeString(this.name);
            dest.writeString(this.endpoint);
            dest.writeString(this.poiType);
            dest.writeLong(this.refresh);
            dest.writeLong(this.until);
        }

        protected PlaceService(Parcel in) {
            super(in);
            this.name = in.readString();
            this.endpoint = in.readString();
            this.poiType = in.readString();
            this.refresh = in.readLong();
            this.until = in.readLong();
        }

        public static final Creator<PlaceService> CREATOR = new Creator<PlaceService>() {
            @Override
            public PlaceService createFromParcel(Parcel source) {
                return new PlaceService(source);
            }

            @Override
            public PlaceService[] newArray(int size) {
                return new PlaceService[size];
            }
        };
    }


    public static class MapStyle implements Parcelable {

        @JsonProperty("id")
        private String id;

        @JsonProperty("name")
        private String name;

        @JsonProperty("url")
        private String url;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        @Override
        public String toString() {
            return "MapStyle{" +
                    "id='" + id + '\'' +
                    ", name='" + name + '\'' +
                    ", url='" + url + '\'' +
                    '}';
        }

        /*
         * Parcelable
         */

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.id);
            dest.writeString(this.name);
            dest.writeString(this.url);
        }

        public MapStyle() {
        }

        protected MapStyle(Parcel in) {
            this.id = in.readString();
            this.name = in.readString();
            this.url = in.readString();
        }

        public static final Parcelable.Creator<MapStyle> CREATOR = new Parcelable.Creator<MapStyle>() {
            @Override
            public MapStyle createFromParcel(Parcel source) {
                return new MapStyle(source);
            }

            @Override
            public MapStyle[] newArray(int size) {
                return new MapStyle[size];
            }
        };
    }


    public static class MapFilter implements Parcelable {

        @JsonProperty("id")
        private String id;

        @JsonProperty("name")
        private String name;

        @JsonProperty("poiTypes")
        private List<String> poiTypeList;

        @JsonProperty("layers")
        private List<String> layerList;

        @JsonProperty("def") // remotely mapped as default
        private boolean enabled;

        public MapFilter copy() {
            MapFilter filter = new MapFilter();
            filter.setId(this.getId());
            filter.setName(this.getName());
            filter.setPoiTypeList(this.getPoiTypeList());
            filter.setLayerList(this.getLayerList());
            filter.setEnabled(this.isEnabled());
            return filter;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<String> getPoiTypeList() {
            return poiTypeList;
        }

        public void setPoiTypeList(List<String> poiTypeList) {
            this.poiTypeList = poiTypeList;
        }

        public List<String> getLayerList() {
            return layerList;
        }

        public void setLayerList(List<String> layerList) {
            this.layerList = layerList;
        }

        public boolean isEnabled() {
            return enabled;
        }

        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }

        @Override
        public String toString() {
            return "MapFilter{" +
                    "id='" + id + '\'' +
                    ", name='" + name + '\'' +
                    ", poiTypeList=" + poiTypeList +
                    ", layerList=" + layerList +
                    ", enabled=" + enabled +
                    '}';
        }

        /*
         * Parcelable
         */

        public MapFilter() {
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.id);
            dest.writeString(this.name);
            dest.writeStringList(this.poiTypeList);
            dest.writeStringList(this.layerList);
            dest.writeByte(this.enabled ? (byte) 1 : (byte) 0);
        }

        protected MapFilter(Parcel in) {
            this.id = in.readString();
            this.name = in.readString();
            this.poiTypeList = in.createStringArrayList();
            this.layerList = in.createStringArrayList();
            this.enabled = in.readByte() != 0;
        }

        public static final Creator<MapFilter> CREATOR = new Creator<MapFilter>() {
            @Override
            public MapFilter createFromParcel(Parcel source) {
                return new MapFilter(source);
            }

            @Override
            public MapFilter[] newArray(int size) {
                return new MapFilter[size];
            }
        };
    }

}
