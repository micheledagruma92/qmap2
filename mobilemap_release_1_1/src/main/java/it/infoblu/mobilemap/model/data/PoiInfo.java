
package it.infoblu.mobilemap.model.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PoiInfo implements Parcelable {

    @JsonProperty("title")
    private String title;
    @JsonProperty("description")
    private String description;
    @JsonProperty("icoRelativeUrl")
    private String icoRelativeUrl;
    @JsonProperty("icoAnchor")
    private String iconAnchor;
    @JsonProperty("severity")
    private int severity;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcoRelativeUrl() {
        return icoRelativeUrl;
    }

    public void setIcoRelativeUrl(String icoRelativeUrl) {
        this.icoRelativeUrl = icoRelativeUrl;
    }

    public String getIconAnchor() {
        return iconAnchor;
    }

    public void setIconAnchor(String iconAnchor) {
        this.iconAnchor = iconAnchor;
    }

    public int getSeverity() {
        return severity;
    }

    public void setSeverity(int severity) {
        this.severity = severity;
    }

    @Override
    public String toString() {
        return "PoiInfo{" +
                "title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", icoRelativeUrl='" + icoRelativeUrl + '\'' +
                ", iconAnchor='" + iconAnchor + '\'' +
                ", severity=" + severity +
                '}';
    }


    /*
     * Parcelable
     */

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.title);
        dest.writeString(this.description);
        dest.writeString(this.icoRelativeUrl);
        dest.writeString(this.iconAnchor);
        dest.writeInt(this.severity);
    }

    public PoiInfo() {
    }

    protected PoiInfo(Parcel in) {
        this.title = in.readString();
        this.description = in.readString();
        this.icoRelativeUrl = in.readString();
        this.iconAnchor = in.readString();
        this.severity = in.readInt();
    }

    public static final Parcelable.Creator<PoiInfo> CREATOR = new Parcelable.Creator<PoiInfo>() {
        @Override
        public PoiInfo createFromParcel(Parcel source) {
            return new PoiInfo(source);
        }

        @Override
        public PoiInfo[] newArray(int size) {
            return new PoiInfo[size];
        }
    };

}
