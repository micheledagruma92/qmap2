package it.infoblu.mobilemap.model.data;

import android.graphics.Color;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 16/02/18.
 */

public class MMPolyline {

    private String id;

    private List<LatLng> pointList;

    private Options options;


    public MMPolyline() {
        this(null);
    }

    public MMPolyline(List<LatLng> pointList) {
        this.pointList = pointList == null ? new ArrayList<LatLng>() : pointList;
    }

    public String getId() {
        return id;
    }

    public MMPolyline id(String id) {
        this.id = id;
        return this;
    }

    public MMPolyline add(LatLng point) {
        pointList.add(point);
        return this;
    }

    public MMPolyline add(LatLng... points) {
        for (LatLng point : points) {
            add(point);
        }
        return this;
    }

    public MMPolyline addAll(Iterable<LatLng> points) {
        for (LatLng point : points) {
            add(point);
        }
        return this;
    }

    public List<LatLng> getPoints() {
        return pointList;
    }

    public MMPolyline options(Options options) {
        this.options = options;
        return this;
    }

    public Options getOptions() {
        return options;
    }


    /*
     * Options
     */

    public static class Options {

        private float alpha = 1f;

        private int color = Color.parseColor("#009688");

        private float width = 2f;

        public Options alpha(float alpha) {
            this.alpha = alpha;
            return this;
        }

        public float getAlpha() {
            return this.alpha;
        }

        public Options color(int color) {
            this.color = color;
            return this;
        }

        public int getColor() {
            return this.color;
        }

        public Options width(float width) {
            this.width = width;
            return this;
        }

        public float getWidth() {
            return this.width;
        }

    }

}
