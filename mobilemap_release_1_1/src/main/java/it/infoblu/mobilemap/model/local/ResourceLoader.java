package it.infoblu.mobilemap.model.local;

import android.content.Context;

import java.io.InputStream;

/**
 * Created on 09/02/18.
 */

public final class ResourceLoader {

    private static Context mContext;

    private ResourceLoader() {
    }

    public static void init(Context context) {
        if (context == null) {
            throw new RuntimeException("Context passed to ResourceLoader has not be null");
        }
        mContext = context.getApplicationContext();
    }

    public static InputStream getRaw(int rawId) {
        return mContext.getResources().openRawResource(rawId);
    }

    public static int getDrawableResId(String drawableName) {
        return mContext.getResources().getIdentifier(drawableName, "drawable", mContext.getPackageName());
    }

}
