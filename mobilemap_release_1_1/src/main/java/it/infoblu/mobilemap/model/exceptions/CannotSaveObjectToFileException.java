package it.infoblu.mobilemap.model.exceptions;

/**
 * Created by carloluchessa on 23/09/15.
 */
public class CannotSaveObjectToFileException extends Exception {

    public CannotSaveObjectToFileException(String detailMessage) {
        super(detailMessage);
    }

    public CannotSaveObjectToFileException(Throwable throwable) {
        super(throwable);
    }
}
