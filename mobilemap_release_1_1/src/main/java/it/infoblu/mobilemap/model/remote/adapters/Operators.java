package it.infoblu.mobilemap.model.remote.adapters;


import androidx.annotation.NonNull;

/**
 * Created by on 24/05/17.
 */

public final class Operators {

    private Operators() {
    }

    @NonNull
    public static <T> ApiErrorOperator<T> apiError() {
        return new ApiErrorOperator<>();
    }
}
