package it.infoblu.mobilemap.model.exceptions;

/**
 * Created by carloluchessa on 23/09/15.
 */
public class CannotLoadObjectFromFileException extends Exception {

    public CannotLoadObjectFromFileException(String detailMessage) {
        super(detailMessage);
    }

    public CannotLoadObjectFromFileException(Throwable throwable) {
        super(throwable);
    }
}
