package it.infoblu.mobilemap.model.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created on 16/03/18.
 */

public class PoiUpdateInfo {

    private String type;

    private List<Poi> poiList;

    public PoiUpdateInfo(String type) {
        this(type, null);
    }

    public PoiUpdateInfo(String type, List<Poi> poiList) {
        this.type = type;
        this.poiList = poiList == null ? new ArrayList<Poi>() : poiList;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Poi> getPoiList() {
        return poiList;
    }

    public void setPoiList(List<Poi> poiList) {
        this.poiList = poiList;
    }

    @Override
    public String toString() {
        return "PoiUpdateInfo{" +
                "type='" + type + '\'' +
                ", poiList=" + poiList +
                '}';
    }
}
