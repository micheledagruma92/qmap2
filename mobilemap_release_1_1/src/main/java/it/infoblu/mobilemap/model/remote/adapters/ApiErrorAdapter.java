package it.infoblu.mobilemap.model.remote.adapters;//package it.infoblu.model.remote.adapters;


import androidx.annotation.Nullable;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.CallAdapter;
import retrofit2.Retrofit;

/**
 * Created by on 24/05/17.
 */

public class ApiErrorAdapter extends CallAdapter.Factory {

    @Nullable
    @Override
    public CallAdapter<?, ?> get(Type returnType, Annotation[] annotations, Retrofit retrofit) {
        if (getRawType(returnType) != Single.class) {
            return null; // Ignore non-Single types.
        }

        final CallAdapter<Object, Single<?>> singleDelegate = (CallAdapter<Object, Single<?>>)
                retrofit.nextCallAdapter(this, returnType, annotations);

        return new CallAdapter<Object, Object>() {
            @Override
            public Type responseType() {
                return singleDelegate.responseType();
            }

            @Override
            public Object adapt(Call<Object> call) {
                Single<?> single = singleDelegate.adapt(call);
                return single.lift(Operators.apiError());
            }
        };

    }
}
