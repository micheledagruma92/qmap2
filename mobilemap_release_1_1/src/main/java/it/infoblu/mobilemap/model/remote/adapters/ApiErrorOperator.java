package it.infoblu.mobilemap.model.remote.adapters;

import io.reactivex.SingleObserver;
import io.reactivex.SingleOperator;
import io.reactivex.disposables.Disposable;

/**
 * Created by on 24/05/17.
 */

public class ApiErrorOperator<T> implements SingleOperator<T, T> {

    @Override
    public SingleObserver<? super T> apply(final SingleObserver<? super T> observer) throws Exception {
        return new SingleObserver<T>() {
            @Override
            public void onSubscribe(Disposable d) {
                observer.onSubscribe(d);
            }

            @Override
            public void onSuccess(T response) {
                // TODO: 15/03/18 do something to properly manage error
                observer.onSuccess(response);
            }

            @Override
            public void onError(Throwable e) {
                observer.onError(e);
            }
        };
    }
}
