package it.infoblu.mobilemap.model.data;

import android.os.Parcel;
import android.os.Parcelable;

public class FilterStateInfo implements Parcelable {

    private String id;

    private boolean enabled;

    public FilterStateInfo(String id, boolean enabled) {
        this.id = id;
        this.enabled = enabled;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "FilterPersistenceInfo{" +
                "id='" + id + '\'' +
                ", enabled=" + enabled +
                '}';
    }

    /*
     * Parcelable
     */


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeByte(this.enabled ? (byte) 1 : (byte) 0);
    }

    protected FilterStateInfo(Parcel in) {
        this.id = in.readString();
        this.enabled = in.readByte() != 0;
    }

    public static final Parcelable.Creator<FilterStateInfo> CREATOR = new Parcelable.Creator<FilterStateInfo>() {
        @Override
        public FilterStateInfo createFromParcel(Parcel source) {
            return new FilterStateInfo(source);
        }

        @Override
        public FilterStateInfo[] newArray(int size) {
            return new FilterStateInfo[size];
        }
    };

}