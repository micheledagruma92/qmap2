package it.infoblu.mobilemap.model.constants;

/**
 * Created on 16/03/18.
 */

public final class InternalGeometryArrowProperty {

    private InternalGeometryArrowProperty() {
    }

    public static final String ROTATION = "rotation";

}
