package it.infoblu.mobilemap.model.remote;

import io.reactivex.Single;
import it.infoblu.mobilemap.model.data.RemoteConfig;
import it.infoblu.mobilemap.model.data.geojson.FeatureCollection;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface IRestApi {

    @GET
    Single<RemoteConfig> getConfiguration(@Url String endpoint);

    @GET
    Single<FeatureCollection> getPoi(@Url String endpoint);

}