package it.infoblu.mobilemap.model.remote.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;

/**
 * Created on 12/09/18.
 */
public class SimplePositionToLatLngDeserializer extends JsonDeserializer<LatLng> {

    @Override
    public LatLng deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException {
        ObjectCodec oc = jp.getCodec();
        JsonNode node = oc.readTree(jp);
        return new LatLng(node.get("lat").asDouble(), node.get("lng").asDouble());
    }

}
