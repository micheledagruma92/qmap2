package it.infoblu.mobilemap.model.remote;

import android.net.Uri;

import androidx.annotation.Nullable;

import java.util.List;

import io.reactivex.Single;
import it.infoblu.mobilemap.MobileMap;
import it.infoblu.mobilemap.model.converter.FeatureCollectionToPoiConverter;
import it.infoblu.mobilemap.model.data.MobileMapConfiguration;
import it.infoblu.mobilemap.model.data.Poi;
import it.infoblu.mobilemap.model.data.RemoteConfig;
import it.infoblu.mobilemap.model.remote.utils.RetrofitUtils;
import it.infoblu.mobilemap.utils.StringTemplateUtils;


public class NetworkProvider {

    private static final String QUERY_STRING_PARAM_NAME_API_KEY = "apikey";

    @Nullable
    private static NetworkProvider INSTANCE = null;
    private Single<IRestApi> mApiSingle;

    private NetworkProvider() {
        mApiSingle = MobileMap.getConfigurationSingle().map(mobileMapConfiguration ->
                RetrofitUtils.getRetrofit(NetworkConfigurator.getInstance().getHttpClient()).create(IRestApi.class));
    }

    public synchronized static NetworkProvider getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new NetworkProvider();
        }
        return INSTANCE;
    }


    public Single<RemoteConfig> getConfiguration() {
        return MobileMap.getConfigurationSingle()
                .flatMap(mobileMapConfiguration -> mApiSingle
                        .flatMap(mApi -> mApi.getConfiguration(mobileMapConfiguration.getPoiConfigEndpoint())
                                .map(config -> {
                                    // inject apikey in style url
                                    for (RemoteConfig.MapStyle mapStyle : config.getMapStyleList()) {
                                        mapStyle.setUrl(decorateApiKeyForced(mobileMapConfiguration, mapStyle.getUrl()));
                                    }
                                    return config;
                                })));
    }

    public Single<List<Poi>> getPoi(String endpoint) {
        return MobileMap.getConfigurationSingle()
                .flatMap(mobileMapConfiguration -> {
                    String endpointDecorated = decorateApiKey(mobileMapConfiguration, endpoint);
                    return mApiSingle.flatMap(mApi -> mApi.getPoi(endpointDecorated)
                            .map(featureCollection -> FeatureCollectionToPoiConverter.convert(featureCollection)));
                });
    }


    /*
     * Internal utils
     */

    public static String decorateApiKey(MobileMapConfiguration mobileMapConfiguration, String endpoint) {
        if (endpoint.contains(QUERY_STRING_PARAM_NAME_API_KEY)) {
            return StringTemplateUtils.decorateCurlyBracket(endpoint, QUERY_STRING_PARAM_NAME_API_KEY, mobileMapConfiguration.getApikey());
        }
        return endpoint;
    }

    public static String decorateApiKeyForced(MobileMapConfiguration mobileMapConfiguration, String endpoint) {
        if (endpoint.contains(QUERY_STRING_PARAM_NAME_API_KEY)) {
            return StringTemplateUtils.decorateCurlyBracket(endpoint, QUERY_STRING_PARAM_NAME_API_KEY, mobileMapConfiguration.getApikey());
        }
        return Uri.parse(endpoint).buildUpon()
                .appendQueryParameter(QUERY_STRING_PARAM_NAME_API_KEY, mobileMapConfiguration.getApikey())
                .build().toString();
    }

}