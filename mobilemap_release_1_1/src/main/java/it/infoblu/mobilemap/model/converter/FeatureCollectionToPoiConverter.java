package it.infoblu.mobilemap.model.converter;

import android.text.TextUtils;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import it.infoblu.mobilemap.MobileMap;
import it.infoblu.mobilemap.model.data.Poi;
import it.infoblu.mobilemap.model.data.PoiInfo;
import it.infoblu.mobilemap.model.data.PoiPosition;
import it.infoblu.mobilemap.model.data.RemoteConfig;
import it.infoblu.mobilemap.model.data.geojson.Feature;
import it.infoblu.mobilemap.model.data.geojson.FeatureCollection;
import it.infoblu.mobilemap.utils.InternalLogger;

import static it.infoblu.mobilemap.model.constants.PoiProperty.CODE;
import static it.infoblu.mobilemap.model.constants.PoiProperty.DESCRIPTION;
import static it.infoblu.mobilemap.model.constants.PoiProperty.ICO_RELATIVE_URL;
import static it.infoblu.mobilemap.model.constants.PoiProperty.ID;
import static it.infoblu.mobilemap.model.constants.PoiProperty.POI_TITLE;
import static it.infoblu.mobilemap.model.constants.PoiProperty.ROAD_DIRECTION_NAME;
import static it.infoblu.mobilemap.model.constants.PoiProperty.ROAD_LCD;
import static it.infoblu.mobilemap.model.constants.PoiProperty.ROAD_NAME;
import static it.infoblu.mobilemap.model.constants.PoiProperty.SEVERITY;
import static it.infoblu.mobilemap.model.constants.PoiProperty.SUBTYPE_ID;

/**
 * Created on 14/03/18.
 */

public final class FeatureCollectionToPoiConverter {

    private FeatureCollectionToPoiConverter() {
    }

    public static List<Poi> convert(FeatureCollection featureCollection) {
        List<Poi> poiList = new ArrayList<>();

        if (featureCollection == null || featureCollection.getFeatures() == null) {
            return poiList; // EXIT
        }

        RemoteConfig remoteConfig = MobileMap.getSession().getRemoteConfigSync();

        for (Feature feature : featureCollection.getFeatures()) {
            if (isValidFeature(feature)) {
                Poi poi = new Poi();

                Map<String,String> propertyJobMap = new HashMap(feature.getProperties()); // copy

                poi.setId(propertyJobMap.remove(ID));
                poi.setCode(propertyJobMap.remove(CODE));
                poi.setType(propertyJobMap.remove(SUBTYPE_ID));
                // TODO: 14/03/18 manca filter tag

                PoiPosition position = new PoiPosition();
                position.setLat(feature.getGeometry().getCoordinates().get(1));
                position.setLng(feature.getGeometry().getCoordinates().get(0));
                position.setDirection(StringUtils.defaultString(propertyJobMap.remove(ROAD_DIRECTION_NAME)));
                position.setRoadLcd(StringUtils.defaultString(propertyJobMap.remove(ROAD_LCD)));
                position.setRoadName(StringUtils.defaultString(propertyJobMap.remove(ROAD_NAME)));
                poi.setPosition(position);

                PoiInfo info = new PoiInfo();
                info.setTitle(StringUtils.defaultString(propertyJobMap.remove(POI_TITLE)));
                info.setDescription(StringUtils.defaultString(propertyJobMap.remove(DESCRIPTION)));
                info.setIcoRelativeUrl(StringUtils.defaultString(propertyJobMap.remove(ICO_RELATIVE_URL)));
                info.setIconAnchor(remoteConfig.getPlace(poi.getType()).getIconAnchor());
                info.setSeverity(Integer.parseInt(StringUtils.defaultString(propertyJobMap.remove(SEVERITY), "0")));
                poi.setInfo(info);

                poi.setPropertyMap(feature.getProperties()); // NB: during the Poi object construction some properties are removed

                poiList.add(poi);
            }
        }

        return poiList;
    }


    /*
     * Internal methods
     */

    private static boolean isValidFeature(Feature feature) {
        if (feature.getGeometry() == null) {
            InternalLogger.e("Feature to Poi error. Feature geometry is null: %s", feature);
            return false;
        }
        if (!StringUtils.equalsIgnoreCase("point", feature.getGeometry().getType())) {
            InternalLogger.e("Feature to Poi error. Feature geometry type is not a Point: %s", feature);
            return false;
        }
        if (feature.getGeometry().getCoordinates().size() != 2) {
            InternalLogger.e("Feature to Poi error. Feature geometry has not lat,lng coordinates: %s", feature);
            return false;
        }
        if (feature.getProperties() == null) {
            InternalLogger.e("Feature to Poi error. Feature geometry has not properties: %s", feature);
            return false;
        }
        if (TextUtils.isEmpty(feature.getProperties().get(ID))) {
            InternalLogger.e("Feature to Poi error. Feature geometry has not a proper Poi id: %s", feature);
            return false;
        }
        if (TextUtils.isEmpty(feature.getProperties().get(SUBTYPE_ID))) {
            InternalLogger.e("Feature to Poi error. Feature geometry has not a proper Poi subtype_id: %s", feature);
            return false;
        }
        return true;
    }

}
