
package it.infoblu.mobilemap.model.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Poi implements Parcelable {

    @JsonProperty("id")
    private String id;
    @JsonProperty("code")
    private String code;
    @JsonProperty("timeDesc")
    private String timeDesc;
    @JsonProperty("type")
    private String type;
    @JsonProperty("filterTag")
    private String filterTag;
    @JsonProperty("position")
    private PoiPosition position;
    @JsonProperty("info")
    private PoiInfo info;
    @JsonProperty("properties")
    private Map<String, String> propertyMap;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTimeDesc() {
        return timeDesc;
    }

    public void setTimeDesc(String timeDesc) {
        this.timeDesc = timeDesc;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFilterTag() {
        return filterTag;
    }

    public void setFilterTag(String filterTag) {
        this.filterTag = filterTag;
    }

    public PoiPosition getPosition() {
        return position;
    }

    public void setPosition(PoiPosition position) {
        this.position = position;
    }

    public PoiInfo getInfo() {
        return info;
    }

    public void setInfo(PoiInfo info) {
        this.info = info;
    }

    public Map<String, String> getPropertyMap() {
        return propertyMap;
    }

    public void setPropertyMap(Map<String, String> propertyMap) {
        this.propertyMap = propertyMap;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Poi poi = (Poi) o;

        if (id != null ? !id.equals(poi.id) : poi.id != null) return false;
        if (type != null ? !type.equals(poi.type) : poi.type != null) return false;
        return filterTag != null ? filterTag.equals(poi.filterTag) : poi.filterTag == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (filterTag != null ? filterTag.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Poi{" +
                "id='" + id + '\'' +
                ", code='" + code + '\'' +
                ", timeDesc='" + timeDesc + '\'' +
                ", type='" + type + '\'' +
                ", filterTag='" + filterTag + '\'' +
                ", position=" + position +
                ", info=" + info +
                ", propertyMap=" + propertyMap +
                '}';
    }


    /*
     * Parcelable
     */

    public Poi() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.code);
        dest.writeString(this.timeDesc);
        dest.writeString(this.type);
        dest.writeString(this.filterTag);
        dest.writeParcelable(this.position, flags);
        dest.writeParcelable(this.info, flags);
        dest.writeInt(this.propertyMap.size());
        for (Map.Entry<String, String> entry : this.propertyMap.entrySet()) {
            dest.writeString(entry.getKey());
            dest.writeString(entry.getValue());
        }
    }

    protected Poi(Parcel in) {
        this.id = in.readString();
        this.code = in.readString();
        this.timeDesc = in.readString();
        this.type = in.readString();
        this.filterTag = in.readString();
        this.position = in.readParcelable(PoiPosition.class.getClassLoader());
        this.info = in.readParcelable(PoiInfo.class.getClassLoader());
        int propertyMapSize = in.readInt();
        this.propertyMap = new HashMap<String, String>(propertyMapSize);
        for (int i = 0; i < propertyMapSize; i++) {
            String key = in.readString();
            String value = in.readString();
            this.propertyMap.put(key, value);
        }
    }

    public static final Creator<Poi> CREATOR = new Creator<Poi>() {
        @Override
        public Poi createFromParcel(Parcel source) {
            return new Poi(source);
        }

        @Override
        public Poi[] newArray(int size) {
            return new Poi[size];
        }
    };

}
