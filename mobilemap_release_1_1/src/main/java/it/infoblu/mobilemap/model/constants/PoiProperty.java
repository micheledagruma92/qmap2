package it.infoblu.mobilemap.model.constants;

import com.google.common.collect.ImmutableList;

/**
 * Created on 16/03/18.
 */

public final class PoiProperty {

    private PoiProperty() {
    }

    public static final String ID = "id";
    public static final String CODE = "code";
    public static final String SUBTYPE_ID = "subtype_id";
    public static final String ROAD_NAME = "road_name";
    public static final String ROAD_DIRECTION_NAME = "road_direction_name";
    public static final String POI_TITLE = "title";
    public static final String ICO_RELATIVE_URL = "ico_relative_url";
    public static final String DESCRIPTION = "description";
    public static final String SEVERITY = "severity";
    public static final String ROAD_LCD = "road_lcd";
    public static final String ENC_GEOM = "enc_geom";

    // local
    public static final String ICO_ANCHOR = "ico_anchor"; // this value is took from PlaceService config objects

    public static final class DEFAULT {
        private DEFAULT() {
        }

        public static final String ICO_RELATIVE_URL = "ico_relative_url_default";
        public static final String ICO_ANCHOR = VALUES.ICO_ANCHOR_VALUES.get(0);
    }

    public static final class VALUES {

        private VALUES() {
        }

        public static final ImmutableList<String> ICO_ANCHOR_VALUES = ImmutableList.<String>builder()
                .add("center")
                .add("bottom")
                .build();
    }

}
