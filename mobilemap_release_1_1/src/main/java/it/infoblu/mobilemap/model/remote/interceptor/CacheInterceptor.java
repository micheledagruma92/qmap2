package it.infoblu.mobilemap.model.remote.interceptor;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Response;

/**
 * Created on 18/07/17.
 */

public class CacheInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        Response originalResponse = chain.proceed(chain.request());
//        if (NetworkUtils.isNetworkAvailable(CustomApplication.getContext())) {
        int maxAge = 60 * 60; // read from cache for 1 minute
        return originalResponse.newBuilder()
                .header("Cache-Control", "public, max-age=" + maxAge)
                .build();
//        } else {
//            int maxStale = 60 * 60; // tolerate 1 hour stale
//            return originalResponse.newBuilder()
//                    .header("Cache-Control", "public, only-if-cached, max-stale=" + maxStale)
//                    .build();
//        }
    }
}
