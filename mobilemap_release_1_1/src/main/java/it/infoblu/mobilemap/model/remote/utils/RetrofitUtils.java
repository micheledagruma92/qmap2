package it.infoblu.mobilemap.model.remote.utils;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.concurrent.Executors;

import io.reactivex.schedulers.Schedulers;
import it.infoblu.mobilemap.MobileMap;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;


public final class RetrofitUtils {

    private static Retrofit sRetrofit;

    public static Retrofit getRetrofit(OkHttpClient client) {
        if (sRetrofit == null) {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

            Retrofit.Builder retrofitBuilder = new Retrofit.Builder();
            retrofitBuilder
//                    .addCallAdapterFactory(new ApiErrorAdapter())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
                    .baseUrl("https://no_url/") // base url managed at runtime
                    .addConverterFactory(JacksonConverterFactory.create(objectMapper))
                    .callbackExecutor(Executors.newCachedThreadPool());
            // NB: here config is always available
            if (MobileMap.getConfigurationSync().isLogEnabled())
                retrofitBuilder.client(client);
            sRetrofit = retrofitBuilder.build();
        }
        return sRetrofit;
    }
}
