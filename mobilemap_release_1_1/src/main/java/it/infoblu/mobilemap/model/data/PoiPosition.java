
package it.infoblu.mobilemap.model.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.android.gms.maps.model.LatLng;

public class PoiPosition implements Parcelable {

    private Double lat;
    private Double lng;
    private String roadLcd;
    private String direction;
    private String roadName;

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public String getRoadLcd() {
        return roadLcd;
    }

    public void setRoadLcd(String roadLcd) {
        this.roadLcd = roadLcd;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getRoadName() {
        return roadName;
    }

    public void setRoadName(String roadName) {
        this.roadName = roadName;
    }

    public LatLng getLatLng() {
        return new LatLng(getLat(), getLng());
    }

    @Override
    public String toString() {
        return "PoiPosition{" +
                "lat=" + lat +
                ", lng=" + lng +
                ", roadLcd='" + roadLcd + '\'' +
                ", direction='" + direction + '\'' +
                ", roadName='" + roadName + '\'' +
                '}';
    }


    /*
     * Parcelable
     */

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.lat);
        dest.writeValue(this.lng);
        dest.writeString(this.roadLcd);
        dest.writeString(this.direction);
        dest.writeString(this.roadName);
    }

    public PoiPosition() {
    }

    protected PoiPosition(Parcel in) {
        this.lat = (Double) in.readValue(Double.class.getClassLoader());
        this.lng = (Double) in.readValue(Double.class.getClassLoader());
        this.roadLcd = in.readString();
        this.direction = in.readString();
        this.roadName = in.readString();
    }

    public static final Parcelable.Creator<PoiPosition> CREATOR = new Parcelable.Creator<PoiPosition>() {
        @Override
        public PoiPosition createFromParcel(Parcel source) {
            return new PoiPosition(source);
        }

        @Override
        public PoiPosition[] newArray(int size) {
            return new PoiPosition[size];
        }
    };

}
