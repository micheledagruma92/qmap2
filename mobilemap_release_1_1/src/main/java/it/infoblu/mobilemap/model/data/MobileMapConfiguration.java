package it.infoblu.mobilemap.model.data;

import java.util.HashMap;
import java.util.Map;

/**
 * Created on 05/02/18.
 */

public class MobileMapConfiguration {

    private String poiConfigEndpoint;

    private int poiConfigDefaultRawId;

    private String apikey;

    private Map<String, Integer> poiIconPlaceholderResIdMap = new HashMap<>();

    private boolean logEnabled;


    private MobileMapConfiguration() {
    }

    public String getPoiConfigEndpoint() {
        return poiConfigEndpoint;
    }

    private void setPoiConfigEndpoint(String poiConfigEndpoint) {
        this.poiConfigEndpoint = poiConfigEndpoint;
    }

    public int getPoiConfigDefaultRawId() {
        return poiConfigDefaultRawId;
    }

    private void setPoiConfigDefaultRawId(int poiConfigDefaultRawId) {
        this.poiConfigDefaultRawId = poiConfigDefaultRawId;
    }

    public String getApikey() {
        return apikey;
    }

    private void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public Map<String, Integer> getPoiIconPlaceholderResIdMap() {
        return poiIconPlaceholderResIdMap;
    }

    public void setPoiIconPlaceholderResIdMap(Map<String, Integer> poiIconPlaceholderResIdMap) {
        this.poiIconPlaceholderResIdMap = poiIconPlaceholderResIdMap;
    }

    public boolean isLogEnabled() {
        return logEnabled;
    }

    private void setLogEnabled(boolean logEnabled) {
        this.logEnabled = logEnabled;
    }

    @Override
    public String toString() {
        return "MapsConfiguration{" +
                "poiConfigEndpoint='" + poiConfigEndpoint + '\'' +
                ", poiConfigDefaultRawId=" + poiConfigDefaultRawId +
                ", logEnabled=" + logEnabled +
                '}';
    }


    /*
     * Builder
     */

    public static class Builder {

        private MobileMapConfiguration configuration;

        public Builder() {
            configuration = new MobileMapConfiguration();
        }

        public Builder poiConfigEndpoint(String endpoint) {
            configuration.setPoiConfigEndpoint(endpoint);
            return this;
        }

        public Builder poiConfigDefaultRawId(int defRawId) {
            configuration.setPoiConfigDefaultRawId(defRawId);
            return this;
        }

        public Builder apikey(String apikey) {
            configuration.setApikey(apikey);
            return this;
        }

        public Builder logEnabled(boolean enabled) {
            configuration.setLogEnabled(enabled);
            return this;
        }

        public Builder poiIconPlaceholderResIdMap(Map<String, Integer> poiIconPlaceholderResIdMap) {
            if (poiIconPlaceholderResIdMap == null) { // safe check
                poiIconPlaceholderResIdMap = new HashMap<>();
            }
            configuration.setPoiIconPlaceholderResIdMap(poiIconPlaceholderResIdMap);
            return this;
        }

        public Builder addPoiIconPlaceholderResId(String placeholderName, int placeholderResId) {
            configuration.getPoiIconPlaceholderResIdMap().put(placeholderName, placeholderResId);
            return this;
        }

        public MobileMapConfiguration build() {
            return configuration;
        }
    }

}
