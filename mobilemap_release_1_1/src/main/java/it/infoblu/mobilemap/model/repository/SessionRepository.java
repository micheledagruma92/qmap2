package it.infoblu.mobilemap.model.repository;

import android.text.TextUtils;

import com.annimon.stream.Stream;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.android.gms.maps.model.LatLngBounds;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;
import it.infoblu.mobilemap.IMobileMapSession;
import it.infoblu.mobilemap.MobileMap;
import it.infoblu.mobilemap.manager.PoiPersistenceManager;
import it.infoblu.mobilemap.model.data.MobileMapConfiguration;
import it.infoblu.mobilemap.model.data.Poi;
import it.infoblu.mobilemap.model.data.PoiUpdateInfo;
import it.infoblu.mobilemap.model.data.RemoteConfig;
import it.infoblu.mobilemap.model.local.ResourceLoader;
import it.infoblu.mobilemap.model.prefs.KeyPreferences;
import it.infoblu.mobilemap.model.remote.NetworkProvider;
import it.infoblu.mobilemap.utils.InternalLogger;
import it.infoblu.mobilemap.utils.JsonMapperManager;

/**
 * Created on 05/02/18.
 */

public final class SessionRepository implements IMobileMapSession {

    public static final String KEY_PERSISTENCE_CONFIG_DEFAULT = "persistence_config_default";

    private static SessionRepository _INSTANCE;

    private BehaviorSubject<RemoteConfig> mConfigObservable = BehaviorSubject.create();

    private BehaviorSubject<HashSet<Poi>> mPoiPoolSubject = BehaviorSubject.createDefault(new HashSet<Poi>());
    private PublishSubject<PoiUpdateInfo> mPoiUpdateSubject = PublishSubject.create();

    private HashSet<String> mPoiTypeSetDownloaded = new HashSet<>();


    public static SessionRepository getInstance() {
        if (_INSTANCE == null) {
            synchronized (SessionRepository.class) {
                if (_INSTANCE == null) {
                    _INSTANCE = new SessionRepository();
                }
            }
        }
        return _INSTANCE;
    }

    private SessionRepository() { // singleton
    }


    public Observable<RemoteConfig> startRemoteConfigRoutine() {
        return MobileMap.getConfigurationSingle()
                .flatMapObservable((Function<MobileMapConfiguration, ObservableSource<RemoteConfig>>) mobileMapConfiguration -> {
                    if (TextUtils.isEmpty(mobileMapConfiguration.getPoiConfigEndpoint())) {
                        return getConfigLocalObservable(); // EXIT
                    }
                    return getConfigRemoteObservable()
                            .onErrorResumeNext(throwable -> {
                                InternalLogger.e("Remote config not retrieved - ERR: %s", throwable);
                                return getConfigLocalObservable();
                            })
                            .map(remoteConfig -> {
                                updateDefaultConfig(remoteConfig);
                                mConfigObservable.onNext(remoteConfig);
                                return remoteConfig;
                            });
                });
    }


    /*
     * Session accessibility methods
     */

    @Override
    public Observable<RemoteConfig> getRemoteConfigObservable() {
        return mConfigObservable;
    }

    @Override
    public RemoteConfig getRemoteConfigSync() {
        return mConfigObservable.getValue();
    }

    @Override
    public Observable<HashSet<Poi>> getPoiListObservable() {
        return mPoiPoolSubject;
    }

    @Override
    public Observable<HashSet<String>> getPoiTypeSetObservable() {
        return getPoiListObservable()
                .map(poiList -> {
                    synchronized (mPoiTypeSetDownloaded) {
                        return new HashSet<>(mPoiTypeSetDownloaded);
                    }
                });
    }

    @Override
    public Observable<PoiUpdateInfo> getPoiUpdateObservable() {
        return mPoiUpdateSubject;
    }

    @Override
    public Observable<String> getPoiTypeUpdateObservable() {
        return getPoiUpdateObservable()
                .map(poiUpdateInfo -> poiUpdateInfo.getType());
    }

    @Override
    public Completable getPoiTypeDownloadedCompletable(String poiType) {
        return Completable.fromObservable(getPoiTypeSetObservable()
                .filter(strings -> strings.contains(poiType))
                .take(1));
    }

    @Override
    public LatLngBounds getLatLngBoundsLayerSync(String poiType) {
        LatLngBounds.Builder builder = LatLngBounds.builder();
        for (Poi poi : mPoiPoolSubject.getValue()) {
            if (StringUtils.equals(poi.getType(), poiType)) {
                builder.include(poi.getPosition().getLatLng());
            }
        }
        try {
            return builder.build();
        } catch (IllegalStateException e) {
            return null;
        }
    }

    /*
     * Internal methods
     */

    private Observable<RemoteConfig> getConfigRemoteObservable() {
        return MobileMap.getConfigurationSingle()
                .flatMapObservable(mobileMapConfiguration -> {
                    if (TextUtils.isEmpty(mobileMapConfiguration.getPoiConfigEndpoint())) {
                        return Observable.empty(); // EXIT
                    }
                    return NetworkProvider.getInstance().getConfiguration()
                            .subscribeOn(Schedulers.io())
                            .observeOn(Schedulers.io())
                            .toObservable();
                });
    }

    private Observable<RemoteConfig> getConfigLocalObservable() {
        return Observable.create((ObservableOnSubscribe<RemoteConfig>) emitter -> {
            RemoteConfig remoteConfig = loadDefaultConfig();
            if (remoteConfig != null) {
                emitter.onNext(remoteConfig);
            }
            emitter.onComplete();
        }).subscribeOn(Schedulers.io()).observeOn(Schedulers.io());
    }

    public Observable<List<Poi>> getPoi(final String poiType, String endpoint) {
        InternalLogger.d("dynamic poi endpoint: %s " + endpoint);
        return NetworkProvider.getInstance().getPoi(endpoint)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .toObservable()
                .map(responseDto -> {
                    propagatePoiListRetrieved(responseDto, poiType);
                    return responseDto;
                });
    }

    public Observable<List<Poi>> getPoiWithPersistence(final String poiType, final String endpoint, final long timestamp) {
        InternalLogger.d("static poi endpoint: %s " + endpoint);
        return Observable.just(-1)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .flatMap((Function<Integer, ObservableSource<List<Poi>>>) integer -> {
                    long poiPersistenceTimestamp = PoiPersistenceManager.getInstance().getPoiPersistenceTimestamp(poiType);
                    if (poiPersistenceTimestamp > timestamp) {
                        return Observable.fromCallable(() -> PoiPersistenceManager.getInstance().getPois(poiType));
                    }
                    // remote
                    return NetworkProvider.getInstance().getPoi(endpoint).toObservable()
                            .map(pois -> {
                                PoiPersistenceManager.getInstance().savePois(poiType, pois);
                                return pois;
                            });
                })
                .map(pois -> {
                    propagatePoiListRetrieved(pois, poiType);
                    return pois;
                });
    }


    /*
     * Internal methods
     */

    private void updateDefaultConfig(RemoteConfig remoteConfig) throws JsonProcessingException {
        KeyPreferences.getPrefs().getString(KEY_PERSISTENCE_CONFIG_DEFAULT)
                .set(JsonMapperManager.MAPPER.writeValueAsString(remoteConfig));
        InternalLogger.d("Default config saved: %s", remoteConfig);
    }

    private RemoteConfig loadDefaultConfig() throws IOException {
        RemoteConfig remoteConfig = null;
        try {
            remoteConfig = JsonMapperManager.MAPPER.readValue(KeyPreferences.getPrefs()
                    .getString(KEY_PERSISTENCE_CONFIG_DEFAULT).get(), RemoteConfig.class);
        } catch (Exception e) {
            // nothing to do here
        }
        // NB: here config is always available
        if (remoteConfig == null && MobileMap.getConfigurationSync().getPoiConfigDefaultRawId() != 0) {
            InputStream is = ResourceLoader.getRaw(MobileMap.getConfigurationSync().getPoiConfigDefaultRawId());
            remoteConfig = JsonMapperManager.MAPPER.readValue(is, RemoteConfig.class);
        }
        InternalLogger.d("Default config loaded: %s", remoteConfig);
        return remoteConfig;
    }

    private void propagatePoiListRetrieved(List<Poi> responseDto, final String poiType) {
        if (responseDto != null && !responseDto.isEmpty()) {
            InternalLogger.d("returned poi size: %s", responseDto.size());
            HashSet<Poi> pool = mPoiPoolSubject.getValue();
            synchronized (pool) {
                // remove previously added Poi
                pool = new HashSet(Stream.of(pool).filter(value -> !StringUtils.equals(poiType, value.getType())).toList());
                pool.addAll(responseDto);
            }
            synchronized (mPoiTypeSetDownloaded) {
                mPoiTypeSetDownloaded.add(poiType);
            }
            mPoiPoolSubject.onNext(pool); // update
            mPoiUpdateSubject.onNext(new PoiUpdateInfo(poiType, responseDto));
        }
    }

}
