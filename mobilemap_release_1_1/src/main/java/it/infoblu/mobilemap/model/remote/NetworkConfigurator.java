package it.infoblu.mobilemap.model.remote;


import java.io.File;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.util.concurrent.TimeUnit;

import it.infoblu.mobilemap.model.remote.interceptor.HeaderInterceptor;
import it.infoblu.mobilemap.model.remote.utils.NetworkUtils;
import it.infoblu.mobilemap.model.remote.utils.OkHttpUtils;
import okhttp3.Cache;
import okhttp3.JavaNetCookieJar;
import okhttp3.OkHttpClient;

public class NetworkConfigurator {

    private static NetworkConfigurator sInstance;
    private OkHttpClient mOkHttpClient;

    private NetworkConfigurator() {
    }

    public static NetworkConfigurator getInstance() {
        if (sInstance == null) {
            sInstance = new NetworkConfigurator();
            sInstance.configure();
        }
        return sInstance;
    }

    private void configure() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        //setOkHttp
        setOkHttpCache(builder);
        builder.connectTimeout(NetworkUtils.CONNECTION_TIMEOUT, TimeUnit.SECONDS);
        builder.readTimeout(NetworkUtils.CONNECTION_TIMEOUT, TimeUnit.SECONDS);
        builder.writeTimeout(NetworkUtils.CONNECTION_TIMEOUT, TimeUnit.SECONDS);
        builder.addInterceptor(new HeaderInterceptor());
//        if (C.CACHE_ENABLED) {
//            builder.addInterceptor(new CacheInterceptor());
//            builder.cache(new Cache(new File(CustomApplication.getContext().getCacheDir(), "http"), 10 * 1024 * 1024));
//        }

        OkHttpUtils.setupOkHttpForThisBuildType(builder);

        CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        builder.cookieJar(new JavaNetCookieJar(cookieManager));

        mOkHttpClient = builder.build();
    }

    private void setOkHttpCache(OkHttpClient.Builder builder) {
        File cacheDirectory = new File(NetworkUtils.pathFileCache);
        Cache cache = new Cache(cacheDirectory, NetworkUtils.cacheSize);
        builder.cache(cache);
    }

    public OkHttpClient getHttpClient() {
        return mOkHttpClient;
    }

}