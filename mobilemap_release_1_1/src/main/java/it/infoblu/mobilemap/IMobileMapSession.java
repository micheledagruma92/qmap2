package it.infoblu.mobilemap;

import com.google.android.gms.maps.model.LatLngBounds;

import java.util.HashSet;

import io.reactivex.Completable;
import io.reactivex.Observable;
import it.infoblu.mobilemap.model.data.Poi;
import it.infoblu.mobilemap.model.data.PoiUpdateInfo;
import it.infoblu.mobilemap.model.data.RemoteConfig;

/**
 * Created on 11/04/18.
 */
public interface IMobileMapSession {

    Observable<RemoteConfig> getRemoteConfigObservable();

    RemoteConfig getRemoteConfigSync();

    Observable<HashSet<Poi>> getPoiListObservable();

    Observable<HashSet<String>> getPoiTypeSetObservable();

    Observable<PoiUpdateInfo> getPoiUpdateObservable();

    Observable<String> getPoiTypeUpdateObservable();

    Completable getPoiTypeDownloadedCompletable(String poiType);

    LatLngBounds getLatLngBoundsLayerSync(String poiType);

}
