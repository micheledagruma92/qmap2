-dontoptimize
-verbose
-optimizationpasses 1
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-dontpreverify
#-optimizations !code/simplification/arithmetic,!field/*,!class/merging/*
-dontwarn afu.org.**
-dontskipnonpubliclibraryclassmembers


#COMMON
-keep class com.google.com.** { public *; }
-dontwarn com.google.errorprone.annotations.ForOverride
-dontwarn com.google.errorprone.annotations.**
-dontwarn com.google.common.util.concurrent.**


#JACKSON
-keepattributes *Annotation*,EnclosingMethod,Signature
-keepnames class com.fasterxml.jackson.** { *; }
-keepnames interface com.fasterxml.jackson.** { *; }
-dontwarn com.fasterxml.jackson.databind.**
-keep class org.codehaus.** { *; }


#GUAVA
-dontwarn javax.annotation.**
-dontwarn javax.inject.**
-dontwarn sun.misc.Unsafe

# Crashlytics
-keep class com.crashlytics.** { *; }
-dontwarn com.crashlytics.**
-keepattributes SourceFile,LineNumberTable,*Annotation*
-keep class com.crashlytics.android.**
-keepnames class com.crashlytics.android.** { *; }

# RETROFIT - OKHTTP3
-keep class com.square.** { *; }
-keep interface com.square.** { *; }
-dontwarn com.square.**

-keep class okhttp3.** { *; }
-keep interface okhttp3.** { *; }
-dontwarn okhttp3.**

-keep class okio.Okio.** { *; }
-keep interface okio.Okio.** { *; }
-dontwarn okio.**

-dontnote retrofit2.Platform
-dontwarn retrofit2.Platform$Java8
-keepattributes Signature
-keepattributes Exceptions

-keepclasseswithmembers class * {
 @retrofit.http.* <methods>;
}

# RXJAVA
-keep class rx.** { *; }
-keep interface rx.** { *; }
-dontwarn rx.**

-keep class * extends java.util.ListResourceBundle {
    protected Object[][] getContents();
}

#GMS
-keep class com.google.android.gms.** { *; }
-keep interface com.google.android.gms.** { *; }
-dontwarn com.google.android.gms.**

-keepnames @com.google.android.gms.common.annotation.KeepName class *
-keepclassmembernames class * {
    @com.google.android.gms.common.annotation.KeepName *;
}


# MOBILE MAP LIBRARY

-dontwarn java.lang.invoke.**
-keep class it.infoblu.mobilemap.helper.hook.** { *; }
-keep class it.infoblu.mobilemap.helper.interfaces.** { *; }
-keep class it.infoblu.mobilemap.helper.listeners.** { *; }
-keep class it.infoblu.mobilemap.helper.location.** { *; }
-keep class it.infoblu.mobilemap.helper.settings.** { *; }
-keep class it.infoblu.mobilemap.helper.config.** { *; }
-keep class it.infoblu.mobilemap.helper.mapbox.utils.InternalMapBoxUtils { *; }
-keep class it.infoblu.mobilemap.helper.settings.MapSettingsComposite { *; }
-keep class it.infoblu.mobilemap.utils.** { *; }
-keep class it.infoblu.mobilemap.IMobileMapSession { *; }
-keep class it.infoblu.mobilemap.MobileMap { *; }
-keep public class it.infoblu.mobilemap.helper.mapbox.MapsMapBoxHelper { public *; }
-keep public class it.infoblu.mobilemap.helper.hook.PoiUpdateMapHelperHook { public *; }
-keep public class it.infoblu.mobilemap.service.SessionRoutineService { public *; }
-keep class it.infoblu.mobilemap.constants.MobileMapSettingsConfig { *; }

-keep class it.infoblu.mobilemap.model.constants.** { *; }
-keep class it.infoblu.mobilemap.model.converter.** { *; }
-keep class it.infoblu.mobilemap.model.data.** { *; }
-keep class it.infoblu.mobilemap.model.exceptions.** { *; }
-keep class it.infoblu.mobilemap.model.local.** { *; }
-keep class it.infoblu.mobilemap.model.prefs.** { *; }
-keep class it.infoblu.mobilemap.model.remote.deserializer.** { *; }
-keep class it.infoblu.mobilemap.model.repository.SessionRepository { public *; }


#COMMONS
-keepattributes Signature,*Annotation*,EnclosingMethod,SourceFile,LineNumberTable
-keep class android.support.v4.** { *; }
-keep class android.support.v7.** { *; }
-keep class java.lang.** { *; }
-keep class com.android.** { *; }


-keepclasseswithmembernames class * {
    native <methods>;
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet);
}

-keepclasseswithmembers class * {
    public <init>(android.content.Context, android.util.AttributeSet, int);
}


-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

#PARCELABLE
-keep class * implements android.os.Parcelable {
  public static final android.os.Parcelable$Creator *;
}

-keepclassmembers class * implements android.os.Parcelable {
    static ** CREATOR;
}

#RESOURCES
-keepclassmembers class **.R$* {
    public static <fields>;
}
-keep class **.R$*

#WEBVIEW
-keepattributes JavascriptInterface
-keepclassmembers class * {
    @android.webkit.JavascriptInterface <methods>;
}


