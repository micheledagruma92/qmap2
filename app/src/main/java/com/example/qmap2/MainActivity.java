package com.example.qmap2;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;

import it.infoblu.mobilemap.MobileMap;
import it.infoblu.mobilemap.helper.mapbox.MapsMapBoxHelper;
import it.infoblu.mobilemap.model.data.MobileMapConfiguration;

public class MainActivity extends AppCompatActivity {
    private final String APIKEY = "O8eibj5uAcJoR2bBsI832OyIPD6WG9e9";
    private final String ENDPOINT = "http://sdk.qmap.it/appconf.json?apikey=";

    private MapView mapView;
    private MapboxMap map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(this, getString(R.string.access_token));
        setContentView(R.layout.activity_main);
        MobileMapConfiguration.Builder config = new MobileMapConfiguration.Builder();
        config.poiConfigEndpoint(ENDPOINT);
        mapView = findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        //mapView.getMapAsync(this);
        config.apikey(APIKEY);
        config.logEnabled(false);
        MobileMap.init(this, config.build(), getString(R.string.access_token));
        MapsMapBoxHelper helper = new MapsMapBoxHelper(getLifecycle(), mapView);
        helper.onCreate(savedInstanceState);
        helper.showTraffic(true);
    }

    /*@Override
    public void onMapReady(MapboxMap mapboxMap) {
        map = mapboxMap;
    }*/
}
